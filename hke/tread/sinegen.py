import textwrap

import numpy as np
from scipy.integrate import quad


n_ch = 16          # Number of channels to measure
T_frame = 1.       # Frame period
N_hp = 125         # Number of ticks per half-period

T0 = T_frame/n_ch  # Single-channel period, assuming 1 second total period
w0 = 2*np.pi/T0    # Single-channel angular frequency
T60 = 1./60.       # 60 Hz period
w60 = 2*np.pi/T60  # 60 Hz angular frequency

SUM_TICKS = 66.
GAP_TICKS = 59.
CHOP_TICKS = SUM_TICKS + GAP_TICKS


amp = np.sqrt(2)   # A = sqrt(2) gives time-averaged power amplitude of 1

f1 = lambda t: -np.sin(w0*t)**2 * amp
f2 = lambda t: np.sin(w0*t)*np.sin(w60*t)
f3 = lambda t: np.sin(2*w0*t)*np.sin(w60*t)

result1 = quad(f1, 0, T0)[0]
result2 = quad(f2, 0, T0)[0]
result3 = quad(f3, 0, T0)[0]

print result1, result2, result3
k0 = 1./result1
k1 = -result2/(result1*result3)
print "k0 = ", k0
print "k1 = ", k1

# N_sum = 66
# N_gap = 59
sign = -1
# betas = np.array([0.]*N_gap + [1.]*N_sum + [0.]*N_gap + [1.*sign]*N_sum)/(2*N_sum)
betas_60 = (k0*np.sin(np.arange(2*N_hp)*np.pi/N_hp)
            + k1*np.sin(2*np.arange(2*N_hp)*np.pi/N_hp))

carrier2 = amp*np.sin(np.arange(2*N_hp)*np.pi/N_hp)
betas_60_sum = np.dot(carrier2, betas_60)
betas_60 = betas_60/betas_60_sum
betas_60_max = np.abs(betas_60).max()
gain1 = 32767/betas_60_max
betas_60 = gain1*betas_60

# betas_60 = (k0*np.sin(np.arange(2*N_hp)*np.pi/N_hp)
#             + k1*np.sin(2*np.arange(2*N_hp)*np.pi/N_hp))

carrier = (65535*np.sin(np.arange(N_hp)*np.pi/N_hp)).astype('int')
# betas_60_max = np.abs(betas_60).max()
# betas_60 = (betas_60/betas_60_max * 32767).astype('int')
print "fmax = ", betas_60_max
GAIN = -2.*betas_60_max/(32767.*k0*T0) * CHOP_TICKS/SUM_TICKS #-2*betas_60_max/(32767*k0*T0) * SUM_TICKS/CHOP_TICKS  #-4.*betas_60_max/(32767.*k0*T0)  #DELME #FIXME?
print "GAIN = ", GAIN


a = r'{' + ', '.join([str(b) for b in betas_60.astype('int')]) + r'}'
a = textwrap.fill(a)
c = r'{' + ', '.join([str(b) for b in carrier]) + r'}'
c = textwrap.fill(c)

ftext = (
"""#ifndef {0}
#define {0}
{6}

const {5} {1}[{2}] = {3};

#endif  // {4}""")
swtxt = ftext.format('SINETABLE_H', 'sine_table', 'CHOP_TICKS', c, 'sinetable.h', 'unsigned', '', '')
filtertxt = ftext.format('FILTERTABLE_H', 'filter_table', 'DEMOD_TICKS', a,
	'filtertable.h', 'int',
	'#define GAIN (SUM_TICKS*{0}f)'.format(GAIN))

fname = 'sinetable.h'
print "Writing sine table file to {0}".format(fname)
with open(fname, 'w') as f:
	f.write(swtxt)
print "Done."

fname = 'filtertable.h'
print "Writing filter table file to {0}".format(fname)
with open(fname, 'w') as f:
	f.write(filtertxt)
print "Done."