#!/bin/env python

"""
psyncadc.py
jlazear
9/1/11

psyncadc.py is a script for interfacing with the PSyncADC board and
the sensors connected to it.

It requires PySerial (http://pyserial.sourceforge.net/index.html) to
be installed on the user's terminal, and comscan.py to be in the
$PYTHONPATH (typically in the same directory as psyncadc.py).

The readline module may not function correctly on non-*nix systems, so
this script may not function correctly on Windows or OS X. It has not
been tested on either. Its added functionality is minimal, so you can
comment out its import and any referencing statements and the script
will function.

psyncadc.py is capable of both interactive mode and timed run
mode. Modify the timer value in the main loop to make a timed run, or
leave it set to None for an interactive run.
"""
version = 110901
releasestatus = 'stable'

import serial
from numpy import *
import threading
import comscan
import os
import datetime
import sys
import time

if sys.platform.startswith('linux'):
    import readline
else:
    from types import ModuleType

    class DummyModule(ModuleType):
        def __getattr__(self, key):
            return self.passfunction

        def passfunction(self, *args, **kwargs):
            pass

        __all__ = []

    readline = DummyModule('readline')


# ============================================================
# Multi-purpose Functions
# ============================================================

def startup():
    """
    The start-up routine of the program. Greets the user, explains the
    program, and questions the user about a timed run.
    """
    print '='*60
    print '='*60
    print 'psyncadc'
    print 'Program for interacting with the PSyncADC'
    print 'data acquisition board.'
    print '='*60
    print 'version {version}.{stat}'.format(version=version,
                                            stat=releasestatus)
    print '='*60

    print
    print """Select duration of timed run, or interactive mode.
0         - Interactive mode [default]
float > 0 - Duration in seconds of timed run
    """
    print '-'*60

    timer = False
    while timer is False:
        timer = raw_input('@@@ ')
        if timer is '':
            timer = 0
        try:
            timer = float(timer)
            if timer < 0:
                timer = False
        except ValueError:
            timer = False

    if timer == 0:
        timer = None

    return timer

def ADCtoVolt(value):
    """
    Converts from ADC units to volts.

    :Arguments:

        value : float
        The ADC value to convert

    :Returns:

        volts : float
        The voltage corresponding to the ADC value `value`.
    """
    return (value - 32768.)/65536*4.99*4.096

def parseADCstring(str):
    """
    Parses an output string from the ADC and returns the information
    contained in it.

    :Arguments:

        string : string
        The string to be parsed.

    :Returns:

        framenum : int
        The frame number, in base 10.

        dataarray : 32-element list of ints
        The value of each channel of the ADC, in base 10.
    """
    if str[0] != '*':
        str = '*' + str

    frame = str[1:9]  # Characters 1-8 are frame number
    data = str[10:] # Remaining characters after hyphen is data
    num = len(data)/4 # Number of samples in the packet

    try:
        framenum = int(frame, 16)
        # Note: '\r\n' is ignored by int(), so don't need to strip it
        dataarray = array([int(data[4*i:4*(i+1)], 16) for i in
                           range(num)])
    except ValueError:
        if '_' in data:
            raise TestPacketError
        else:
            raise PacketError(str)
    dataarray = ADCtoVolt(dataarray)

    return framenum, dataarray

def numericrangetolist(nrange):
    try:
        if nrange == 'all':
            return range(32)
        elif '-' not in nrange:
            return [int(nrange)]
        nlist = nrange.split('-')
        lower = max(int(nlist[0]), 0)
        upper = min(int(nlist[-1]) + 1, 32)
    except ValueError:
        return []
    retrange = range(lower, upper)
    return retrange

def parsechannelsstring(inputstr):
    """
    Parses the string input to select the channels and returns a list
    of enabled channel numbers.
    """
    # If empty, use default 'all'
    if inputstr is '':
        print 'Recording channels: 0-31.'
        return range(32)

     # Make everything lowercase
    inputstr = inputstr.lower()

    # Remove all commas
    inputstr = inputstr.replace(',', ' ')

    # Items after 'but' should be removed from the set
    toadd, but, todel = inputstr.partition('but')

    toaddlist = toadd.split(' ')
    todellist = todel.split(' ')

    # Remove 'and' and empty strings from the lists
    toaddlist = [value for value in toaddlist if
                 value not in ['and', '']]
    todellist = [value for value in todellist if
                 value not in ['and', '']]

    channelset = set()

    # Add items to the set
    for i in toaddlist:
        toadd = numericrangetolist(i)
        channelset = channelset.union(toadd)


    # Remove items from the set
    for i in todellist:
        todel = numericrangetolist(i)
        channelset = channelset.difference(todel)

    # Sort and convert to list
    channellist = sorted(channelset)

    return channellist

# ============================================================
# Classes
# ============================================================

class GUIThread(threading.Thread):
    """
    The thread that will handle interfacing with the GUI. Monitors for
    GUI input and broadcasts it to the SerialThread.
    """
    def __init__(self):
        threading.Thread.__init__(self)
        self.cont = True
        self.acknowledge = threading.Event()
        self.acknowledge.set()
        self.message = None

    def run(self):
        while True:
            self.acknowledge.wait()
            if self.cont is False:
                return
            time.sleep(0.01)
            if self.message is not None:
                self.acknowledge.clear()

class InputThread(threading.Thread):
    """
    The thread that will handle interfacing with the user. Records
    user input and broadcasts it to the SerialThread.
    """
    def __init__(self, timer=None):
        threading.Thread.__init__(self)
        self.cont = True
#        self.acknowledge = True
        self.acknowledge = threading.Event()
        self.acknowledge.set()
        self.message = None
        self.timer = timer

    def run(self):
        if self.timer is not None:
            self.timerun()
        else:
            self.userrun()

    def timerun(self):
        self.t0 = time.time()
        try:
            self.tf = self.t0 + float(self.timer)
        except ValueError:
            self.message = 'quit'
            raise TimerError(self.timer)

        print "Beginning timed run of duration: {d}s\nRun will end at\
: {f}".format(d=self.timer, f=time.strftime('%H:%M:%S',
                                            time.gmtime(self.tf)))

        while time.time() < self.tf:
            time.sleep(0.1)

        self.message = 'quit'

    def userrun(self):
        while True:
            self.acknowledge.wait()
            if self.cont is False:
                return
            print ""
            self.message = raw_input(">>> ")
            self.acknowledge.clear()

class SerialThread(threading.Thread):
    """
    The thread that will be interacting with the serial port.
    """

    # self.commands = {'quit': self.quit,
    #                  's': self.send,
    #                  'dper': self.dataperiod,
    #                  'period': self.dataperiod,
    #                  'data': self.datatoggle,
    #                  'toggle': self.datatoggle,
    #                  'reset': self.reset,
    #                  'ph': self.printheader,
    #                  'header': self.printheader,
    #                  'source': self.fsrc,
    #                  'fsrc': self.fsrc}

    #====================================================#
    # system methods
    #----------------------------------------------------#

    def __init__(self, inputthread=InputThread(), port=None,
                 fsrc=None, dper=None, channels=None, comments=None,
                 baudrate=115200, parity='N', stopbits=1, timeout=10,
                 bytesize=8, spew=True, verbose=True):
        threading.Thread.__init__(self)
        self.inputthread = inputthread
        self.portname = port
        self.framesource = fsrc
        self.dataperiod
        self.channels = channels
        self.comments = comments
        self.baudrate = baudrate
        self.parity = parity
        self.stopbits = stopbits
        self.timeout = timeout
        self.bytesize = bytesize
        self.cont = True
        self.spew = spew
        self.resetflag = False
        self.verbose = verbose

        self.commands = {'quit': self.quit,
                         's': self.send,
                         'dper': self.dataperiod,
                         'period': self.dataperiod,
                         'data': self.datatoggle,
                         'toggle': self.datatoggle,
                         'reset': self.reset,
                         'ph': self.ph,
                         'header': self.ph,
                         'help': self.help}

    def __del__(self):
        try:
            self.outputfile.close()
            print "Closed data file..."
            self.connection.close()
            print "Closed serial connection..."
        except AttributeError, NameError:
            pass
        print "Destroyed psyncadc.SerialThread object."

    #====================================================#
    # initialization
    #----------------------------------------------------#

    def initialize(self, port=None, fsrc=None, dper=None,
        channels=None, comments=None, verbose=None):
        """
        Run the initialization routines. This is separate from
        __init__ because there are instances where we'd like to create
        the SerialThread object, perform some other actions, and then
        initialize the communications with the board.
        """
        if verbose is None:
            verbose = self.verbose
        if port is None:
            port = self.portname

        self.portname = self.choseport(port=port)
        self.initializeport()

        if fsrc is None:
            fsrc = self.framesource
        if dper is None:
            dper = self.dataperiod
        if channels is None:
            channels = self.channels

        if comments is None:
            comments = ''
        self.comments = comments

        self.initializeboard(fsrc=fsrc, dper=dper, channels=channels,
                             verbose=verbose)

        self.outputfilename = self.getoutputfilename()
        self.outputfile = open(self.outputfilename, 'w')

        self.cont = True
        self.inputthread.cont = True

    def getoutputfilename(self):
        """
        Determines the filename of the next file.
        """
        pwd = os.getcwd()
        filelist = os.listdir(pwd)
        count = 0
        now = datetime.datetime.now()
        prefix = 'psyncadc_' + now.strftime("%Y-%m-%d")
        for filename in filelist:
            if filename.startswith(prefix):
                count += 1

        filename = (prefix + '_{count}.dat'.format(count=count))
        return filename

    def choseport(self, port=None):
        """
        Scans for available ports and displays them to the user, then
        prompts the user to select which port to use.
        """
        if port is not None:
            return port
        print "="*60
        print "Available serial ports:"
        print "-"*60
        devices = comscan.comscan()
        available = ['forceselect']
        for device in devices:
            if device['available'] == True:
                name = device['name']
                print name
                available.append(name)
                readline.add_history(name)
        print '='*60
        print 'You may select a port with up/down arrows, or type it in.'
        print '-'*60

        chosen = "/dev/null"      # This will never be available
        while chosen not in available:
            chosen = raw_input("Input which port you wish to use \
('quit' to quit): ")
            if chosen == 'quit':
                sys.exit()

        if chosen == 'forceselect':
            print "Force-selecting a port. Will not protect against\
 errors."
            chosen = raw_input("Type in your port: ")

        return chosen

    def initializeport(self):
        """
        Initializes the serial port to a known state. Turns off data
        transmission from the board, clears the input buffer, and
        tests the serial connection. Raises an InitializationError if
        this fails.
        """
        self.connection = self.openport()
        self.send('data off', False)

        time.sleep(0.1)

        counter = 0
        while self.connection.inWaiting() != 0:
            if counter == 10:
                raise InitializationError(self.portname,
                                          'Flush input buffer')
            self.connection.flushInput()
            time.sleep(0.1)             # Let the buffer excess
                                        # trickle in
            counter += 1

        time.sleep(0.1)
        self.send('data off', False)
        time.sleep(0.1)
        doffresponse = self.connection.read(5)
        if doffresponse != 'OK!\r\n':
            raise InitializationError(self.portname, 'First data off')

    def openport(self):
        """
        Initializes and opens the connection to the serial port.
        """
        ser = serial.Serial()
        ser.port = self.portname
        ser.baudrate = self.baudrate
        ser.parity = self.parity
        ser.stopbits = self.stopbits
        ser.timeout = self.timeout
        ser.bytesize = self.bytesize
        ser.open()
        return ser

    def initializeboard(self, fsrc=None, dper=None, channels=None,
                        verbose=True):
        """
        Initializes the PSyncADC board to the desired state. Prompts
        the user for the desired settings. Selects the clock to use
        (external or internal), selects the data period, and selects
        the channels to monitor.
        """
        self.fsrc(fsrc=fsrc, verbose=verbose)
        self.dper(dper=dper, verbose=verbose)
        self.checkchannels(channels=channels, verbose=verbose)

    def fsrc(self, fsrc=None, verbose=True):
        """
        Prompts the user to select the frame source (i.e. which clock
        to use).
        """
        if verbose == True:
            print '='*60
            print """Select which clock to use.
int - [default] Use the crystal oscillator on the board. Allows
      frequencies of integer divisors of 400Hz (e.g. 400Hz, 200Hz,
      100Hz, ...)
ext - Use an external clock, e.g. the SyncBox. Allows frequencies of
      integer divisors of the external clock frequency.
quit - Quit the program.
"""
            print '-'*60
        if fsrc is None:
            choices = ['int', 'ext']
            for choice in choices:
                readline.add_history(choice)
            while not fsrc:
                print "Select your clock (int/ext/quit):"
                fsrc = raw_input('$$$ ')
                if fsrc == 'quit':
                    sys.exit()
                if fsrc is '':
                    fsrc = 'int'        # Default setting = 'int'
                if fsrc not in choices:
                    fsrc = ''           # Repeat loop

        self.clock = fsrc

        tosend = ['fsrc'] + [fsrc]
        self.send(tosend)
        if verbose == True:
            if fsrc == 'int':
                print "Using internal clock."
            elif fsrc == 'ext':
                print "Using external clock."
            else:
                raise InitializationError(self.portname, 'Clock select')

    def dper(self, dper=None, verbose=True):
        """
        Prompts the user to select the data period (i.e. number of
        frames to skip between sample reads).
        """
        if verbose == True:
            print '='*60
            print """Select the data period (number of frames between
sample reads). The data period determines the sampling rate:

(sampling rate) = (clock frequency)/(data period)

You must use a positive integer data period (1, 2, 3, ...).
Default data period = 10.
"""
            print '-'*60
        if dper is None:
            dperlist = range(1, 10)
            dperlist.reverse()
            for item in dperlist:
                readline.add_history(str(item))
            while not dper:
                print "Select your data period (positive integer, 'quit'\
 to quit):"
                dper = raw_input('### ')
                if dper == 'quit':
                    sys.exit()
                if dper is '':
                    dper = 10
                try:
                    dper = int(dper)
                    if dper <= 0:
                        dper = ''
                except ValueError:
                    dper = ''

        self.dataperiod = dper

        tosend = ['dper'] + [str(dper)]
        self.send(tosend)

        if verbose == True:
            print "Using data period of {dper}".format(dper=dper)

    def checkchannels(self, channels=None, verbose=True):
        """
        Asks the user which channels should be monitored.
        """
        if verbose == True:
            print '='*60
            print """Select which channels to monitor. 'all' is default.
Channels begin at 0 and end at 31.
e.g.
%%% 1-4 12, 18 and 19
will enable recording on channels 1, 2, 3, 4, 12, 18, and 19.
%%% all
will enable recording on all 32 channels.
%%% all but 15-17
will enable recording all all channels except 15, 16, and 17
'quit' will quit."""
            print '-'*60

        if channels is None:
            while not channels:
                print "Select your channels ('quit' to quit):"
                channels = raw_input('%%% ')
                if channels == 'quit':
                    sys.exit()
                channels = parsechannelsstring(channels)

        self.channels = channels

        tosend = ['ch'] + [str(channel) for channel in channels]
        self.send(tosend)

        if verbose == True:
            print 'Recording channels: ' + ', '.join([str(i) for i in
                                                      channels])

    #====================================================#
    # user commands
    #----------------------------------------------------#

    def reset(self, args=None):
        """
        Resets the program, allowing the user to reconfigure the board
        settings.
        """
        # Is this a better way of doing resets?
#        self.__init__()
#        self.run()
        self.resetflag = True
        self.quit()

    def ph(self, args=None):
        """
        Print the state information for the board.
        """
        return self.send('ph')

    def quit(self, args=None):
        now = datetime.datetime.now()
        towrite = '#END TIME: ' + str(now)
        self.outputfile.write(towrite)
        self.cont = False
        self.inputthread.cont = False
        print "\nQuitting..."
        self.connection.close()
        self.outputfile.close()

    def dataperiod(self, args=['10']):
        tosend = ['dper'] + args
        return self.send(tosend)

    def datatoggle(self, args=None):
        if args is None:
            state = not self.spew
        elif type(args) is list:
            state = args[0]
        else:
            state = args

        try:
            if type(state) is str:
                state = state.lower()
                if state in ['f', 'false', 'off', 'no', 'n']:
                    state = False
                elif state in ['t', 'true', 'on', 'yes', 'y']:
                    state = True
                else:
                    try:
                        print "state = ", state
                        newstate = eval(state)
                        state = newstate
                    except NameError:
                        print "errorstate = ", state
                        raise ArgumentError(state)

            self.spew = bool(state)

            if state == True:
                cmd = 'on'
            elif state == False:
                cmd = 'off'
            else:
                print 'state = ', state
                print 'type(state) = ', type(state)
                raise ArgumentError(state)

        except ArgumentError, e:
            print e.msg
            return

        tosend = ['data', cmd]
        return self.send(tosend)

    def listcommands(self):
        print "Available commands:"
        for key in self.commands.keys():
            print key
        print '-'*5
        print "Use 'help [command]' for help on a specific\
 command"

    def help(self, args=None):
        """
        Get help for a particular command.

        E.g.
        >>> help help
        prints this message.
        """
        if args is None:
            self.listcommands()
            return

        command = str(args)
        try:
            cmdmethod = self.commands[command]
        except KeyError:
            print 'Command "{command}" not recognized!'.format(command=command)
            self.listcommands()
            return

        print command
        print '-'*60
        print cmdmethod.__doc__

    #====================================================#
    # PSyncADC message handling
    #----------------------------------------------------#

    def send(self, args=None, readresponse=True):
        """
        Send a raw string to the PSyncADC board.
        """
        if type(args) is not str:
            try:
                tosend = ' '.join(args)
            except TypeError:
                return
        else:
            tosend = args

        tosend = tosend + '\r'          # Append the termination char
        self.connection.write(tosend)
        if readresponse is True:
            response = self.readresponse()
            print response
            return response
        else:
            time.sleep(0.1)

    def readresponse(self):
        """
        Reads the response to a command.
        """
        buffer = []
        char = None
        while char is not '!':
            char = self.connection.read(1)
            buffer.append(char)
        rn = self.connection.read(2)
        buffer.append(rn)
        response = ''.join(buffer)
        textlist = response.split('*')

        packetlist = []
        restlist = [textlist[0],]
        for item in textlist[1:]:
            packet, waste, rest = item.partition('\n')
            packetlist.append(packet)
            restlist.append(rest)

        for packet in packetlist:
            self.writepacket(packet)

        message = ''.join(restlist)
        return message

    #====================================================#
    # File I/O
    #----------------------------------------------------#

    def writepacket(self, packet):
        """
        Decodes and writes a packet to the output file.
        """
        try:
            frame, dataarray = parseADCstring(packet)
        except TestPacketError:         # If parsing a test packet,
            return                      # don't write to file.
        except PacketError, e:
            savetxt(self.outputfile, '# ' + e.packet, '%s')
            print e.msg
        try:
            chindices = range(len(self.channels))
            dataarray = [dataarray[i] for i in chindices]
            packet = array(insert(dataarray, 0, frame)).reshape(1, -1)
            savetxt(self.outputfile, packet, fmt='%.8f')
        except IndexError:
            errmsg =  "Data packet was too short! Length = \
{len}\n>>> ".format(len=len(packet))

            # This try/except statement shouldn't be necessary... DELME
            try:
                savetxt(self.outputfile, '# ' + packet, '%s')
                print errmsg
            except IndexError, e:
                print "packet = ", packet

    def printheader(self, comment='#'):
        """
        Print the header to the file.
        """
        try:
            # Write the file header
            now = datetime.datetime.now()

            header = []
            header.append("TIMESTAMP: " + str(now))
            header.append("SOURCE: psyncadc.py v. " + str(version) + '.'
                          + str(releasestatus))
            header.append("START TIME: " + str(now))
            header.append("")

            print '-'*60
            ph = self.send('ph')
            print '-'*60
            ph = ph[:-5].split('\r\n')
            header.extend(ph)

            header.append('')
            header.append('BEGIN COMMENTS')
            header.extend(self.comments.split('\n'))
            header.append('END COMMENTS')
            header.append('')

            header = [comment + s for s in header]
            savetxt(self.outputfile, header, fmt='%s')

            # Write the column headers
            headlist = [comment + 'frame']
            headlist.extend(['channel_{i}'.format(i=i) for i in
                             self.channels])
            savetxt(self.outputfile, [headlist], fmt='%s')
        except Exception, e:
            print e

    #====================================================#
    # user interface control
    #----------------------------------------------------#

    def parsemessage(self):
        """
        Parses and acts upon the message being sent by the
        inputthread.
        """
        message = self.inputthread.message
        args = message.split()
        try:
            command = args.pop(0)
        except IndexError:
            command = None
        if args == []:
            args = None

        try:
            self.commands[command](args)
        except KeyError:
            print 'Command "{command}" not recognized!'.format(command=command)
            self.listcommands()

        self.inputthread.message = None # Clear the message buffer
        self.inputthread.acknowledge.set() # Acknowledge that the
                                           # message was processed

    #====================================================#
    # operation
    #----------------------------------------------------#

    def run(self):
        self.printheader()
        if self.spew is True:
            self.datatoggle('on')

        name = "{prefix}/{name}".format(prefix=os.getcwd(),
                                        name=self.outputfile.name)
        print "Saving data to {name}".format(name=name)
        print "Acquiring data..."
        try:
            self.inputthread.start()    # Start if not running
        except RuntimeError:
            pass
            # self.inputthread.run()      # Restart if already running
        while self.cont is True:
            if self.inputthread.message is not None:
                self.parsemessage()
            elif self.spew is True:
                char = self.connection.read(1)
                if char == '*':
                    line = self.connection.readline()
                    self.writepacket(line)

# ============================================================
# Exception Classes
# ============================================================

class PsyncadcError(Exception):
    """Base class for psyncadc errors."""
    pass

class PacketError(PsyncadcError):
    """
    Exception indicating a malformed packet from the Psync ADC
    board.

    :Attributes:

        packet : string
        The malformed packet in original string form.

        length : int
        The length of the malformed packet.

        msg : string
        The error message associated with this error.
    """

    def __init__(self, packet):
        self.packet = packet
        self.length = len(packet)
        self.msg = "Data packet was malformed! Length = \
{len}\n>>>".format(len = self.length)

    def __str__(self):
        return self.msg[:-4]

class ArgumentError(PsyncadcError):
    """
    Exception indicating an unknown argument to a command.

    :Attributes:

        argument : string
        The unknown argument.

        msg : string
        The error message associated with this error.
    """
    def __init__(self, argument):
        self.argument = argument
        self.msg = "Unknown argument: '{argument}'".format(argument
                                                              = argument)
        def __str__(self):
            return self.msg

class TimerError(PsyncadcError):
    """
    Exception indicating that the timer was improperly specified.

    :Attributes:

        timer : object
        The improperly specified timer value.

        timertype : type
        The type of the improperly specified timer value.

        msg : string
        The error message associated with this error.
    """
    def __init__(self, timer):
        self.timer = timer
        self.timertype = type(timer)
        self.msg = "Timer must be a number. You passed: {timer}, of \
type {type}".format(timer=self.timer, type=self.timertype)

    def __str__(self):
        return self.msg

class InitializationError(PsyncadcError):
    """
    Exception indicating that the initialization failed.

    :Attributes:

        port : string
        The port that failed to initialize correctly

        location : string
        Where in the initialization procedure that the initialization
        failed.

        msg : string
        The error message associated with this error.
    """
    def __init__(self, port, location):
        self.port = port
        self.location = location
        self.msg = "psyncadc initialization failed on port {port} at\
 location {location}".format(port=self.port, location=self.location)

    def __str__(self):
        return self.msg

class TestPacketError(PsyncadcError):
    """
    Exception indicating that a test packet (the first packet after a
    channel change command is a test packet and should be discarded)
    was parsed.
    """
    pass


# ============================================================
# Main Loop
# ============================================================

if __name__ == "__main__":
    timer = startup()
    resetflag = True
    while resetflag is True:
        ithread = InputThread(timer=timer)
        sthread = SerialThread(inputthread=ithread)
        sthread.initialize()
        sthread.start()
        sthread.join()
        resetflag = sthread.resetflag
