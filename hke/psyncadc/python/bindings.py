import wx
import gui
import psyncadc
import sys
import comscan
import serial
import os

class Bindings:
    def __init__(self, app, sthread=None, guithread=None):
        self.app = app
        self.sthread = sthread
        self.guithread = guithread
        self.fMainFrame = app.fMainFrame

    def set_bindings(self):
        """
        Bind the events.
        """
        # Menu item bindings
        f = self.fMainFrame
        f.Bind(wx.EVT_MENU, self.on_quit, f.miQuit)
        f.Bind(wx.EVT_MENU, self.on_about, f.miAbout)
        f.Bind(wx.EVT_MENU, self.on_inspector, f.miInspector)

        # Settings selection bindings
        f.Bind(wx.EVT_TEXT, self.check_port, f.cbPort)
        f.Bind(wx.EVT_SPINCTRL, self.estimate_frequency, f.scDper)

        # # Send/Buffer bindings
        f.Bind(wx.EVT_TEXT_ENTER, self.on_send)
        redir = RedirectText(f.txtctlBuffer)
        sys.stdout = redir

        # Initialize/Reset buttons bindings
        f.Bind(wx.EVT_BUTTON, self.on_initialize, f.bInitialize)
        f.Bind(wx.EVT_BUTTON, self.on_reset, f.bReset)

        self.set_initial_states()

    def set_initial_states(self):
        """
        Sets the initial states (e.g. hidden/shown) of the various
        controls. This is here instead of in the frame definitions
        because the bindings determine the usage of the GUI controls,
        and the usage of the GUI controls determines their initial
        states.
        """
        f = self.fMainFrame

        self.enable_send_buffer(False)
        f.bInitialize.Enable(False)

        # Populate the port dropdown box
        devices = comscan.comscan()
        for device in devices:
            if device['available'] == True:
                name = device['name']
                f.cbPort.Append(name)
        if len(f.cbPort.GetItems()) == 0:
            f.cbPort.Append('None Available')
        f.cbPort.SetSelection(0)

        # Check default port
        self.check_port(None)

        # Estimate default frequency
        self.estimate_frequency(None)

        # Hide the header status box
        ms = f.GetSizer()
        ms.Hide(2)
        f.Fit()

        f.Layout()

    def on_quit(self, event):
        self.guithread.message = 'quit' # Tell threads to quit
        if self.guithread.isAlive():
            self.guithread.join()       # Wait until threads quit
        self.fMainFrame.Close()         # Destroy the GUI

    def on_send(self, event):
        f = self.fMainFrame
        g = self.guithread
        toadd = f.txtctlSend.GetValue()
        f.txtctlSend.Clear()
        old = f.txtctlBuffer.GetValue()
        new = old + toadd + '\r\n'
        f.txtctlBuffer.ChangeValue(new)
        g.message = toadd

    def on_about(self, event):
        aboutdlg = wx.MessageDialog(self.fMainFrame,
                                    """psyncadc.py
GUI for interfacing with the PSyncADC board
Justin Lazear (jlazear@pha.jhu.edu)""",
                                    'About psyncadc.py', wx.OK |
                                    wx.ICON_INFORMATION)

        aboutdlg.ShowModal()
        aboutdlg.Destroy()

    def on_initialize(self, event):
        print ""
        f = self.fMainFrame
        self.enable_send_buffer(True)
        s = self.sthread

        # Initialize the board
        port = f.cbPort.GetValue()
        fsrcn = f.rbFsrc.GetSelection()
        if fsrcn == 0:
            fsrc = 'int'
        elif fsrcn == 1:
            fsrc = 'ext'
        else:
            raise ValueError            # Never get here
        dper = f.scDper.GetValue()
        channelsstr = f.txtctlChannels.GetValue()
        channels = psyncadc.parsechannelsstring(channelsstr)
        comments = f.txtctlComments.GetValue()
        s.initialize(port=port, fsrc=fsrc, dper=dper,
                     channels=channels, comments=comments)

        # Show the header status box
        ms = f.GetSizer()
        ms.Show(2)
        f.Fit()

        # Set and configure the header status box
        header = s.ph()[:-5]
        h = f.txtctlHeader
        h.SetValue(header)
        h.SetMinSize(h.GetTextExtent(header))

        # Set and configure the file path box
        filepath = os.path.abspath(s.outputfilename)
        f.txtctlFile.ChangeValue(filepath)
        f.lblFile.SetLabel('Writing to file: ')

        h.Parent.Layout()

        try:
            s.start()
        except RuntimeError:
            pass
            # s.run()

    def on_reset(self, event):
        # f = self.fMainFrame
        # s = self.sthread
        # g = self.guithread

        # g.message = 'quit'
        # s.join()

        f = self.fMainFrame
        s = self.sthread
        s.datatoggle(False)

        self.enable_send_buffer(False)
        f.txtctlBuffer.Clear()
        f.lblFile.SetLabel('Wrote to file: ')

    def estimate_frequency(self, event):
        """
        Estimate the sampling frequency of the currently selected
        settings.
        """
        f = self.fMainFrame
        # Get parameter values
        fsrcn = f.rbFsrc.GetSelection()
        if fsrcn == 0:
            fsrcfreq = 400.
        elif fsrcn == 1:
            fsrcfreq = 200.
        else:
            raise ValueError            # Never get here
        dper = f.scDper.GetValue()

        # Estimate the sampling frequency
        estfreq = fsrcfreq/dper
        freqstring = str(estfreq)[:7] + ' Hz'

        # Set the text control value to reflect estimate
        f.txtctlFrequency.ChangeValue(freqstring)

    def check_port(self, event):
        """
        Check the port specified in cbPort to see if it's an active
        port talking to a PSyncADC board.
        """
        f = self.fMainFrame
        s = self.sthread

        f.txtctlPortCheck.ChangeValue('Checking port...')
        f.Layout()

        portname = f.cbPort.GetValue()
        s.portname = portname

        try:
            s.initializeport()
            f.txtctlPortCheck.ChangeValue('Port okay!')
            f.bInitialize.Enable(True)
        except (psyncadc.InitializationError,
                serial.SerialException):
            f.txtctlPortCheck.ChangeValue('Port failed.')
            f.bInitialize.Enable(False)

    def enable_send_buffer(self, state=True):
        """
        Enables or disables the entire send/receive block.
        """
        f = self.fMainFrame

        # Enable/disable send and receive buffer controls
        f.txtctlBuffer.Enable(state)
        f.txtctlSend.Enable(state)
        f.lblSend.Enable(state)
        f.bInitialize.Enable(not state)
        f.bReset.Enable(state)

        # Disable/enable settings controls
        f.lblComments.Enable(not state)
        f.txtctlComments.Enable(not state)
        f.lblPort.Enable(not state)
        f.cbPort.Enable(not state)
        f.rbFsrc.Enable(not state)
        f.lblDper.Enable(not state)
        f.scDper.Enable(not state)
        f.lblChannels.Enable(not state)
        f.txtctlChannels.Enable(not state)

    def on_inspector(self, event):
        """
        Import and launch the widget insepction tool.
        """
        import wx.lib.inspection
        wx.lib.inspection.InspectionTool().Show()


class RedirectText:
    """
    Creates a replacement for sys.stdout that allows messages to
    stdout to be redirected to the text control specified in the
    constructor.
    """
    def __init__(self, wxTextCtrl):
        self.out = wxTextCtrl

    def write(self, string):
        wx.CallAfter(self.out.AppendText, string)
