#ifndef __DISCRETE_INCLUDED
#define __DISCRETE_INCLUDED

#define BOARD_NAME  "DISCRETE"

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

//-----> OUTPUT PINS
#define PIN_LED                 LATCbits.LATC1	// Front pannel LED
#define PIN_LED2                LATCbits.LATC2	// Front pannel LED

#define PIN_485_ENABLE          LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

//------> INPUT PINS
#define PIN_ADDR0               PORTFbits.RF1
#define PIN_ADDR1               PORTGbits.RG1
#define PIN_ADDR2               PORTGbits.RG0
#define PIN_ADDR3               PORTFbits.RF0
#define PIN_ADDR4               PORTGbits.RG14  // Address high bit (jumper next to hex address switch)

#define PIN_SYNC_CLK        PORTDbits.RD3   // From the Backplane 96 pin connector
#define PIN_FRAME_CLK       PORTDbits.RD2   // From the Backplane 96 pin connector

// discrete.c
void init_pic( void );		// Init the PIC ports and hardware

// int_handler.c

// process_cmd.c
void process_cmd( char *str ) ;

#endif	// __DISCRETE_INCLUDED