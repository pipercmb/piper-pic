#include <p30F5011.h>
#include <dsp.h>
#include <spi.h>
#include <stdio.h>
#include <libpic30.h>

#include "discrete.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & XT_PLL16); /* XT with 8xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF); /* Code protect disabled */


/* Global variables */
unsigned char CardAddress;

/*
 * Set all the GPIO leads on the pic to be either inputs or outputs
 * as needed.
 *
 * Set up the internal pic hardware, like the timers to generate square wave
 * for the convert input of the ADC.
 *
 * Set up the PIC SPI and UART (RS-232) port
 */
void init_pic()
{
//    char str[100];
	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;
	
	// Set the direction for GPIO pins
//	TRISB = 0x20FF;
//	TRISC = 0xA004;
//	TRISD = 0x0CED;
//	TRISF = 0x007F;
//	TRISG = 0xF3CF;
    
	// Set up values for GPIO pins
//	PORTCbits.RC1 = 1; // Turn on the LED

	// Set the direction for GPIO
	TRISCbits.TRISC14 = 0; // Set the RS-485 Enable pin as an output
	TRISCbits.TRISC1 = 0; // Set LED1 as an output
	TRISCbits.TRISC2 = 0; // Set LED2 as an output


//	PIN_LED1 = 0; // turn off LED

	// Short delay to wait for power supply voltages to settle
	__delay32((long) (RESET_DELAY_SECONDS * CLOCKFREQ));
	
	// Setup the SPI port
/*	OpenSPI2(FRAME_ENABLE_OFF & FRAME_SYNC_OUTPUT & ENABLE_SDO_PIN & SPI_MODE16_ON &
             MASTER_ENABLE_ON & SEC_PRESCAL_1_1 &
             PRI_PRESCAL_4_1,
             SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR);
    ConfigIntSPI2(SPI_INT_DIS); // NO SPI interrupts
	SPI2CONbits.CKE = 1; // CKE=1 (see above)
	SPI2CONbits.CKP = 0; // CKP=0 means SCLK is active high
	SPI2CONbits.SMP = 1; // If the SMP bit is set, then the input sampling is done at the end of the bit output
*/
	SPI2CON = 0x073E;	// SPI2 Control Register
	/*                              // bit <15> Unimplemented read as '0'
	 * SPI2CONbits.FRMEN = 0;		// bit <14> Framed SPI support disabled
	 * SPI2CONbits.SPIFSD = 0;		// bit <13> Frame sync pulse output (master)
	 *                              // bit <12> Unimplemented read as '0'
	 * SPI2CONbits.DISSDO = 0;		// bit <11> Enable SDO2 pin
	 * SPI2CONbits.MODE16 = 1;		// bit <10> Communication is word-wide (16 bits)
	 * SPI2CONbits.SMP = 1;			// bit <9> Input data sampled at end of data output time
	 * SPI2CONbits.CKE = 1;			// bit <8> SDO changes on transition from active to idle SCK
	 * SPI2CONbits.SSEN = 0;		// bit <7> Slave select enable bit
	 * SPI2CONbits.CKP = 0;			// bit <6> Idle state for clock is low, active state is high
	 * SPI2CONbits.MSTEN = 1;		// bit <5> Master mode enable bit
	 * SPI2CONbits.SPRE = 0b111;	// bit <4-2> Secondary Prescale 1:1
	 * SPI2CONbits.PPRE = 0b10;		// bit <1-0> Primary Prescale 4:1
	 */
	SPI2STAT = 0x8000;	// SPI2 Status Register
    /* SPI2STATbits.SPIEN = 1;      // bit <15> Enable SPI module
     *                              // bit <14> Unimplemented read as '0'
     * SPI2STATbits.SPISIDL = 0;	// bit <13> Continue operation in Idle mode
     *                              // bit <12-7> Unimplemented read as '0'
     * SPI2STATbits.SPIROV = 0;     // bit <6> Receive overflow flag bit
     *                              // bit <5-2> Unimplemented read as '0'
     * SPI2STATbits.SPITBF = 0;     // bit <1> SPI transmit buffer full status bit
     * SPI2STATbits.SPIRBF = 0;     // bit <0> SPI receive buffer full status bit
     */

    // Setup the UART with our standard backplane settings
//	config_uart2();
	U2BRG = BRG_SETTING;	// U2BRG = 10; // 115200 baud rate
	U2MODE = 0x8080;	// UART2 Mode Register
	/* U2MODEbits.UARTEN = 1;		// bit <15> UART2 enable, U2TX/U2RX pins controlled by UART2
	 *								// bit <14> Unimplemented read as '0'
	 * U2MODEbits.USIDL = 0;		// bit <13> Continue operation in Idle mode
	 *								// bit <12> Unimplemented read as '0'
	 *								// bit <11-8> Reserved write '0'
	 * U2MODEbits.WAKE = 1;			// bit <7> Wake-up enabled
	 * U2MODEbits.LPBACK = 0;		// bit <6> Loopback Mode disabled
	 * U2MODEbits.ABAUD = 0;		// bit <5> Input to Capture module from ICx pin
	 *								// bit <4-3> Unimplemented read as '0'
	 * U2MODEbits.PDSEL1 = 0;		// bit <2> 8-bit data, no parity
	 * U2MODEbits.PDSEL0 = 0;		// bit <1> 8-bit data, no parity
	 * U2MODEbits.STSEL = 0;		// bit <0> 1 stop bit
	 */
	U2STA = 0x0510;	// UART2 Status & Control Register
	/* U2STAbits.UTXISEL = 0;		// bit <15> Interrupt when a character is transferred to Transmit Shift Register
	 *								// bit <14-12> Unimplemented read as '0'
	 * U2STAbits.UTXBRK = 0;		// bit <11> U2TX pin operates normally
	 * U2STAbits.UTXEN = 1;			// bit <10> UART2 Transmitter enabled, U2TX pin controlled by UART2
	 * U2STAbits.UTXBF = 0;			// bit <9> Transmit Buffer Full Status bit (read only)
	 * U2STAbits.TRMT = 1;			// bit <8> Transmit Shift Register Empty bit (read only)
	 * U2STAbits.URXISEL1 = 0;		// bit <7> Interrupt flag bit is set when character is received
	 * U2STAbits.URXISEL0 = 0;		// bit <6> Interrupt flag bit is set when character is received
	 * U2STAbits.ADDEN = 0;			// bit <5> Address detect mode disabled
	 * U2STAbits.RIDLE = 1;			// bit <4> Receive Idle bit (read only)
	 * U2STAbits.PERR = 0;			// bit <3> Parity Error Status bit (read only)
	 * U2STAbits.FERR = 0;			// bit <2> Framing Error Status bit (read only)
	 * U2STAbits.OERR = 0;			// bit <1> Receive Buffer Overrun Error Status bit (read/clear only)
	 * U2STAbits.URXDA = 0;			// bit <0> Receive Buffer Data Available bit (read only)
	 */

    // Read in our card address and store in global variable
    CardAddress = read_address_pins();

    // Attempt to load stored configuration settings from EEPROM
//    load_settings();

    // Before we start the interrupt handler, wait for a rising edge on external frame clock
    // This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
    // Do this just before turning on the timer interrupt.
    sync_to_frame_clock();

	// Setup Timer 2 and set its period to 5000
	TMR2 = 0;
	PR2 = CYCLES_PER_TICK - 1;	// Note, PIC timer counts from 0 to period, inclusive, so need to sub 1 to get desired sampling / tick rate
	T2CON = 0x8000;	// T2CON Type B Time Base Register
	/* T2CONbits.TON = 1;			// bit <15> Timer2 On
	 *								// bit <14> Unimplemented read as '0'
	 * T2CONbits.TSIDL = 0;			// bit <13> Continue operation in Idle Mode
	 *								// bit <12-7> Unimplemented read as '0'
	 * T2CONbits.TGATE = 0;			// bit <6> Timer gated time accumulation disabled
	 * T2CONbits.TCKPS = 0b00;		// bit <5-4> Timer Input Clock Prescaler (1:1)
	 * T2CONbits.T32 = 0;			// bit <3> 32-bit Timer Mode Select bits
	 *								// bit <2> Unimplemented read as '0'
	 * T2CONbits.TCS = 0;			// bit <1> Internal clock (Fcyc=Fosc/4)
	 *								// bit <0> Unimplemented read as '0'	
	 */
    
	// Open the output compare module to generate 10 usec pulses, with interrupts on FALLING edge
	// Setup timer 2 and set its period (exact period depends on configuration, but it will be roughly a few KHZ)
	// turn on ~4 kHz interrupt handler.  Do this last.
//	config_timer2();

	// Setup Output Compare 2 module to generate 10 usec pulses, with interrupts on FALLING edge
	OC2CONbits.OCM = 0;	// turn off OC2 before switching to new mode
	OC2RS = CYCLES_PER_ADC_CONVERT_PULSE;	// OC2RS Secondary Register
	OC2R = 0;	// OC2R Main Register
	OC2CON = 0x0005;	//Output Compare 2 Control Register
	/*								// bit <15-14> Unimplemented read as '0'
	 * OC2CONbits.OCSIDL = 0;		// bit <13> Continue operation in Idle Mode
									// bit <12-5> Unimplemented read as '0'
	 * OC2CONbits.OCFLT = 0;		// bit <4> No PWM Fault condition has occurred (only used when OCM-2:0> = 0b111)
	 * OC2CONbits.OCTSEL = 0;		// bit <3> Timer 2 is clock source of Output Compare 2
	 * OC2CONbits.OCM = 0b101;		// bit <2-0> Initialize OC2 pin low, generate continuous output pulses on OC2 pin
	 */
	TMR2 = (3*CYCLES_PER_TICK/4);	// Clear the timer.  This value sets the phase of the OC2 interrupts with respect to the frame_clk
									// By setting to (3/4)*cycles_per_tick we wait 1/4 period before our first OC2 interrupt.
									// This gives some time before the first interrupt, and extra time to the last tick of the frame
									// The last tick often has to write a bunch of data (esp. DSPID), so needs some extra time

	// Finally, turn on interrupts!
	IFS0bits.OC2IF = 0;		// Clear Output Compare 2 Interrupt Flag Status bit
	IPC1bits.OC2IP = 2;		// Assign Priority 2 for Output Compare 2 Interrupt
	IEC0bits.OC2IE = 1;		// Enable Output Compare 2 Interrupt

}

int main(void)
{
    // Setup the PIC internal peripherals
    init_pic();
    
    // Setup our data frame buffers
    init_frame_buf_backplane('E', CardAddress);

    // Main loop, wait for command from user, do it, repeat forever...
    while (1) {
        char s[80];

        cmd_gets(s, sizeof (s)); // Get a command from the user
        //process_cmd(s); // Deal with the command
    }
}