#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>
#include <math.h>

#include "pmotor-pid.h"

// Local function prototypes
static char do_cmd_pid_group_set(void);//Change PID settings to predetermined ones

static char do_cmd_stop(void); //Set mode to free run set amplitude to stop
static char do_cmd_cdac(void); // Set the CDAC.

// Position PID commands
static char do_cmd_pid_s(void); // Adjust setpoint
static char do_cmd_pid_sign(void); // Set the sign for pterm and iterm
static char do_cmd_pid_p(void); // Adjust proportional factor
static char do_cmd_pid_i(void); // Adjust integral factor
static char do_cmd_pid_d(void); // Adjust differential factor
static char do_cmd_pid_p_shift(void); // Adjust fixed point basis for p
static char do_cmd_pid_i_shift(void); // Adjust fixed point basis for i
static char do_cmd_pid_d_shift(void); // Adjust fixed point basis for d
static char do_cmd_pid_on(void); // Enable the PID loop
static char do_cmd_pid_off(void); // Terminate the PID loop
static char do_cmd_ff_on(void); // Enable the FF loop
static char do_cmd_ff_off(void); // Enable the FF loop
static char do_cmd_ff_a(void); // Enable the FF loop
static char do_cmd_ff_b(void); // Enable the FF loop
static char do_cmd_ff_c(void); // Enable the FF loop
static char do_cmd_pid_reset(void); // Terminate the PID loop

static char do_cmd_sqwave(void);

static char do_cmd_sinewave(void);
static char do_cmd_sinewave_amp(void);
static char do_cmd_sinewave_freq_hz(void);
static char do_cmd_sinewave_freq_cpf(void);
static char do_cmd_sinewave_offset(void);
static char do_cmd_sinewave_phase(void);
static char do_cmd_sinewave_sync(void);
static char do_cmd_hit_stop_reset(void);

// Diagnostic commands

static char do_cmd_led(void); // turn LED on or off
static char do_cmd_p(void); // prints status
static char do_cmd_echo(void); // echo on / off
static char do_cmd_data(void); // data on / off
static char do_cmd_debug1(void); // debug command 1

// helper functions
static void talk(char *s);      // Talks out the port, unless the silent flag is set




struct pidparams_both pid_groups[] = {
    {.vel_p =0, .vel_i=0, .vel_sign=0, .vel_sign=0, .vel_i_shift=0,.vel_p_shift=0,
    .pos_p=0, .pos_d=0, .pos_i=0, .pos_p_shift=0, .pos_d_shift=0, .pos_i_shift=0,
    .vel_ramp_delay=0, .vel_ramp_delta=0}, //ARRAY 0, All values 0

    {.vel_p =15, .vel_i=2, .vel_sign=0, .vel_sign=0, .vel_i_shift=5,.vel_p_shift=0,
    .pos_p=200, .pos_d=0, .pos_i=0, .pos_p_shift=0, .pos_d_shift=0, .pos_i_shift=0,
    .vel_ramp_delay=0, .vel_ramp_delta=0}, //ARRAY 1,Good for unloaded motor

     {.vel_p =25, .vel_i=35, .vel_sign=0, .vel_sign=0, .vel_i_shift=3,.vel_p_shift=0,
    .pos_p=200, .pos_d=0, .pos_i=0, .pos_p_shift=0, .pos_d_shift=0, .pos_i_shift=0,
    .vel_ramp_delay=15, .vel_ramp_delta=1} //ARRAY 2,Good for Big Wheel
};
/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */

void process_cmd(char *cmd)
{
    char *s;
    unsigned char r;

    // Parse the command name and check the address
    if( PIN_CLK_MODE_INTERNAL ) {
        cmd_parse(cmd);         // Parse the command string
        s = tok_get_str();      // first token is the command name
    } else {
        // Parse command string and check address to see if it's for us
        s = bp_parse_cmd_string(cmd);
        if( s == NULL )     // Command is not for us
            return;
    }

    // Discard empty commands
    if (!strcmp(s, ""))
        return;

    if (!strcmp(s,"ppg")) r = do_cmd_pid_group_set();

    else if (!strcmp(s, "ps")) r = do_cmd_pid_s(); // Changes PID setpoint
    else if (!strcmp(s, "psign")) r = do_cmd_pid_sign(); // Changes P and I sign
    else if (!strcmp(s, "pp")) r = do_cmd_pid_p(); // Changes PID p constant
    else if (!strcmp(s, "pi")) r = do_cmd_pid_i(); // Changes PID i constant$
    else if (!strcmp(s, "pd")) r = do_cmd_pid_d(); // Changes PID d constant
    else if (!strcmp(s, "pp_shift")) r = do_cmd_pid_p_shift(); // Changes PID p fixed point basis
    else if (!strcmp(s, "pi_shift")) r = do_cmd_pid_i_shift(); // Changes PID i fixed point basis
    else if (!strcmp(s, "pd_shift")) r = do_cmd_pid_d_shift(); // Changes PID d fixed point basis
    else if (!strcmp(s, "pon")) r = do_cmd_pid_on(); // Initializes a PID loop
    else if (!strcmp(s, "poff")) r = do_cmd_pid_off(); // Terminate the PID loop
    else if (!strcmp(s, "preset")) r = do_cmd_pid_reset(); // Reset PID integrator

    else if (!strcmp(s, "ffon")) r = do_cmd_ff_on(); // Feedforward on
    else if (!strcmp(s, "ffoff")) r = do_cmd_ff_off(); // Feedforward off
    else if (!strcmp(s, "ffa")) r = do_cmd_ff_a(); // Feedforward "a" parameter
    else if (!strcmp(s, "ffb")) r = do_cmd_ff_b(); // Feedforward "b" parameter
    else if (!strcmp(s, "ffc")) r = do_cmd_ff_c(); // Feedforward "b" parameter

    else if (!strcmp(s, "sq")) r = do_cmd_sqwave(); // Square wave PID setpoint

    else if (!strcmp(s, "sw")) r = do_cmd_sinewave(); // Sine wave PID setpoint on / off
    else if (!strcmp(s, "swa")) r = do_cmd_sinewave_amp(); // Sine wave amplitude
    else if (!strcmp(s, "swfhz")) r = do_cmd_sinewave_freq_hz(); // Sine wave frequency Hz
    else if (!strcmp(s, "swfcpf")) r = do_cmd_sinewave_freq_cpf(); // Sine wave frequency Cycles per Frame
    else if (!strcmp(s, "swo")) r = do_cmd_sinewave_offset(); // Sine wave offset
    else if (!strcmp(s, "swp")) r =do_cmd_sinewave_phase(); //set the phase of the sinewave

    else if (!strcmp(s, "stop")) r = do_cmd_stop(); //set mode to free run set amplitude to stop
    else if (!strcmp(s, "reset stop")) r = do_cmd_hit_stop_reset();

    //killed mdac command since it couldn't actually do anything
    else if (!strcmp(s, "cdac"))    r = do_cmd_cdac(); // output a value to the coil dac

    else if (!strcmp(s, "led"))
        r = do_cmd_led(); // set the LED
    else if (!strcmp(s, "p")) // Full print command
        r = do_cmd_p();
    else if (!strcmp(s, "echo"))
        r = do_cmd_echo(); // Sets echo on or off
    else if (!strcmp(s, "data"))
        r = do_cmd_data(); // Sets data on or off
    else if (!strcmp(s, "d1"))
        r = do_cmd_debug1(); // Debug1 command -- who knows what this does???  Try it!  You'll get a kick out of it!
    else
        r = STATUS_FAIL_CMD; // User typed unknown command

    // Print the status, unless we're taking data
    if( !get_data_mode() )
        print_status(r);
}

void talk( char *s )
{
    if( get_data_mode() != DATA_MODE_RAW )
        TTYPuts(s);
}

static char do_cmd_pid_group_set(void)
{
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    char *arg = tok_get_str();
    int array_index;
    if(!strcmp(arg,"zero"))
        array_index = 0;
    else if(!strcmp(arg,"no_load"))
        array_index = 1;
    else if(!strcmp(arg,"big_wheel"))
        array_index = 2;
    else
        return STATUS_FAIL_INVALID;
    
    set_pid_parameter_group(pid_groups[array_index]);
 
    return STATUS_OK;

}

/*-----------------------------------------------------------
 * PID Commands
 *-------------------------------------------------------------*/

/** PPS
 *      Adjust the setpoint of current PID loop
 *
 *      Arguments:
 *      <sp=int32>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_s(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(9, 1, 0))
        return STATUS_FAIL_INVALID;
    long setpoint = tok_get_int32();

    set_pid_mode(PID_MODE_NORMAL);
    set_pid_setpoint(setpoint);

    sprintf(str, "*PMP: pid_s %ld\r\n", setpoint);
    talk(str);

    return STATUS_OK;
}

/** PPSIGN
 *      Adjust the sign (pos or neg) of the P and I terms of the PID loop.
 *
 *      Arguments:
 *      "pos" or "neg"
 *
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_sign(void)
{
    char str[100];

    if (!tok_available())
        return STATUS_FAIL_NARGS;

    char *arg = tok_get_str();
    if (!strcmp(arg, "pos")) {
        set_pid_sign(1);
    } else if (!strcmp(arg, "neg")) {
        set_pid_sign(0);
    } else
        return STATUS_FAIL_INVALID;

    sprintf(str, "*PMP: pid_sign %s\r\n", arg);
    talk(str);

    return STATUS_OK;
}

/** PPP
 *      Adjust the p constant of current PID loop
 *
 *      Arguments:
 *      <p=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_p(void)
{
    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p = (unsigned int) tok_get_int32();

    set_pid_p(p);

    sprintf(str, "*PMP: pid_p\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPP_SHIFT
 *      Adjust the fixed point basis of the proportional term of the PID loop
 *
 *      Arguments:
 *      <p_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the proportional term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_p_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int p_shift = (unsigned int) tok_get_int32();

    set_pid_p_shift(p_shift);

    sprintf(str, "*PMP: pid_p_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPI
 *      Adjust the i constant of current PID loop
 *
 *      Arguments:
 *      <i=uint16>
 *
 *      Changes current PID loop's setpoint. Does not reinitialize PID loop
 *      or change any other PID parameters.
 */
static char do_cmd_pid_i(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i = (unsigned int) tok_get_int32();

    set_pid_i(i);

    sprintf(str, "*PMP: pid_i\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPI_SHIFT
 *      Adjust the fixed point basis of the integral term of the PID loop
 *
 *      Arguments:
 *      <i_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the integral term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_i_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int i_shift = (unsigned int) tok_get_int32();

    set_pid_i_shift(i_shift);

    sprintf(str, "*PMP: pid_i_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPD
 *      Adjust the d constant of current PID loop
 *
 *      Arguments:
 *      <d=uint16>
 *
 *      Changes current PID loop's derivative coefficient. Does not reinitialize
 *      PID loop or change any other PID parameters.
 */
static char do_cmd_pid_d(void)
{
    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int d = (unsigned int) tok_get_int32();

    set_pid_d(d);

    sprintf(str, "*PMP: pid_d\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPD_SHIFT
 *      Adjust the fixed point basis of the differential term of the PID loop
 *
 *      Arguments:
 *      <d_shift=uint16>
 *
 *      Changes current PID loop's fixed point basis for the differential term.
 *      Does not reinitialize PID loop or change any other PID parameters.
 */
static char do_cmd_pid_d_shift(void)
{
    char str[100];

    // Get low value of sweep
    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;
    unsigned int d_shift = (unsigned int) tok_get_int16();

    set_pid_d_shift(d_shift);

    sprintf(str, "*PMP: pid_d_shift\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPON
 *      Enable the PID loop.
 *
 *      Arguments:
 *      None
 *
 *      Starts the PID loop.
 */
static char do_cmd_pid_on(void)
{
    char str[100];
   
    set_sine_phase_raw(0);  // Reset the phase every time you turn on the PID loop to make sure it's synchronizable with other boards.
    set_mode(MODE_PID);

    sprintf(str, "*PMP: mode = PID\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPOFF
 *      Terminate the PID loop.
 *
 *      Arguments:
 *      None
 *
 *      Stops the PID loop. Does not re-initialize anything, since the PID
 *      initialization command initializes the PID variables.
 */
static char do_cmd_pid_off(void)
{
    char str[100];

    set_mode(MODE_FREERUN);

    sprintf(str, "*PMP: mode FREERUN\r\n");
    talk(str);

    return STATUS_OK;
}

/** PPRESET
 *      Resets the accumulator.
 *
 *      Arguments:
 *      None
 */
static char do_cmd_pid_reset(void)
{
    char str[100];

    set_pid_reset();

    sprintf(str, "*PMP: pid_reset\r\n");
    talk(str);

    return STATUS_OK;
}

static char do_cmd_stop() //Get out of whatever mode your in and set amplitude to zero
{
    char str[100];
    set_mode(MODE_FREERUN);
    sprintf(str, "*PMP: Stop!\r\n");
    talk(str);

    return STATUS_OK;
}

/*-----------------------------------------------------------
 * Feedforward Commands
 *-------------------------------------------------------------*/

/** FFON
 *      Enables feedforward (no arguments)
 */
static char do_cmd_ff_on(void)
{
    set_ff_enable(1);
    talk("*PMP: Feedforward on\r\n");
    return STATUS_OK;
}

/** FFOFF
 *      Disables feedforward (no arguments)
 */
static char do_cmd_ff_off(void)
{
    set_ff_enable(0);
    talk("*PMP: Feedforward off\r\n");
    return STATUS_OK;
}

/** FFA
 *      Set the "a" parameter (acceleration coefficient) for the feedforward control
 *
 *      Arguments:
 *      <a=int16>
 */
static char do_cmd_ff_a(void)
{
    char str[100];

    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int a = tok_get_int16();

    set_ff_a(a);

    sprintf(str, "*PMP: set ff a = %d\r\n", a);
    talk(str);

    return STATUS_OK;
}

/** FFB
 *      Set the "b" parameter (velocity coefficient) for the feedforward control.
 *
 *      Arguments:
 *      <b=int16>
 */
static char do_cmd_ff_b(void)
{
    char str[100];

    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int b = tok_get_int16();

    set_ff_b(b);

    sprintf(str, "*PMP: set ff b = %d\r\n", b);
    talk(str);

    return STATUS_OK;
}

/** FFC
 *      Set the "c" parameter (constant coefficient) for the feedforward control.
 *
 *      Arguments:
 *      <c=int16>
 */
static char do_cmd_ff_c(void)
{
    char str[100];

    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int c = tok_get_int16();

    set_ff_c(c);

    sprintf(str, "*PMP: set ff c = %d\r\n", c);
    talk(str);

    return STATUS_OK;
}

/*-----------------------------------------------------------
 * Setpoint waveform commands
 *-------------------------------------------------------------*/

/** SQ
 *      Sets the square wave setpoint parameters
 *
 *      Arguments:
 *      <min=int16> or <mode=str> (on/off)
 *      <max=int16>
 *      <period=float>
 *
 *      Sets the square wave parameters. min is the low DAC value. max is the
 *      high DAC value. period is the period of the square wave in seconds.
 *
 *      Also used to turn on/off the square wave.
 *
 *      Ex:
 *
 *      sq -5000 2500 1.25
 *      sq on
 *      sq off
 */
static char do_cmd_sqwave(void)
{
    char str[100];

    if( !tok_available() )
        return STATUS_FAIL_NARGS;
    
    // Get low value of square wave
    if (!tok_valid_num(5, 1, 0)) {
        char* mode = tok_get_str();
        if (!strcmp(mode, "on")) {
            reset_sqwave();
            set_pid_mode(PID_MODE_SQUAREWAVE);
        }
        else if (!strcmp(mode, "off"))
            set_pid_mode(PID_MODE_NORMAL);
        else
            return STATUS_FAIL_INVALID;
        sprintf(str, "*PMP: sq %s\r\n", mode);
        talk(str);
        
        return STATUS_OK;
    }

    int min = tok_get_int16();

    // Get high value of square wave
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int max = tok_get_int16();

    // Get period in seconds
    if (!tok_valid_num(8, 0, 1))
        return STATUS_FAIL_INVALID;
    float fp = tok_get_float();

    unsigned halfperiod = (unsigned)(fp / 2.0 * PID_UPDATE_RATE);
    set_sqwave(min, max, halfperiod);

    sprintf(str, "*PMP: sq %d %d %f (%d)\r\n", min, max, fp, halfperiod);
    talk(str);

    return STATUS_OK;
}


/** SW
 *      Sets the sine wave setpoint on or off
 *
 *      Arguments:
 *      <mode=str> (on/off)
 *
 *      Examples:
 *
 *      sw on
 *      sw off
 */
static char do_cmd_sinewave(void)
{
    char str[100];

    if( !tok_available() )
        return STATUS_FAIL_NARGS;

    char* mode = tok_get_str();
    if (!strcmp(mode, "on")) {
        set_pid_mode(PID_MODE_SINEWAVE);
    }
    else if (!strcmp(mode, "off"))
        set_pid_mode(PID_MODE_NORMAL);
    else
        return STATUS_FAIL_INVALID;

    sprintf(str, "*PMP: sw %s\r\n", mode);
    talk(str);

    return STATUS_OK;
}
/** SWP
 * Sets the phase offset of the sinewave (in degrees)
 *    Arguements:
 *    <phase=float>
 */

static char do_cmd_sinewave_phase(void)
{
    char str[100];

    if (!tok_valid_num(6, 0, 1))
        return STATUS_FAIL_INVALID;

    float phase = tok_get_float();
    if(phase>=360)
        return STATUS_FAIL_INVALID;


    set_sine_phase(phase);

    sprintf(str, "*PMP: swp %f\r\n",phase);
    talk(str);

    return STATUS_OK;
}


/** SWA
 *      Sets the sine wave PID setpoint amplitude
 *
 *      Arguments:
 *      <amp=int16>
 *
 *      Ex:
 *      swa 1000
 *      swa 13456
 */
static char do_cmd_sinewave_amp(void)
{
    char str[100];

    if (!tok_valid_num(5, 0, 0))
        return STATUS_FAIL_INVALID;

    int amp = tok_get_int16();

    set_sine_amp(amp);

    sprintf(str, "*PMP: swa %d\r\n", amp);
    talk(str);

    return STATUS_OK;
}

/** SWFHZ
 *      Sets the sine wave PID setpoint frequency in Hz
 *
 *      Arguments:
 *          <freq=float>
 *
 *      Ex:
 *      swf 3.00
 *      swf 0.250
 */
static char do_cmd_sinewave_freq_hz(void)
{
    char str[100];

    if (!tok_valid_num(8, 0, 1))
        return STATUS_FAIL_INVALID;

    float hz = tok_get_float();

    set_sine_freq_hz(hz);

    float cpf = hz / FRAME_RATE;
    
    sprintf(str, "*PMP: swf %f Hz (%f cycles per frame)\r\n", hz, cpf);
    talk(str);

    return STATUS_OK;
}

/** SWFCPF
 *      Sets the sine wave PID setpoint frequency in Hz
 *
 *      Arguments:
 *          <freq=float>
 *
 *      Ex:
 *      swf 3.00
 *      swf 0.250
 */
static char do_cmd_sinewave_freq_cpf(void)
{
    char str[100];

    if (!tok_valid_num(3, 0, 0))
        return STATUS_FAIL_INVALID;

    int cpf = tok_get_int16();

    set_sine_freq_cpf(cpf);

    float hz = (float)cpf * FRAME_RATE;

    sprintf(str, "*PMP: swfcpf %f cycles per frame (%f Hz)\r\n", cpf, hz);
    talk(str);

    return STATUS_OK;
}


/** SWO
 *      Sets the sine wave PID setpoint offset in bits
 *
 *      Arguments:
 *          <offset=int>
 *
 *      Ex:
 *      swo 10000
 *      swf 0
 *      swf -5000
 */
static char do_cmd_sinewave_offset(void)
{
    char str[100];

    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;

    int offset = tok_get_int16();

    set_sine_offset(offset);

    sprintf(str, "*PMP: swo %d\r\n", offset);
    talk(str);

    return STATUS_OK;
}

static char do_cmd_sinewave_sync(void)
{
    
}


/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/

/** P
 * Prints current readings from all channels
 */
static char do_cmd_p(void)
{
    char str[100]; // max string len is about 51 chars, so 100 is very safe

    unsigned char mode = get_mode();

    int adc_value;
    int i;
    for (i=0; i<NUM_ADC_CHANNELS; i++)
    {
        adc_value = get_adc_value(i);
        sprintf(str, "ADC%d = %d\r\n", i, adc_value);
        talk(str);
    }
   
    if (mode == MODE_PID) {
        talk("PSG: MODE = PID_POSITION\r\n");
        pid_status();
        ff_status();
    }
    else {
        talk("PSG: MODE = FREERUN\r\n");
        pid_status();
        ff_status();
    }
    return STATUS_OK;
}


/** CDAC
 *	Command for setting a value to the CDAC.
 *
 *	Usage:
 *	cdac 1000-> Send a decimal 1000 to the CDAC
 */
static char do_cmd_cdac(void)
{
    // Get the channel
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    // Get the dac value
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    if (!tok_valid_num(5, 1, 0))
        return STATUS_FAIL_INVALID;
    int val = tok_get_int16();

    // Write the value to the DAC channel
    set_cdac(val);

    return STATUS_OK;
}

/** LED
 *	Set LED light.
 *
 *	Usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led(void)
{
    char *arg;

    // Check that the next token is a valid number
    if (!tok_available())
        return STATUS_FAIL_NARGS;

    arg = tok_get_str();
    if (!strcmp(arg, "on")) {
        PIN_LED1 = 1;
    } else if (!strcmp(arg, "off")) {
        PIN_LED1 = 0;
    } else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** ECHO
 *	Sets echo on or off.
 *
 *	Usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if (!strcmp(arg, "on"))
        cmd_set_echo(1);
    else if (!strcmp(arg, "off"))
        cmd_set_echo(0);
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** DATA
 *	Sets backplane data packet reporting on or off.
 *
 *	Usage:
 *	data on		-> sets data on
 *	data off	-> sets data off
 *	data raw	-> enables lightning mode
 */
static char do_cmd_data(void)
{
    char *arg;

    // Check that the next token is available
    if (!tok_available())
        return STATUS_FAIL_NARGS;
    arg = tok_get_str();
    if (!strcmp(arg, "on"))
        set_data_mode(DATA_MODE_ON);
    else if (!strcmp(arg, "raw"))
        set_data_mode(DATA_MODE_RAW);
    else if (!strcmp(arg, "off"))
        set_data_mode(DATA_MODE_OFF);
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** DEBUG1
 *	First debug command.
 *
 *	Usage:
 *	???
 */
static char do_cmd_debug1(void)
{
    char str[100];

//    // Get value of DAC to set
//    if (!tok_valid_num(8, 1, 1))
//        return STATUS_FAIL_INVALID;
//    int a = tok_get_int16();

    DDacWrite(DACREG_DAC, 3, 0);


    sprintf(str, "*PMP: debug1 -- Reset!\r\n");
    talk(str);

    return STATUS_OK;
}
/*
 * reset the auto stop 
*/
static char do_cmd_hit_stop_reset(void){
    char str[100];
    reset_hit_stop();
    sprintf(str, "*PMP: reset hit\r\n");
}

/* This function is really defined in common_functions.c
 * But we can't include that file in this project because it references a bunch of other stuff
 * that we don't need / want for this board.  Should really move print_status to its own
 * file in the /common directory.
 *
 * Print a human readable status message depending on the status code.
 */
void print_status(char status)
{
    switch (status) {
    case STATUS_OK:
        TTYPuts("OK");
        break;
    case STATUS_FAIL_CMD:
        TTYPuts("FAIL: unknown command");
        break;
    case STATUS_FAIL_NARGS:
        TTYPuts("FAIL: incorrect number of arguments");
        break;
    case STATUS_FAIL_INVALID:
        TTYPuts("FAIL: invalid argument format");
        break;
    case STATUS_FAIL_RANGE:
        TTYPuts("FAIL: argument out of range");
        break;
    case STATUS_FAIL_COULD_NOT_COMPLETE:
        TTYPuts("FAIL: the requested operation could not be completed");
        break;
    default:
        TTYPuts("FAIL: unknown error");
        break;
    }

    // All responses must end with ! to signal parser this is end of response
    TTYPuts("!\r\n");
}

