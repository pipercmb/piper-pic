#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "../common/math_utilities.h"
#include "pmotor-pid.h"

// ---------------------
// ----VARIABLE DEFS----
// ---------------------

struct pidparams {
    unsigned p; // P term for PID
    unsigned i; // I term for PID
    unsigned d;  // D term for PID
    int setpoint; // PID setpoint (ADC units from cap sensor)
    int setpoint_d1; // PID setpoint first derivative (actually just proportional to 1st derivative)
    int setpoint_d2; // PID setpoint second derivative (actually just proportional to 2nd derivative)
    long accumulator; // PID integrator term accumulator
    unsigned char sign; // Sign of P term and I term
    unsigned char i_shift; // I term is divided by 2^i_shift
    unsigned char p_shift; // P term is divided by 2^p_shift
    unsigned char d_shift; // D term is divided by 2^d_shift
    int amp_init; // Initial amplitude
};

struct sqwaveparams {
    int min;
    int max;
    unsigned halfperiod;
    unsigned counter;
};

static long error;
static long pterm;
static long iterm;
static long dterm;
static int vel_last;


/* Parameters for Flight Reaction Wheel
     p = 13, p_shift = 4, i = 0, dterm=70, d_shift = 0

 Parameters for bare motor
 p = 200, p_shift = 0, i = 0, d = 0
 */

/* First guess at parameters for the MTM
PSG: p = 10, p_shift = 4, p_term = 1
PSG: i = 2, i_shift = 8, i_term = 7179
PSG: v = 34, d = 0, d_shift = 0, d_term = 0

 Second guess:
 *
 * PSG: s = 5000, e = -5
PSG: accum = 513706, sign = 0
PSG: p = 10, p_shift = 3, p_term = 6245
PSG: i = 3, i_shift = 6, i_term = 7134
PSG: v = -1772, d = 600, d_shift = 8, d_term = -4154
 *
 * For non-reactionless VPM assembly
 *
 * PSG: p = 10, p_shift = 4, p_term = -2200
PSG: i = 4, i_shift = 10, i_term = 17159
PSG: v = 564, d = 16, d_shift = 2, d_term = 2256
 */
static struct pidparams pid = {
    .p = 20,
    .i = 2,  // Good (possibly?): i=1, i_shift = 12
    .d = 16,
    .setpoint = 0,
    .accumulator = 0,
    .sign = 0,  // 0 = negative, 1 = positive
    .p_shift = 4,
    .i_shift = 10,
    .d_shift = 2,
    .amp_init = 0};

static struct sqwaveparams sq = {
    .min = -1000,
    .max = 1000,
    .halfperiod = 250,
    .counter = 0
};

int compute_pid(int pos, int vel)
{
    static long sum;
    static int toret;
    vel_last = vel;

    error = (long)pos - (long)pid.setpoint;
    //error = -(long)pos - (long)pid.setpoint; //polartiy reverse on drive

    // NOTE: pid.sign is handled here for i coefficient so that accumulator is
    // always continuous.
    long toadd;

    if (pid.sign)
        toadd = pid.i * error;
    else
        toadd = -(pid.i * error);
    // #DELME
//    if (pid.sign)
//        toadd = error;
//    else
//        toadd = -error;
    // END #DELME

//    // Clip to prevent accumulator from overflowing
//    if( toadd >= 0 ) {
//        long max_add = LONG_MAX - pid.accumulator;
//        if (toadd >= max_add)
//            toadd = max_add;
//    } else {
//        long nmax_add = LONG_MIN - pid.accumulator;
//        if (toadd <= nmax_add)
//            toadd = nmax_add;
//    }
//
    pid.accumulator += toadd;
//    pid.accumulator = safe_add(pid.accumulator, toadd);

    if (pid.p != 0) {
        pterm = pid.p * error;

        pterm = fast_rshift_long(pterm, pid.p_shift);
    } else {
        pterm = 0;
    }

    // Compiler handles this switch structure by indexing i_shift into a look-up BRA table
    // so there is no performance cost to having all of the cases
    iterm = fast_rshift_long(pid.accumulator, pid.i_shift);

    dterm = pid.d * (long)vel;  // WHY do we need the (long) cast???  //#DELME comment only
    //dter =pid.d*-(long)vel; //polarity flip on velocity measurment
    dterm = fast_rshift_long(dterm, pid.d_shift);

    // Compute the full PID output
    // NOTE: Only p term of pid.sign is handled here, since i term handled above
    if (pid.sign)
        sum = (long)pid.amp_init + pterm + iterm + dterm;
    else
        sum = (long)pid.amp_init - pterm + iterm - dterm;

    // Coerce to signed 16-bit integer
    // PID algorithm should naturally fall into range, unless
    // specified set point is impossible to achieve, in which case
    // the controller will rail
    if (sum <= -32768L)
        toret = -32768;
    else if (sum >= 32767L)
        toret = 32767;
    else
        toret = (int)sum;

    return toret;
}

void update_sqwave_setpoint(void)
{
    if (sq.counter < sq.halfperiod)
        pid.setpoint = sq.max;
    else
        pid.setpoint = sq.min;

    sq.counter++;
    if (sq.counter >= sq.halfperiod + sq.halfperiod)
        sq.counter = 0;
}

/* Sets the square wave setpoint parameters
 *
 * min - minimum value
 * max - maximum value
 * halfperiod - half of the full period of the square wave, in ticks/8
 */
void set_sqwave(int min, int max, int halfperiod)
{
    sq.min = min;
    sq.max = max;
    sq.halfperiod = halfperiod;
    sq.counter = 0;
}

/* Resets the square wave internal counter
 */
void reset_sqwave(void)
{
     sq.counter = 0;
}

/* set_pid_setpoint
 *
 * Adjusts the current PID loop to have a new setpoint
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 *
 * setpoint - value to which to rotate the wheel
 */
void set_pid_setpoint(int setpoint)
{
    pid.setpoint = setpoint;

}

void set_pid_setpoint_derivatives( int sd1, int sd2 )
{
    pid.setpoint_d1 = sd1;
    pid.setpoint_d2 = sd2;
}

/* set_pid_sign
 *
 * Valid arguments are 0 and 1.
 */
void set_pid_sign(unsigned char sign)
{
    pid.sign = sign;
}

/* set_pid_p
 *
 * Adjusts the current PID loop to have a new p constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_p(unsigned p)
{
    pid.p = p;
}

/* set_pid_p_shift
 *
 * Changes the fixed-point basis of the proportional term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_p_shift(unsigned p_shift)
{
    pid.p_shift = p_shift;
}


/* set_pid_i
 *
 * Adjusts the current PID loop to have a new i constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_i(unsigned i)
{
    pid.i = i;
    if (i==0)
        pid.accumulator = 0;
}

/* set_pid_i_shift
 *
 * Changes the fixed-point basis of the integral term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop. Does attempt to compensate the accumulator for the new scaling
 * factor so that there is not too large a discontinuity. 
 */
void set_pid_i_shift(unsigned i_shift)
{
    int diff = (int)pid.i_shift - (int)i_shift;
    if (diff >=0) {
        pid.accumulator = pid.accumulator >> diff;
    } else {
        pid.accumulator = pid.accumulator << -diff;
    }

    pid.i_shift = i_shift;
}

/* set_pid_d
 *
 * Adjusts the current PID loop to have a new d constant
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_d(unsigned d)
{
    pid.d = d;
}

/* set_pid_d_shift
 *
 * Changes the fixed-point basis of the derivative term of the current PID loop
 *
 * Does not change any other PID parameters. Does not re-initialize the PID
 * loop.
 */
void set_pid_d_shift(unsigned d_shift)
{
    pid.d_shift = d_shift;
}

// Zeroes the accumulator.
void set_pid_reset(void)
{
    pid.accumulator = 0;
}

void set_pid_parameter_group(struct pidparams_both params){
    pid.p = params.pos_p;
    pid.d = params.pos_d;
    pid.i = params.pos_i;

    pid.p_shift = params.pos_p_shift;
    pid.d_shift = params.pos_d_shift;
    pid.i_shift = params.pos_i_shift;
}

void pid_status(void)
{
    char str[100]; // max string len is about 51 chars, so 100 is very safe

    sprintf(str, "PSG: ---POSITION PID PARAMETERS---\r\n");
    TTYPuts(str);
    if(get_pid_mode()==PID_MODE_SINEWAVE){
        sprintf(str, "PSG: PID MODE SINEWAVE\r\n");
        TTYPuts(str);
    }
    if(get_pid_mode()==PID_MODE_NORMAL){
        sprintf(str, "PSG: PID MODE SINGLE SET\r\n");
        TTYPuts(str);
    }
    if(get_pid_mode()==PID_MODE_SQUAREWAVE){
        sprintf(str, "PSG: PID MODE SQUAREWAVE\r\n");
        TTYPuts(str);
    }
    sprintf(str, "PSG: s = %d, e = %ld\r\n", pid.setpoint, error);
    TTYPuts(str);
    sprintf(str, "PSG: accum = %ld, sign = %s\r\n", pid.accumulator, pid.sign ? "pos" : "neg");
    TTYPuts(str);
    sprintf(str, "PSG: p = %u, p_shift = %u, p_term = %ld\r\n", pid.p, pid.p_shift, pterm);
    TTYPuts(str);
    sprintf(str, "PSG: i = %u, i_shift = %u, i_term = %ld\r\n", pid.i, pid.i_shift, iterm);
    TTYPuts(str);
    sprintf(str, "PSG: v = %i, d = %u, d_shift = %u, d_term = %ld\r\n",
            vel_last, pid.d, pid.d_shift, dterm);
    TTYPuts(str);
    if(get_pid_mode()==PID_MODE_SINEWAVE){
        sprintf(str,"PSG: amplitude = %i ,frequency = %f, offset %i\r\n",get_sine_amp(),
                get_sine_freq(),get_sine_offset());
        TTYPuts(str);
    }

}

//char *get_pid_parameters(void)
//{
//    static char str[20];
//    sprintf(str, "%c%.2X%.2X%.2X%.1X%.1X%.1X", pid.sign ? '+' : '-',
//                                               pid.p,
//                                               pid.i,
//                                               pid.d,
//                                               pid.p_shift,
//                                               pid.i_shift,
//                                               pid.d_shift);
//    return str;
//}

int get_pid_setpoint(void)
{
    return pid.setpoint;
}

int get_pid_setpoint_d1(void)
{
    return pid.setpoint_d1;
}

int get_pid_setpoint_d2(void)
{
    return pid.setpoint_d2;
}

void get_pid_parameters(void)
{
    fb_putc(pid.sign ? '+' : '-');
    fb_put16(pid.p);
    fb_put16(pid.i);
    fb_put16(pid.d);
    fb_put4(pid.p_shift);
    fb_put4(pid.i_shift);
    fb_put4(pid.d_shift);
}
