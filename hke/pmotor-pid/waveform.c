#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <math.h>

#include "pmotor-pid.h"
#include "sinetable.h"


struct sinewaveparams {
    int offset;
    unsigned amplitude;
    unsigned long delta_phase;
    unsigned long phase_offset;
    unsigned long phase_accumulator;
    int resync_on_frame;
};

static struct sinewaveparams sw = {
    .phase_offset = 0,
    .offset = 0,
    .amplitude = 1000,
    .delta_phase = 0,
    .phase_accumulator = 0,
    .resync_on_frame = 1
};


int calculate_sine( int vindex )
{
//    // Coerce vindex into range 0 <= vindex < SINE_TABLE_LEN
//    mask = 0b1 << SINE_TABLE_LEN_POW;
//
   // while (vindex < 0)
   //     vindex += SINE_TABLE_LEN;
    //while (vindex >= SINE_TABLE_LEN)
    //    vindex -= SINE_TABLE_LEN;
    
    //If our frequency gives us a even number of sinewaves per frame reset the 
    //sine wave to zero on the correct tick to avoid drift
    //does this work? No it Doesn't makes the PID real noisy
    //#FIXME
    
     

   


    // Compute the quadrant from the top 2 bits of vindex
    int quadrant = (vindex >> SINE_TABLE_LEN_POW) & 0b11;  // if vindex is out of range, this may be bigger than 3. Only want 3.
    int index = vindex & SINE_TABLE_LEN_MASK;       // index is the bottom 10 bits
    int invert = 0;

    // Flip / invert depending on the quadrant
    switch( quadrant ) {
    case 0:
        // nothing to do
        break;
    case 1:
        index = SINE_TABLE_LEN - index - 1;
        break;
    case 2:
        invert = 1;
        break;
    default:
        index = SINE_TABLE_LEN - index - 1;
        invert = 1;
        break;
    }

    // Lookup the value in the sine table
    int tval = sine_table[index];
    if( invert )
        tval = -tval;

    return tval;
}

// updates the PID setpoint as a sinewave
void update_sinewave( void )
{
    int s, sd1, sd2;

    sw.phase_accumulator += sw.delta_phase;
    if(sw.resync_on_frame && get_tick()==1){
     sw.phase_accumulator = sw.phase_offset;

    }
    // Index into "virtual" table that is 4 times as long and contains the full sine waveform
    // Our real table only has the first quadrant of the waveform
    int vindex = sw.phase_accumulator >> (SW_ACCUMULATOR_BITS - (SINE_TABLE_LEN_POW+2));

    int tval = calculate_sine(vindex);

    // Scale by amplitude and add offset
    // long tmp = ((long)tval)*((unsigned long)sw.amplitude);
    long tmp = __builtin_mulsu(tval, sw.amplitude);  // Faster equivalent to above line (only does 16x16 multiply)

    int scaled = (tmp >> 16);   // we only want the top 16 bits
    s = scaled + sw.offset;

    // FIRST DERIVATIVE -- (cosine)
    vindex += SINE_TABLE_LEN;       // offset by 1/4 cycle ("virtual" table is 4 times longer than real table)
    tval = calculate_sine(vindex);
    tmp = __builtin_mulsu(tval, sw.amplitude);  // Faster equivalent to above line (only does 16x16 multiply)
    sd1 = (tmp >> 16);             // we only want the top 16 bits (no offset, since derivative has no offset)

    // SECOND DERIVATIVE -- (-sine)
    sd2 = -scaled;         // the second derivative of sine is -sine

    set_pid_setpoint(s);
    set_pid_setpoint_derivatives( sd1, sd2 );
}

void set_sine_amp( int amp )
{
    // Clip the amplitude to keep from wrapping the sine wave
    int abs_off = sw.offset >= 0 ? sw.offset :-sw.offset;
    if( amp > (32767 - abs_off) )
        amp = 32767 - abs_off;

    sw.amplitude = amp;
}

void set_sine_offset( int offset )
{
    // Clip the amplitude to keep from wrapping the sine wave
    int abs_off = offset >= 0 ? offset :-offset;
    if( sw.amplitude > (32767 - abs_off) )
        sw.amplitude = 32767 - abs_off;

    sw.offset = offset;
}

void set_sine_phase(float phase ){
    unsigned long phase_offset = (long)((phase/360)*(1ULL<<SW_ACCUMULATOR_BITS));//
    unsigned long current_phase = sw.phase_offset;
    unsigned long phase_offset_change;

    sw.phase_offset=phase_offset;
    if (phase_offset>=current_phase)
        phase_offset_change = phase_offset-current_phase;
    else
        phase_offset_change = (long)((1ULL<<SW_ACCUMULATOR_BITS) - (current_phase - phase_offset));

    sw.phase_accumulator += phase_offset_change;
}

// Sets the phase accumulator IN RAW UNITS, not degrees.
void set_sine_phase_raw(unsigned long phase)
{
    sw.phase_accumulator = phase;
}

void set_sine_freq_hz( float freq_hz )
{

    long delta = (long)(((freq_hz / SW_FREQ_NATURAL) * SW_DELTA_NATURAL) + 0.5);
    sw.resync_on_frame = 0;
    sw.delta_phase = delta;
}

void set_sine_freq_cpf( int freq_cpf )
{
    //float epsilon =.001;//SW_FREQ_NATURAL/TICKS_PER_FRAME/NUM_INPUTS

    //if( freq_cpf-floor(freq_cpf) > epsilon )
    //    sw.resync_on_frame = 0;

    long delta = (long)((freq_cpf / (SW_FREQ_NATURAL / FRAME_RATE)) * SW_DELTA_NATURAL);
    sw.delta_phase = delta;
}

void set_sine_sync(int sync)
{
    sw.resync_on_frame=sync;
}

int get_sine_amp(void)
{
    return sw.amplitude;
}

int get_sine_offset(void)
{
    return sw.offset;
}

float get_sine_freq(void)
{
    float freq = (sw.delta_phase * SW_FREQ_NATURAL) / SW_DELTA_NATURAL;
    return freq;
}

void get_sine_parameters(void)
{
    fb_put16(sw.amplitude);
    fb_put32(sw.delta_phase);
    fb_put16(sw.offset);
}

int get_sine_sync(void)
{
    return sw.resync_on_frame;
}

int is_freq_int(void)
{

    return;
}

