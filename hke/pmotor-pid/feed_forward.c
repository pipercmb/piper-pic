#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>

#include "pmotor-pid.h"


struct ffparams {
    int a;
    int b;
    int c;

    /* #NUMDER For numerical derivatives
    int sp0, sp1, sp2;      // The current setpoint and the two previous values
    */
    
    int dac;              // The most recent calculated feedforeward signal
};

static struct ffparams ff = {
    .a = 0,
    .b = 0,
    .c = 0,
};

long update_feedforward( void )
{
    /* #NUMDER Numerical derivates required storing previous samples
    ff.sp2 = ff.sp1;
    ff.sp1 = ff.sp0;
    ff.sp0 = get_pid_setpoint();        // Get the curent value of the pid setpoint
    */
    
    /* #NUMDER Numerical derivatives worked for the sine wave case, but the 2nd derivative was extremely noisy.
     * Feed forward turned out not to be helpful at all for the square wave case.
    // calculate the numerical derivatives as first order diferences
    long d1 = (long)ff.sp1 - (long)ff.sp0;                          // First derivative (setpoint velocity)
    long d2 = (long)ff.sp2 - (long)ff.sp1 - (long)ff.sp1 + (long)ff.sp0;    // Second derivative (setpoint acceleration)
    // CompuTTYPutcFromInt(tick);
    TTYPutcFromInt('\r');
    TTYPutcFromInt('\n');te the feedword signal
    long dac = (ff.a * d2) + (ff.b * d1) + ((ff.c * (long)ff.sp0) >> 8);
    */

    int d1 = get_pid_setpoint_d1();
    int d2 = get_pid_setpoint_d2();

    // Compute the feedword signal  (this is a faster equivalant to long dac = (ff.a * (long)d2) + (ff.b * (long)d1);
    long dac = __builtin_mulss(ff.a, d2) >> 6;
    dac += __builtin_mulss(ff.b, d1) >> 6;

    // Clip to a 16 bit dac value
    if (dac <= -32768L)
        ff.dac = -32768;
    else if (dac >= 32767L)
        ff.dac = 32767;
    else
        ff.dac = (int)dac;

    return ff.dac;
}

void set_ff_a(int a)
{
    ff.a = a;
}
void set_ff_b(int b)
{
    ff.b = b;
}
void set_ff_c(int c)
{
    ff.c = c;
}

void get_ff_parameters()
{
    fb_put16(ff.a);
    fb_put16(ff.b);
    fb_put16(ff.c);
}
void ff_status(void)
{
    char str[100]; // max string len is about 51 chars, so 100 is very safe

    sprintf(str, "PSG: ---FEED FORWARD PARAMETERS---\r\n");
    TTYPuts(str);
    sprintf(str, "PSG: a = %d, b = %d, c = %d\r\n", ff.a, ff.b, ff.c);
    TTYPuts(str);
}
