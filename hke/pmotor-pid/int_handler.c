#include <libpic30.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "pmotor-pid.h"

static void update_data_raw(void);
static void update_frame(void); // writes a new reading (if we have one) to our data frame
static void adc_update();
static unsigned char frame_counter = 0; // Which frame are we on?, a frame holds 16 readings and takes 1 seconds

static int tick = 0;

static volatile unsigned char data_mode_request = DATA_MODE_ON; // default: data stream is on
static volatile unsigned char data_mode = DATA_MODE_ON;

unsigned char mode = MODE_FREERUN;    // Start up in freerun mode.
unsigned char pid_mode = PID_MODE_NORMAL;
unsigned char ff_on = 0;        // Set to 1 to enable feedforward
static int ADC_values[NUM_ADC_CHANNELS];     // Most recent value of the ADC Converter
static unsigned char ADC_indices[NUM_INPUTS] = {6, 7};    // Which ADC channels to read
static unsigned char mux_counter = 0;       // ADC index table lookup counter
static int mux_channel = 7;                 // mux channel -- this does not change
static int cdac = 0;                        // PID loop output
static int pos = 0;                         // Current position value from ADC
static int vel = 0;                         // Current velocity value from ADC
static int hit_stop = 0;
static int drive_lim = 19000;

static unsigned dac_val_print; //DELME

void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void)
{
    IFS0bits.OC2IF = 0; // reset Output Compare 2 interrupt flag
    PIN_OSCOPE2 = 1;

     // See if we're starting a new frame
    char isNewFrame;
    if( PIN_CLK_MODE_INTERNAL ) {
        PIN_LED3 = 1;   // #DELME
        isNewFrame = (tick == 0);   // New frame starts on tick 0
    } else {
        // Check if a new frame has started by reading in PIN_FRAME_CLK (keeps all boards in lockstep)
        isNewFrame = check_frame_clock();
    }

    // If we have just started a new frame, then reset all our state variables (happens once per second)
    // This guarantees that we stay in sync with the rest of the backplane, (even if a cosmic ray corrupted tick)
    if (isNewFrame) {
        tick = 0;
        data_mode = data_mode_request;  // only allow data mode to change at the start of a new frame
                                        // this ensures that when switching from "raw" to "on"
                                        // that update_communication is called with isNewFrame == 1 which resets his internal state.
    }

    // Read the ADC values -- Cap sensor
    // ! This needs to be the *first* SPI port operation of the interrupt handler
    // since the ADC has no chip select pin, he must be read out before loading any DACs.
    adc_update();
    mode = get_mode();

    static int ffdac = 0;  // static because only update PID every 8 cycles, but update DAC every cycle
    if (mode == MODE_PID) {
        if (mux_counter == 0) {
            if (pid_mode == PID_MODE_SQUAREWAVE)
                update_sqwave_setpoint();
            else if(pid_mode == PID_MODE_SINEWAVE)
                update_sinewave();
            
            pos = ADC_values[CHANNEL_POSITION];
            vel = ADC_values[CHANNEL_VELOCITY];
            cdac = compute_pid(pos, vel);

            ffdac = update_feedforward();

            if( ff_on ) {
                long ff = (long)cdac + (long)ffdac;
                if (ff <= -32768L)
                    cdac = -32768;
                else if (ff >= 32767L)
                    cdac = 32767;
                else
                    cdac = (int)ff;
            }
        }
    }

    // write the calcuolated voltage out to the hardware DAC (channel 0 of our 4 channel DAC)
    /*if (pos > drive_lim || hit_stop ==1){
        hit_stop = 1;
        cdac = 0;
        PIN_LED3=1;
    } */
    unsigned dac_val = cdac + 32768;      // For AD5544 channels 0-2, dac_val = 32768 corresponds to 0 volts

    
    MDacWrite(0, dac_val);
    // Latch the values we just set into the MDAC
    PIN_MDAC_LOAD = 0;
    PIN_MDAC_LOAD = 1;

 //  Write encoder position out to the dac so we can look at it with the scope
  //  int ddac0 = abs_pos + 32768;
   // int ddac1 = (velocity<<2)+32768 ;
    int ddac2 = get_pid_setpoint() + 32768;
    int ddac1 = ffdac + 32768;
    unsigned ddac0;

    long error = (long)pos - (long)get_pid_setpoint();
    if (error <= INT_MIN)
        ddac0 = INT_MIN;
    else if (error >= INT_MAX)
        ddac0 = INT_MAX;
    else
        ddac0 = error;

    ddac0 += 32768;
//    DDacWrite(DACREG_DAC, 0, ddac0);
    DDacWrite(DACREG_DAC, 0, ddac0); // Write PID control ouput
    DDacWrite(DACREG_DAC, 1, ddac1);  // Write out the Feedforward setting
    DDacWrite(DACREG_DAC, 2, ddac2);  // Write out the PID setpoint
    
    // Write data
    if( data_mode == DATA_MODE_RAW ) {
       if ((tick % 20 == 0))  // (4000 Hz)/32 = 125 Hz data rate  // #FIXME
           update_data_raw();
       TTYUpdateTXNoFrameFast4();   // Look for characters in TX queue to send over UART
    }
    else {
        update_frame(); // writes a new reading (if we have one) to our data frame
        update_communication(isNewFrame); // Update address counters and transmit buffer in our time slot (does not look at tick)
    }

    // Flash the LED at 1 Hz, for consistency with all the other boards
    if (tick >= TICKS_PER_FRAME/2) {
        PIN_LED1 = 0;
    } else {
        PIN_LED1 = 1;
    }

    // For backplane mode, this is a redundant check.  This is required for internal clock mode
    ++tick;
    if (tick >= TICKS_PER_FRAME ) {
        tick = 0;
    }

    PIN_OSCOPE2 = 0; 
}

static void adc_update() //channels 1-8 per data sheet
{
    SPI1CONbits.CKE = 1;    // CKE=1 for our standard ADC
    SPI1CONbits.MODE16 = 1; // Set to 16 bit mode

    int data_read = 0;
    SPI1BUF = 0xFFFF;
    while (!SPI1STATbits.SPIRBF);
    data_read = SPI1BUF;

    // Save ADC reading to our array and make it signed
    ADC_values[mux_channel] = data_read - 32768;


    // Set the MUX to the new channel so it has a whole tick to settle
    
    mux_counter++;
    if (mux_counter >= NUM_INPUTS)
        mux_counter = 0;

    mux_channel = ADC_indices[mux_counter];

    PIN_A0 = (mux_channel & 0b001) != 0;
    PIN_A1 = (mux_channel & 0b010) != 0;
    PIN_A2 = (mux_channel & 0b100) != 0;
}

static void update_data_raw(void)
{
    // CHARS_PER_DATA_WRITE = 12.
    //TTYPutcFromInt('^');
    //TTYPut16FromInt(amplitude);
    //TTYPutcFromInt(' ');
    //TTYPut32FromInt(abs_dacpos);
    int pid_set = get_pid_setpoint();
    pos = ADC_values[CHANNEL_POSITION];
    //vel = ADC_values[CHANNEL_VELOCITY];
    //char str1[50];
    //sprintf(str1,"%d \r\n",get_cdac());
    //sprintf(str1,"%04X%04X\r\n", pos,pid_set);
    TTYPut16FromInt(pid_set);
    TTYPutcFromInt(' ');
    TTYPut16FromInt(pos);
    TTYPutcFromInt('\r');
    TTYPutcFromInt('\n');
    
    
    //TTYPutsDirect(str1);
    
    //TTYPut32FromInt(pid_set);
    //TTYPut32FromInt(pos);
    //TTYPut32FromInt(vel);
    //TTYPutcFromInt('\r');
    //TTYPutcFromInt('\n');
}


static void update_frame() {
    // Start of the frame
    if( tick == 0 ) {
        fb_put8(frame_counter); // Save the frame counter as 1st entry in new frame buffer

        unsigned char status = 0; // get_cur_mode();
        fb_put8(status);
    }
    if( tick == 2 ) {
        pos = ADC_values[CHANNEL_POSITION];
        vel = ADC_values[CHANNEL_VELOCITY];
        fb_put16(pos);
        fb_put16(vel);
        fb_putc(mode);          // Board mode ('F', 'V', or 'P')
        fb_put4(pid_mode);
      
        
    }
    if (tick == 3) {
        get_pid_parameters();
    }
    if (tick == 4) {
        get_sine_parameters();
    }

    if (tick == 6) {
        fb_put4(ff_on);
        get_ff_parameters();
    }
    // Check if we are on the last tick of the frame
    if (tick == TICKS_PER_FRAME-1 ) {
        PIN_LED2 = 1;
        // This buffer is full, so swap to new one and queue up the old one for sending
        swap_active_frame_buf(); // Swap which frame buffer we're writing to
        if (data_mode)
            enqueue_last_frame_buf();

        // Update frame_counter
        frame_counter++;
    }
}




//-------------- SETTERS / GETTERS --------------


// Set sine wave amplitude in raw units (-32768 to 32767)
void set_cdac(int dac)
{
    cdac = dac;
}

// Set mode
void set_data_mode(char flag)
{
    data_mode_request = flag;
}

// Set mode
void set_mode(unsigned char new_mode)
{
    mode = new_mode;
}

// Set pid mode
void set_pid_mode(int mode)
{
    pid_mode = mode;
}

// Set FF on or off
void set_ff_enable(int on)
{
    ff_on = on;
}


int get_cdac(void)
{
    return cdac;
}

int get_pid_mode(void)
{
    return pid_mode;
}

unsigned char get_mode(void)
{
    return mode;
}

char get_data_mode(void)
{
    return data_mode;
}


int get_adc_value(int channel)
{
    if (channel<0 || channel>8)
        return -1;
    return ADC_values[channel];
}
int get_tick(void)
{
    return tick;
}
void reset_hit_stop(void){
    hit_stop = 0;
}