#ifndef __PMOTOR_PID_INCLUDED
#define __PMOTOR_PID_INCLUDED

#define BOARD_NAME  "PMOTOR_PID"

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

//---------> Some useful stuff
#define CLOCKFREQ 20000000L		// 20.000 MHz instruction clock (synthesised from 5 MHz crystal, 16x PLL)

// #define BRG_SETTING 2       // 416 kbs.  This is the fastest rate we can generate with a 20 MHz FCY that is
                            // compatible with the MAC using our FTDI chip USB to Fiber board.
                            // BRG = FCLK/(BaudRate*16) - 1
// Calculate the actual BAUD rate from the specified BRG setting
// #define BAUD_RATE           (CLOCKFREQ/(BRG_SETTING+1) >> 4)

#define BAUD_RATE	115200
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)- 1 + 0.5)))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Add 0.5 to convert to int from float
#define ENCODER_PULSE_PER_REV     5000      // 2500 lines per revolution and we run hardware in 2x mode
#define ENCODER_MAX       (ENCODER_PULSE_PER_REV - 1)
#define LEAD_ANGLE_OPTIMUM      33   // Guessed optimum lead angle

#define NUM_ADC_CHANNELS	8
#define CONVERT_PULSE_STOP_TICK 100  // 100 gives a 5 us pulse on the ADC convert pin

// Motor control modes (these character constants are sent out in the data packet over the serial port direclty)
#define MODE_FREERUN      'F'
#define MODE_PID          'P'


#define DATA_MODE_OFF     0     // Board will report no data
#define DATA_MODE_ON      1     // Standard hex-encoded backplane-style data packets
#define DATA_MODE_RAW     2     // Super-fast, binary data (aka "lightning"  or "ronset" mode)

// Interval measured in ticks at which we compute velocity by differencing the encoder readings.
// An interval of 100 ticks gives a velocity loop update rate of 40 Hz.
// It gives a velocity precision of 0.008 revs / sec ~ 0.5 RPM
// calculated as (1 count)/(100 ticks)*(4000 ticks/s)/(5000 counts/rev) =
#define VELOCITY_MEASUREMENT_INTERVAL   100

// Multiply this by velocity in hardware units to get velocity in Hz.  NOTE THE FLOAT (slow!!!)
#define VELOCITY_HZ_PER_COUNTS  ((float) TICKS_PER_FRAME / ((float)VELOCITY_MEASUREMENT_INTERVAL * ((float)ENCODER_PULSE_PER_REV)))

// How long to dwell before incrementing the velocity ramp setpoint *in units of VELOCITY_MEASUREMENT_INTERVAL*
//#define VRAMP_UPDATE_DWELL 15 //Moved into pid_vel to make setable

// Interval measured in ticks at which we update the PID position setpoint for trapezoid slews
// An interval of 10 ticks gives an update rate of 400 Hz.
#define TRAPEZOID_UPDATE_INTERVAL 10

//------> INPUT PINS
#define PIN_ADDR0           PORTEbits.RE1   // Backplane card address HEX switch
#define PIN_ADDR1           PORTEbits.RE2   // Backplane card address HEX switch
#define PIN_ADDR2           PORTEbits.RE3   // Backplane card address HEX switch
#define PIN_ADDR3           PORTEbits.RE0   // Backplane card address HEX switch
#define PIN_ADDR4           PORTEbits.RE4   // Address high bit (jumper next to hex address switch)

#define PIN_GROUP0          PORTDbits.RD7   // Command Group OCT switch bit 0
#define PIN_GROUP1          PORTFbits.RF0   // Command Group OCT switch bit 1
#define PIN_GROUP2          PORTFbits.RF1   // Command Group OCT switch bit 2

#define PIN_SYNC_CLK        PORTDbits.RD3   // From the Backplane 96 pin connector
#define PIN_FRAME_CLK       PORTDbits.RD2   // From the Backplane 96 pin connector

#define PIN_CLK_MODE_INTERNAL     PORTCbits.RC13  // Low = External (Backplane) clock / High = on-board crystal clock

#define PIN_LIMIT_A         PORTDbits.RD0   // External limit switch input A (normally high / low = stop)
#define PIN_LIMIT_B         PORTDbits.RD11  // External limit switch input B (normally high / low = stop)
#define PIN_HOME            PORTDbits.RD10  // External home switch input (normally high / low = home)

#define PIN_EXT_SPARE       PORTDbits.RD9   // External switch - unknown purpose

//-----> OUTPUT PINS
#define PIN_A0			LATBbits.LATB11	// ADC input mux
#define PIN_A1			LATBbits.LATB12	// ADC input mux
#define PIN_A2			LATBbits.LATB13	// ADC input mux
#define PIN_GAIN0               LATGbits.LATG3	// ADC Instrumentation Amplifier LSB
#define PIN_GAIN1               LATGbits.LATG2	// ADC Instrumentation Amplifier MSB

#define PIN_MDAC_CS		LATBbits.LATB8	// Motor DAC Chip Select Bar (low = select)
#define PIN_MDAC_LOAD		LATBbits.LATB9	// Motor DAC Load Dac (low = load)

#define PIN_DDAC_SYNC		LATBbits.LATB14	// Diagnostic DAC Sync pin (active low)
#define PIN_DDAC_LOAD		LATBbits.LATB15	// Diagnostic DAC Load pin (low = load)

#define PIN_485_ENABLE          LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

#define PIN_DIG_OUT1		LATDbits.LATD8  // Digital Out 1 (to 96 pin connector)
#define PIN_DIG_OUT2            LATCbits.LATC15	// Digital Out 2 (to 96 pin connector)

// Diagnostic outputs
#define PIN_OSCOPE0		LATDbits.LATD6  // Diagnostic pin
#define PIN_OSCOPE1		LATDbits.LATD5  // Diagnostic pin
#define PIN_OSCOPE2		LATDbits.LATD4  // Diagnostic pin

#define PIN_LED1		LATBbits.LATB2	// Green LED
#define PIN_LED2		LATBbits.LATB1	// Yellow LED
#define PIN_LED3		LATBbits.LATB0	// Red LED
#define PIN_LED4		LATEbits.LATE7	// Red LED
#define PIN_LED                 PIN_LED1        // Standard backplane boards require a PIN_LED symbol (flashes on sync_to_frame_clock)

//  V_adc = ADC * 20480 mV / 65536 = ADC * 10 / 2^5
#define ADC_TO_MV(x) (((x>>4)*5)-10240)

// DAC Hardware
#define DACREG_DAC			0
#define DACREG_OUTPUT_RANGE		1
#define DACREG_POWER_CONTROL            2
#define DACREG_CONTROL			3

// DAC conversions
#define AMP_TO_DAC(x) (10000.0 * (x) / 3.00)
#define DAC_TO_AMP(x) (3.00 * (x) / 10000.0)

// ------------- PID STUFF ------------
#define NUM_INPUTS 2
#define PID_UPDATE_RATE     ((float)(TICK_RATE)/NUM_INPUTS) //PID Update in Hz
#define CHANNEL_POSITION    7

#define CHANNEL_VELOCITY    6

#define PID_MODE_NORMAL     0
#define PID_MODE_SQUAREWAVE 1
#define PID_MODE_SINEWAVE 2

// constants for the sine wave setpoint table defined in sinetable.h
#define SINE_TABLE_LEN_POW  10                  // table length must be a power of two
#define SINE_TABLE_LEN  (1<<SINE_TABLE_LEN_POW)                     // 1024
#define SINE_TABLE_LEN_MASK (SINE_TABLE_LEN - 1)      // 0b1111111111 = 0x3ff  (10 ones)

#define SW_ACCUMULATOR_BITS 32          // phase accumulator size in bits
#define SW_DELTA_NATURAL    (1L << (SW_ACCUMULATOR_BITS - (SINE_TABLE_LEN_POW+2)))
#define SW_FREQ_NATURAL     ((float)PID_UPDATE_RATE / (SINE_TABLE_LEN<<2))
#define FRAME_RATE          ((float)TICKS_PER_FRAME / TICK_RATE)


//---------> Function Prototypes
// pmotor-pid.c
void init_pic( void );		// Init the PIC ports and hardware

// process_cmd.c
void process_cmd( char *cmd );

// cmd_gets.c
void cmd_gets( char *str, int n );

// ddac.c
void DDacInit( void );
void DDacTest( void );
void DDacWrite( int reg, int ch, int data );
void MDacWrite(int ch, unsigned int data);
void MDacClear(void);

// feed_forward.c
long update_feedforward( void );
void set_ff_a(int a);
void set_ff_b(int b);
void set_ff_c(int c);
void ff_status(void);

// int_handler.c
void set_mode(unsigned char new_mode);
void set_pid_mode(int mode);
void set_data_mode(char flag);
unsigned char get_mode(void);
int get_pid_mode(void);
char get_data_mode(void);
int get_adc_value(int ch);
void set_cdac(int dac);
void set_ff_enable(int on);
int get_tick(void);


struct pidparams_both {
    unsigned vel_p; // P term for PID
    unsigned vel_i; // I term for PID
    unsigned char vel_sign; // Sign of P term and I term
    unsigned char vel_i_shift; // I term is divided by 2^i_shift
    unsigned char vel_p_shift; // P term is divided by 2^p_shift

    unsigned pos_p; // P term for PID
    unsigned pos_i; // I term for PID
    unsigned pos_d;  // D term for PID
    unsigned char pos_sign; // Sign of P term and I term
    unsigned char pos_i_shift; // I term is divided by 2^i_shift
    unsigned char pos_p_shift; // P term is divided by 2^p_shift
    unsigned char pos_d_shift; // D term is divided by 2^d_shift

    int vel_ramp_delay;
    int vel_ramp_delta;
};

// pid_loop.c
int compute_pid(int pos, int vel);
void update_sqwave_setpoint(void);
void set_sqwave(int min, int max, int halfperiod);
void reset_sqwave(void);
void set_pid_setpoint(int setpoint);
void set_pid_setpoint_derivatives( int sd1, int sd2 );
void set_pid_sign(unsigned char sign);
void set_pid_p(unsigned p);
void set_pid_p_shift(unsigned p_shift);
void set_pid_i(unsigned i);
void set_pid_i_shift(unsigned i_shift);
void set_pid_d(unsigned d);
void set_pid_d_shift(unsigned d_shift);
void set_pid_reset(void);
void pid_status(void);
int get_pid_setpoint(void);
int get_pid_setpoint_d1(void);
int get_pid_setpoint_d2(void);
void get_pid_parameters(void);
void set_pid_parameter_group(struct pidparams_both params);  // #DELME Notation?

// waveform.c
void update_sinewave( void );
void set_sine_freq_hz( float freq );
void set_sine_amp( int amp );
void set_sine_offset( int offset );
int get_sine_amp(void);
int get_sine_offset(void);
float get_sine_freq(void);
void get_sine_parameters();
void get_ff_parameters();
void set_sine_phase_raw(unsigned long phase);
void set_sine_phase(float phase );
void reset_hit_stop(void);




#endif //__PMOTOR_PID_INCLUDED