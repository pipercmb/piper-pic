/* One of these macros should be defined for each project
 * They are set in the project file (.mcp), not in any source file (.c) or header file (.h)
 * MPLAB v8 -> Project -> Build Options -> Project -> MPLAB C30 -> Preprocessor Macros
 * MPLAB X -> Right click Project Name -> Properties -> xc16-gcc -> Option Category Preprocessor / Messages -> Define C Macro
 * Alternate: open *.mcp file and edit
 */

#if defined TREAD
	#include "../tread/tread.h"

#elif defined DSPID
	#include "../dspid/dspid.h"

#elif defined ANALOG_OUT
	#include "../analog_out/analog_out.h"

#elif defined ANALOG_IN
	#include "../analog_in/analog_in.h"

#elif defined PMASTER
	#include "../pmaster/pmaster.h"

#elif defined PMOTOR
	#include "../pmotor/pmotor.h"

#elif defined PMOTOR_PID
	#include "../pmotor-pid/pmotor-pid.h"

#elif defined PMOTOR_QEI
	#include "../pmotor-qei/pmotor-qei.h"

#elif defined DISCRETE
	#include "../discrete/discrete.h"

#elif defined PIXIEBIAS
	#include "../pixiebias/pixiebias.h"
    #include "../pixiebias/version.h"

#elif defined PIXIEREADOUT
	#include "../pixiereadout/pixiereadout.h"

#else
    #error "The board.h file was not found.  To fix this please define BOARD name macro in project properties.  See board-defs.h!"
#endif