#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>

#include "../common/ptty.h"
#include "../common/tty_print.h"

#include "dspid.h"
#include "../common/math_utilities.h"

#define ACCUMULATOR_MAX     (1073741824L)  // = 2^30. Prevents accumulator from overflowing when adding new error.


static char ishift = 8; 			// this is the "i" parameter for the pid loop

static short ihold_setpoint = 0;     // Setpoint for the PID controller
static long error = 0;				// Most recent error (measurement - setpoint)
static long accumulator = 0;		// the integration accumulator
static long iterm = 0;              // i*accum

static short ihold_coil_start_value;	// the coil dac setting when we start PID controlling
                                        // this needs to be initialized before we start in ihold mode

// Max/Min values for voltage across coil and current through coil during ihold mode
static short coil_v_max = COIL_MV_TO_DAC(300);
static short coil_v_min = COIL_MV_TO_DAC(-300);
static short coil_i_max = I_SENSE_MA_TO_ADC(5500);
static short coil_i_min = 0;

static unsigned tick = 0;       //#DEBUG

/*
 * Call this when begining ihold mode.
 */
void init_ihold( short coil_dac, short coil_v_mon )
{
    // We must subtract off any existing vmon (ramp rate)
    // Or else, current continues ramping while vmon decays slowly
    ihold_coil_start_value = coil_dac - coil_v_mon;
    accumulator = 0;
}

/*
 * Computes CDAC setting using supplied demod value
 */
short compute_ihold(short coil_i_sense, short last_coil_dac, short coil_v_mon)
{
	char add;
	long new_coil_dac;

	PIN_OSCOPE1 = 1;

	// calculate the error, in hardware ADC units
	error = (long)ihold_setpoint - (long)coil_i_sense;		// Error is ~24 bits

	// Now deal with accumulator
	// Ensure |accumulator| < max (keeps iterm<2^32 from overflowing)
	add = 1;

	if( accumulator > ACCUMULATOR_MAX && error > 0 )
		add = 0;
	if( accumulator < -ACCUMULATOR_MAX && error < 0 )
		add = 0;

	// Anti-windup
	if( last_coil_dac >= 32000 && error > 0 )
		add = 0;
	if( last_coil_dac <= -32000 && error < 0 )
		add = 0;

	// Compute the I term of the PID
	iterm = fast_rshift_long(accumulator, ishift);

	// Figure out proposed new_coil_dac setting from I sum
	new_coil_dac = ihold_coil_start_value + iterm;  // old, PID only way

    // new_coil_dac = last_coil_dac - coil_v_mon + iterm;      // New FeedForward + PID


	//---> Compute offset from previous CDAC setting, and clip to prevent large changes
	long delta = new_coil_dac - last_coil_dac;
	if( delta >=  256 )
		delta =  256;
    else if( delta <= -256 )
	 	delta = -256;

	//---> Change allowable coil voltage based on current limits
	int v_max = coil_v_max, v_min = coil_v_min;
	if( coil_i_sense >= coil_i_max )  {
		v_max = 0;
		add = 0;		// anti-windup
	}
    else if( coil_i_sense <= coil_i_min )  {
		v_min = 0;
		add = 0;		// anti-windup
	}

	//---> Impose coil voltage maximums to limit delta
	long new_coil_v = (long)delta + (long)coil_v_mon;	// any change in CDAC goes straight to coil voltage
	if( new_coil_v > v_max )  {
		delta = v_max - coil_v_mon;
		add = 0;		// anti-windup
	}
    else if( new_coil_v < v_min )  {
		delta = v_min - coil_v_mon;
		add = 0;		// anti-windup
	}

	//---> Add delta to our previous dac setting to compute new setting
	new_coil_dac = last_coil_dac + delta;

	//---> Update the accumulator for next time, if no clipping is in effect
	if( add )
		accumulator += error;

	//---> Clip new_coil_dac to 16 bits
	// New_coil_dac is a LONG, we must truncate to make it fit in a 16 bit value.
    short toret;

	if( new_coil_dac <= -32767 )
		toret = -32767;
	else if( new_coil_dac >= 32767 )
		toret = 32767;
	else
		toret = (short)new_coil_dac;

    ++tick;     //#DEBUG
	PIN_OSCOPE1 = 0;

    //#Debug print statements
    /*
    if (!(tick & 0x00f)) {
        TTYPutsFromInt("e: ");
        TTYPrintHex32(error);
        TTYPutsFromInt(" - i: ");
        TTYPrintHex32(accumulator);
        TTYPutsFromInt(" - r: ");
        TTYPrintHex16(toret);
        TTYPutsFromInt(" - add: ");
        TTYPrintHex8(add);
        TTYPutsFromInt("\r\n");
    }
    */
    return toret;
}


/*
 * Interface routines for the outside world.
 */
void ihold_set_ishift( unsigned char _ishift )
{
    // Rescale the accumulator to prevent a big jump
    int diff = (int)ishift - (int)_ishift;
    if (diff >=0) {
        accumulator = accumulator >> diff;
    } else {
        accumulator = accumulator << -diff;
    }
    
	ishift = _ishift;
}

// Set the PID setpoint in counts
void ihold_set_s( short sp )
{
    ihold_setpoint = sp;
}
// Get the PID setpoint in counts
short ihold_get_s( void )
{
	return ihold_setpoint;
}

// Get the ishift value
short ihold_get_ishift( void )
{
	return ishift;
}

// reset the accumulator to 0
void ihold_reset_integrator()
{
	accumulator = 0;
}

// Set coil limits in ADC units
void ihold_set_coil_v_limits(short v)
{
    if (v < 0) {
        v = -v;
    }
    coil_v_max = v;
    coil_v_min = -v;
}

/*
 * Sends the PID terms to the frame buffer
 */
void ihold_send_params_to_frame(void)
{
    fb_put32( ihold_setpoint );
    fb_put32( error );
    fb_put32( accumulator );
    fb_put8( 0 );  // p coefficient is always 0
    fb_put8( ishift ); // i coefficient is always 1, report i_shift instead
}
