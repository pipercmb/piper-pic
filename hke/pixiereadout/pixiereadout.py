#!/bin/env python

"""
psquid.py
jlazear
2013-06-20

Python interface classes for PixieReadout board.

Long description

Example:

# pro = PixieReadout('/dev/tty.usbserial-A10180PC')
# f = pro.data(t=1., filename='testdata.txt')
"""
version = 20131018
releasestatus = 'beta'

from numpy import *
import serial
import inspect
import time
from collections import Iterable
from types import StringTypes
import cereal
from serial import SerialException
import os
from tempfile import NamedTemporaryFile
from proerrors import PixieReadoutError


class PixieReadout(object):
    """
    An interface class for interacting with the PixieReadout board.
    """
    def __init__(self, port=None, baudrate=1250000, timeout=1, throttle=False,
                 **kwargs):
        self.connection = PixieReadoutSerial(port, baudrate, timeout,
                                       **kwargs)
        self.connected = self.connection.ser.isOpen()

        self.locked = False
        self.locked_ch = None

        self.throttle = bool(throttle)

    def __del__(self):
        try:
            self.connection.close()
            self.connection.stop()
        except:
            pass
        del self.connection

    def __repr__(self):
        idstr = hex(id(self))
        state = self.connection.state
        toret = ('<pixiereadout.PixieReadout object at {id}: state = '
                 '{state}>'.format(id=idstr, state=state))
        return toret

    def close(self):
        if self.connection.state == 'data':
            try:
                self.data_off()
                time.sleep(0.2)
            except:
                pass
        try:
            self.connection.stop()
            self.connection.close()
        except:
            pass

    def set_port(self, port):
        """
        Sets the port and attempts to open the connection.
        """
        self.connection.port = port
        try:
            self.connection.open()
            self.connected = self.connection.ser.isOpen()
        except SerialException:
            raise ValueError('Invalid port: {port}'.format(port=port))

    #-----------------------------------------------------------#
    #--------------------- User Commands -----------------------#
    #-----------------------------------------------------------#

    def data(self, t=1., f=None, delete=True):
        """
        Enable data reporting mode.

        :Arguments:
            t - (float or 'on' or 'off' or bool)
            The argument to the data command:
                float - duration to collect data for, in seconds
                'on'  - collect data until told to stop
                'off' - stop collecting data
                bool  - True -> 'on', False -> 'off'

            f - ([optional]file handle/str) The file to record the
            data into. If None, creates a temporary file in the
            current directory.

            delete - ([optional]bool) If a temporary file is used, whether
            or not the file should be deleted when closed.

        :Returns:
            f - (file handle) File handle of file to which data is
            being saved.
        """
        if t == 'on':
            arg = 'on'
        elif t == 'off':
            return self.data_off()
        elif isinstance(t, bool):
            if t:
                arg = 'on'
            else:
                return self.data_off()
        elif isinstance(t, (int, float)):
            arg = '{0:.2f}'.format(float(t))
        else:
            if t:
                arg = 'on'
            else:
                return self.data_off()

        if f is None:
            f = NamedTemporaryFile(mode='wb', delete=delete)

        self.connection.set_state('header', f)
        self._data(arg)
        return f

    def data_off(self):
        """
        Turn off any data reporting.
        """
        self._data_off()

    def ver(self, readresponse=False):
        """
        Queries the board for its PIC code version.
        """
        return self._ver(readresponse)

    #-----------------------------------------------------------#
    #-------------------- Serial Functions ---------------------#
    #-----------------------------------------------------------#
    def write(self, *args, **kwargs):
        """
        Send an arbitrary string to the board. Simply calls
        self.connection.write.
        """
        return self.connection.write(*args, **kwargs)

    def read(self, *args, **kwargs):
        """
        Reads an arbitrary string from the board. Simply calls
        self.connection.read.
        """
        return self.connection.read(*args, **kwargs)

    def send(self, command, args=(), readresponse=False,
             terminator='\r', throttle=None):
        """
        Sends a command to the board. Arguments to the command may be
        included in arglist. Reads the response if readresponse is
        True. Correctly appends termination strings.
        """
        if not isinstance(args, str):
            try:
                argstr = ' '.join((str(arg) for arg in args))
            except TypeError:
                return
        else:
            argstr = args

        if throttle is None:
            throttle = self.throttle

        space = ' ' if argstr else ''
        towrite = command + space + argstr + terminator
        print "towrite = ", repr(towrite) #DELME
        self.write(towrite)
        if throttle:
            time.sleep(0.05)  # Only allow one command every 50 ms

        if readresponse:
            response = self.readresponse()
            return response

    def readresponse(self, terminator='OK!\r\n', timeout=2.):
        """
        Reads the response to a command. Continues reading until it
        encounters the terminator string or reads for longer than the
        timeout period.
        """
        t0 = time.time()
        t = time.time()
        buf = ''
        char = None
        while not buf.endswith(terminator):
            t = time.time()
            if (t - t0 > timeout):
                raise PROTimeoutError
            char = self.connection.read(1)
            buf += char

        return buf

    #-----------------------------------------------------------#
    #---------------------- Raw Commands -----------------------#
    #-----------------------------------------------------------#

    def _data(self, arg):
        """
        Enable data reporting.

        Arguments:
        <ch=integer dac identifier>
        <navg=int>

        Reports back (ADC sample, read DAC code) pairs until interrupted.
        Averages together navg samples before reporting back.
        """
        args = [arg]
        self.send('data', args=args)

    def _data_off(self):
        """
        Terminate the data reporting.

        Arguments:
        None

        Terminates the reporting of data. Resets all data parameters.
        """
        self.send('data off')
        time.sleep(.01)

    def _ver(self, readresponse=False):
        """
        Spits out a version string for the current code.

        Usage:
        No arguments
        """
        return self.send('ver', readresponse=readresponse)



class PixieReadoutSerial(cereal.Cereal):
    """
    A stateful class that is meant to handle serial monitoring and
    communication for the PixieReadout interface.

    Owns a serial connection, a state, and (sometimes) a file handle.

    The serial connection allows it to communicate with the board.

    The state indicates how the communication should be handled (i.e.
    what should SerialThread be doing while communicating, e.g. does
    SerialThread need to record data from a sweep command?).

    The file handle is the location to which PixieReadoutSerial will
    stream data if it needs to read data.

    A file is used as the storage location because it maximally
    decouples any operation PixieReadout wants to do on the data from
    the recording of the data by PixieReadoutSerial.

    Subclasses cereal.Cereal because Cereal is already a buffered
    serial port driver. All we need to do is dump the buffer to the
    file handle and add statefulness.

    Note that PixieReadoutSerial is run in a separate thread because we do
    not want any modes of communication that require monitoring and
    polling the serial port (e.g. reading the output during sweeps) to
    be blocking to the PixieReadout class.
    """
    # Variables with these names should not be passed to the
    # underlying serial connection object.
    localvars = ('ser', 'timeout', 'buffer', 'running', 'state',
                 'filehandle', '_statedict', '_setstatedict',
                 'chunksize', 'startstring', 'headerlen')

    def __init__(self, port=None, baudrate=115200, timeout=1, chunksize=1017,
                 **kwargs):
        super(PixieReadoutSerial, self).__init__(port=port,
                                                 baudrate=baudrate,
                                                 timeout=timeout,
                                                 **kwargs)

        self.state = 'do nothing'
        self.filehandle = None

        # Dictionary of actions to take according to the state
        self._statedict = {'do nothing': self._do_nothing,
                           'data': self._data,
                           'header': self._header}

        # Dictionary of functions to call to change the state
        self._setstatedict = {'do nothing': self._set_do_nothing,
                              'data': self._set_data,
                              'header': self._set_header}

        self.chunksize = (int(chunksize)/9)*9
        self.startstring = '!---DATA ON---!\r\n'  # Length of signal string
        self.headerlen = 37  # Length of full header

    def run(self):
        """
        Essentially a copy of Cereal.run(), except with
        self.handledata() added into the loop.
        """
        while not self.stopped.isSet():
            try:
                if self.openflag:
                    with self.bufferlock:
                        try:
                            iw = self.ser.inWaiting()
                            toadd = ''
                            # iw = max(1, int(iw))
                            # inWaiting will never report back more than 1020,
                            # even if there are more than that waiting
                            while iw:
                                toadd = self.ser.read(iw)
                                # time.sleep(.001)
                                iw = self.ser.inWaiting()
                        except (ValueError, TypeError):
                            toadd = ''
                        except (OSError, serial.SerialException):
                            toadd = ''
                        self.buffer += toadd
                time.sleep(.005)    # Must be less than 30 ms, time to fill
                                    # 3896 buffer at 1.25 Mbaud
                self.handledata()   # Deal with data by state
            except KeyboardInterrupt:
                self.stopped.set()

    def stop(self):
        """
        Adds a lock around the port access so there are no race conditions
        between closing the port in stop() and reading the port in run().
        """
        # DELME Shutdown procedure seems to cause segfault/illegal
        # instruction, need to sort that out at some point.
        # Not a big deal right now since happens near end of shutdown
        # procedure and everything has already been safely closed by that
        # point
        with self.bufferlock:
            # super(PSquidSerial, self).stop()
            if self.isAlive():
                self.stopped.set()
                self.join()
            self.ser.close()

    #-----------------------------------------------------------#
    #-------------------- Utility Functions --------------------#
    #-----------------------------------------------------------#
    def set_state(self, newstate, *args, **kwargs):
        """
        Changes the state.
        """
        try:
            self._setstatedict[newstate](*args, **kwargs)
        except KeyError:
            raise PSquidInvalidStateError(newstate)

    def _set_do_nothing(self, *args, **kwargs):
        """
        Change to the 'do nothing' state.
        """
        if self.filehandle is not None:
            self.filehandle.close()
            self.filehandle = None

        self.flushInput()
        self.state = 'do nothing'

    def _set_data(self, *args, **kwargs):
        """
        Change to the 'data' state. Must pass in a file handle or
        filename.
        """
        self.state = 'data'

    def _set_header(self, f, headerdict=None, *args, **kwargs):
        # Close the currently used file handle
        if self.filehandle is not None:
            self.filehandle.close()

        # Make new file handle
        if isinstance(f, file):
            self.filehandle = f
        else:
            try:
                self.filehandle = open(f, 'wb')
            except TypeError as e:
                try:
                    f.file
                    self.filehandle = f
                except AttributeError:
                    raise e


        if headerdict:
            self._write_header(headerdict)
        self.flushInput()
        self.state = 'header'

    def handledata(self):
        """
        Handle the data according to the state
        """
        try:
            self._statedict[self.state]()  # Call appropriate function
        except PROWriteToClosedFileError:
            self.set_state('do nothing')

    def _do_nothing(self):
        """
        The 'do nothing' state. Used when the PixieReadout board is not
        reporting anything back except command responses.
        """
        if self.buffer.endswith('\r\n'):
            print repr(self.read(self.inWaiting())) #DELME #FIXEME

        # Parse for triggered data collection, do logic stuff

    def _data(self):
        """
        The 'data' state. Used when the PixieReadout board is in the data
        collection state and is reporting back data.
        """
        if self.filehandle is None:
            return

        with self.bufferlock:
            while len(self.buffer) >= 9:
                lentoread = (len(self.buffer)/9)*9
                if self.buffer[0] == '*' and self.buffer[lentoread-9] == '*':
                    line = self._unsafe_read(lentoread)
                    self._write_to_file(line)
                    # # Read a large chunk if possible (multiple of 9)
                    # if (len(self.buffer) >= self.chunksize and
                    #    self.buffer[self.chunksize - 9]):
                    #     line = self._unsafe_read(self.chunksize)
                    #     self._write_to_file(line)
                    # else:
                    #     # Else read in increments of 9
                    #     line = self._unsafe_read(9)
                    #     self._write_to_file(line)
                elif self.buffer[0] == '*':
                    line = self._unsafe_read(9)
                    self._write_to_file(line)
                elif self.buffer[:9] == '!---DATA ':
                    line = self.readline()
                    self._write_to_file(line)
                    self.set_state('do nothing')
                else:
                    try:
                        star = self.buffer.index('*')
                    except ValueError:
                        star = 'none'
                    try:
                        excl = self.buffer.index('!')
                    except ValueError:
                        excl = 'none'

                    next = min(star, excl)
                    if isinstance(next, int):
                        line = self._unsafe_read(next)

    def _header(self):
        """
        The 'header' state. Searching for a header and writing it to disk,
        then setting to 'data' state once header has been found and recorded.
        """
        with self.bufferlock:
            if len(self.buffer) >= self.headerlen:
                header = self._unsafe_read(self.headerlen)
                if header.startswith(self.startstring):
                    self._write_to_file(header)
                    self.set_state('data')
                else:
                    self.set_state('do nothing')
                    raise PROInvalidHeaderError(header)

    def _write_to_file(self, towrite):
        """
        Forces a write to disk, i.e. writes to the file buffer then
        flushes the file buffer to disk.

        This guarantees that the string written to the file will
        appear immediately in the file, which is useful if some other
        program is monitoring the file (e.g. kst or pyoscope).
        """
        try:
            self.filehandle.write(towrite)
            # self.filehandle.flush()
            # os.fsync(self.filehandle.fileno())
        except AttributeError:
            raise PROWriteToClosedFileError

    def _write_header(self, headerdict, comments='#'):
        """
        Writes the key-value pairs in headerdict to the file header.
        """
        for key, value in headerdict.iteritems():
            # ['adc', 'locked'] -> '[adc, locked]'
            if isinstance(value, Iterable) and \
               not isinstance(value, StringTypes):
                value = [str(v) for v in value]
                value = '[{0}]'.format(', '.join(value))
            towrite = "{com} {key}: {value}\n".format(com=comments, key=key,
                                                      value=value)
            self._write_to_file(towrite)


#-----------------------------------------------------------#
#---------------------- Error Classes ----------------------#
#-----------------------------------------------------------#
class PROArgumentError(PixieReadoutError):
    """
    An error indicating that there was an error with the arguments
    passed to a PixieReadout function.
    """
    def __init__(self, values, vartypes):
        if isinstance(values, (int, float)):
            values = [values]
        if isinstance(vartypes, (int, float)):
            vartypes = [vartypes]

        # Get method that raised error
        callername = inspect.stack()[1][3]
        method = getattr(PixieReadout, callername)

        # Get arguments of method
        argnames = inspect.getargspec(method)[0]
        argnames = argnames[1:]  # Get rid of 'self' argument

        self.msg = ("Invalid argument passed to "
                    "PixieReadout.{0}. Expected (Got): ".format(callername))
        toappend = []
        checks = PixieReadout._check_types(values, vartypes, summed=False)
        for i, check in enumerate(checks):
            if not check:
                vartype = vartypes[i]
                value = values[i]
                low, high = PixieReadout.vartypes[vartype]
                name = argnames[i]
                temp = "{0} <= {1} ({2}) <= {3}".format(low, name,
                                                        value, high)
                toappend.append(temp)

        toappend = ' and '.join(toappend)
        self.msg += toappend

    def __str__(self):
        return self.msg


class PROTimeoutError(PixieReadoutError):
    """
    An error indicating that something in PixieReadout timed out.
    """
    def __init__(self):
        self.msg = "PixieReadout timed out!"

    def __str__(self):
        return self.msg


class PROInvalidStateError(PixieReadoutError):
    """
    An error indicating the specified state is invalid.
    """
    def __init__(self, newstate):
        self.msg = "Attempted to change to an invalid state: "
        self.msg += str(newstate)

    def __str__(self):
        return self.msg


class PROInvalidHeaderError(PixieReadoutError):
    """
    An error indicating that received an unexpected header.
    """
    def __init__(self, header):
        self.msg = "Received unexpected header: "
        self.msg += repr(str(header))

    def __str__(self):
        return self.msg


class PROWriteToClosedFileError(PixieReadoutError):
    """
    An error indicating that somebody tried to write to a non-existent file.
    """
    def __init__(self, fname=None):
        if fname is None:
            self.msg = "No file specified!"
        else:
            self.msg = "Attempted to write to closed file: {0}"
            self.msg = self.msg.format(fname)

    def __str__(self):
        return self.msg
