#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "pixiereadout.h"
#include "version.h"
#include "../common/ptty.h"
#include "../common/common.h"

// Local function prototypes
static char do_cmd_p(void);                // prints status
static char do_cmd_version(void);          // prints version of the code
static char do_cmd_sample_delay_us(void);  // Set EXT_CONVERT phase in degrees
static char do_cmd_data(void);             // Report back data until told to stop, or specified num samples
static char do_cmd_trigger(void);          // Trigger data collection on extern

#define CMP	!strcmp

/*
 * Process a user command contained in the command buffer.
 */
void process_cmd(char *cmd)
{
    char *s;
    unsigned char r;
    char silent;

    // Parse the command string
    cmd_parse(cmd);

    s = tok_get_str(); // next token is the command name

    if (CMP(s, "p")) r = do_cmd_p(); // prints dac status
    else if (CMP(s, "ver"))        r = do_cmd_version();        // prints software version
    else if( CMP( s,"sdus" ))      r = do_cmd_sample_delay_us();// Set the EXT_CONVERT delay in us
    else if( CMP( s,"data" ))      r = do_cmd_data();           // Report back specified number of samples or told to stop
    else if( CMP( s,"trigger" ))   r = do_cmd_trigger();        // Report back specified number of samples or told to stop
    else r = STATUS_FAIL_CMD; // User typed unknown command

    // Print out the status, usually "OK!\r\n"
    silent = get_silent_flag();
    if (!silent) {
        print_status(r);
    }
}

/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/

/** P
 *      Prints board status.
 *
 *      Usage:
 *      p   -> Prints board status
 *
 */
static char do_cmd_p(void)
{
    char str[160];

    sprintf(str, "*PIXIEREADOUT\r\n");
    TTYPuts(str);

    sprintf(str, " trigger = %d\r\n", get_trigger_mode() );
    TTYPuts(str);

    return STATUS_OK;
}

/** VER
 *	Spits out a version string for the current code.
 *
 *	Usage:
 *	No arguments
 */
static char do_cmd_version(void)
{
    // Print the board name and mode
    char silent = get_silent_flag();
    if (!silent) {
        TTYPuts("*PIXIEREADOUT ");

        TTYPuts(VERSION_STRING);

    }
    return STATUS_OK;
}

/*-----------------------------------------------------------
 * PIXIEREADOUT Commands
 *-------------------------------------------------------------*/

/** SDUS
 *  Sets the ADC Sample Delay in micro seconds.
 *  Use this to maximize the demodulated signal by aligning the sampling with the peaks of
 *  the 2 kHz bias sine wave.
 *  Arguments:
 *    <delay=sampling delay in us, [0, max]>
 *    max depends on the sampling rate.  For 8 kHz, it is 124 us.
 *  Examples:
 *    phase 90
 */
static char do_cmd_sample_delay_us(void)
{
    char str[100];

    // Check that the next token is available
    if( !tok_available() )
        return STATUS_FAIL_NARGS;
    // Get delay in us
    if (!tok_valid_num(3, 0, 0))
        return STATUS_FAIL_INVALID;
    int usec = tok_get_int16();

    if (usec >= PR_US_PER_TICK)  {
        sprintf(str, "*PXREADOUT: sampling delay must be less than %u us\r\n", PR_US_PER_TICK);
        TTYPuts(str);
        return STATUS_FAIL_RANGE;
    }

    // Convert from degrees to clock cycles
    unsigned ncycles = (unsigned)(((unsigned long)usec * PR_CYCLES_PER_TICK) / PR_US_PER_TICK);

    set_sampling_delay(ncycles);
    sprintf(str, "*PXREADOUT: sampling delay set to %d us (%u cycles)\r\n", usec, ncycles);
    TTYPuts(str);

    return STATUS_OK;
}

/** DATA
 *  Initiates data collection.
 *
 *  Arguments:
 *    <seconds of data to take (optional, can be fractional)> or "on" or "off"
 *  Examples:
 *    data -- begins taking data.  Collection continues till data off
 *    data off -- stops data collection
 *    data 3.5 -- takes 3.5 seconds worth of data
 *    data 3.5 -- takes 3.5 seconds worth of data
 */
static char do_cmd_data(void)
{
    char str[100];

    // The default behavior, for no arguments, is to turn data collection on forever
    enum DataState dstate;
    unsigned long num_samples = 0;  // 0 means take data forever

    // See what state we're currently in
    dstate = get_DataState();

    // Attempt to read in the optional number of seconds to collect data for
    if( tok_available() )  {
        if (tok_valid_num(7, 0, 1))  {
            float sec = tok_get_float();

            // Impose some reasonable maximum, so we don't overflow the integer sample counter
            // Can always take data forever
            if (sec >= (24L*3600*PR_CYCLES_PER_TICK) )  {
                sprintf(str, "*PXREADOUT: number of samples is too large\r\n");
                TTYPuts(str);
                return STATUS_FAIL_RANGE;
            }
            num_samples = (unsigned long)(sec * PR_TICKS_PER_SEC);
            dstate = Initiated;         // Will cause header to print, then change state to on
        }
        else {
            char *arg = tok_get_str();
            if( CMP(arg,"on") ) {
                if( dstate != On) {     // Only turn on if we were not already on
                    num_samples = 0;
                    dstate = Initiated;       // Will cause header to print, then change state to on
                }
            }
            else if( CMP(arg,"off"))  {
                if( dstate == On)
                    dstate = Ending;     // if we were actively collecting data, cause footer to be printed (ends data collection gracefully)
                else
                    dstate = Off;        // turn off data collection (no footer printed)
            }
            else
                return STATUS_FAIL_INVALID;
        }
    }

    set_DataState(dstate, num_samples);

    return STATUS_OK;
}


/** TRIGGER
 *  Sets the board trigger mode.
 *  The trigger is a digital line on the B-row of the backplane that is
 *  controlled by the Pixie Bias board.  This enables the bias board to
 *  start / stop data collection on the readout board.
 *  Available modes are "on" and "off".
 *
 *  On - data collection will begin whenever the trigger line goes high
 *       and will stop whenever the trigger line goes low.
 *  Off - the trigger line is ignored
 *
 *  Arguments:
 *    "on" or "off"
 *  Examples:
 *    trigger on
 *    trigger off
 */
static char do_cmd_trigger(void)
{
    if( tok_available() )  {
        char *arg = tok_get_str();

        if( CMP(arg,"on") )
            set_trigger_mode(TRIGGER_MODE_ON);
        else if( CMP(arg,"off"))
            set_trigger_mode(TRIGGER_MODE_OFF);
        else
            return STATUS_FAIL_INVALID;
    }

    return STATUS_OK;
}
