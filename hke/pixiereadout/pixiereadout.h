#ifndef __PIXIEREADOUT_INCLUDED
#define __PIXIEREADOUT_INCLUDED

// Include the standard include files for the project
#include "../bp_common/bp_common_base.h"
#include "../common/common.h"
#include "../common/ptty.h"


//---------> Some useful stuff
#define CLOCKFREQ 20000000L		// 20.000 MHz instruction clock (synthesised from 5 MHz crystal, 16x PLL)

#define PR_CYCLES_PER_TICK  2500        // 125 us = 8 kHz sampling rate (turbo = 2)
#define PR_CONV_CYCLES       1000        //  50 us - Convert must remain high while all 4 ADCs are read out
#define PR_INTERRUPT_DELAY_CYCLES    100 //   5 us - Delay after rising edge of convert before interrupt handler triggered
                                         //          must be > 3.2 us which is maximum convert time in data sheet

// Number of microseconds per interrupt (adc sample).  Should be an integer!  Computed weirdly to prevent overflow issues
#define PR_US_PER_TICK  ((unsigned)(((long)PR_CYCLES_PER_TICK * 10000L) / (CLOCKFREQ/100)))

// Sampling rate in samples per second
#define PR_TICKS_PER_SEC ((unsigned)(CLOCKFREQ/PR_CYCLES_PER_TICK))

// The number of ADC channels
#define NCH 4

// Data collection modes / states
enum DataState {
    Off=0,          // Data collection is turned off
    Initiated=1,    // Data collection just started.  We write the header out and transition to "on"
    On=2,           // Data collection is turned on, stops after N samples, or stopped by user
    Ending=3        // Collection is over, write out the end of file marker
};

// Trigger modes
#define TRIGGER_MODE_ON     1       // Data collection will start / stop based on PIN_EXT_TRIGGER
#define TRIGGER_MODE_OFF    0       // Data collection controlled by "data" command


//-----> OUTPUT PINS
#define PIN_LED				LATBbits.LATB5	 // Front pannel LED
#define PIN_CS_ADC0			LATBbits.LATB11	 // ADC0 CS BAR
#define PIN_CS_ADC1			LATBbits.LATB10	 // ADC1 CS BAR
#define PIN_CS_ADC2			LATBbits.LATB9	 // ADC2 CS BAR
#define PIN_CS_ADC3			LATBbits.LATB8	 // ADC3 CS BAR

#define PIN_BUCK_DAC_LD                 LATBbits.LATB12  // Bucker amplitude dac load pin
#define PIN_POT_SYNC                    LATBbits.LATB13  // Bucker phase pot load

#define PIN_GAIN_A0                     LATDbits.LATD8   // Post bucker amplifier gain A0
#define PIN_GAIN_A1                     LATDbits.LATD9   // Post bucker amplifier gain A1
#define PIN_MODE                        LATDbits.LATD11  // 0 = low gain / 1 = high gain ????

// Diagnostic outputs
#define PIN_OSCOPE1			LATDbits.LATD7	// Debug pins for looking at with scope
#define PIN_OSCOPE2			LATDbits.LATD6
#define PIN_OSCOPE3			LATDbits.LATD5
#define PIN_OSCOPE4			LATDbits.LATD4

//------> INPUT PINS
#define PIN_EXT_TRIGGER         PORTDbits.RD0           // Triggers data data collection scan (from bias board)
#define PIN_FRAME_CLK		PORTDbits.RD2
#define PIN_SYNC_CLK            PORTDbits.RD3


//---------> Function Prototypes
// psquid.c
void init_pic( void );		// Init the PIC ports and hardware

// process_cmd.c
void process_cmd( char *cmd );

// do_cmd.c
void write_mux( unsigned short ch );

// cmd_gets.c
void cmd_gets( char *str, int n );

// int_handler.c
void set_DataState(enum DataState state, unsigned long num_samples);
enum DataState get_DataState(void);

void set_trigger_mode(int trigger_mode);
int get_trigger_mode();

int get_silent_flag(void);
void set_sampling_delay(unsigned p);


#endif