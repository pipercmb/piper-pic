#!/bin/env python

"""
pixiereadout.py
jlazear
2013-10-18

pyoscope reading class for reading pixiereadout data.
"""
version = 20131018
releasestatus = 'beta'

import numpy as np
import pandas as pd
from types import StringTypes
from tempfile import _TemporaryFileWrapper

class ReaderInterface(object):
    """
    A reader "interface". Simply lists the methods that a pyoscope
    reader must implement.

    Readers are not meant to be threaded. Any threading would happen
    by the object that owns the reader. They should, however,
    facilitate threading by providing the `update_data` method.

    A reader must implement the following methods:

        reader.init_data(*args, **kwargs)
            - Intial read of data
            - Returns a single numpy array with the data in columns.
              May be a structured array or Pandas DataFrame.
        reader.update_data()
            - Reads changes to the data file
            - Returns a single updated numpy/pandas array.
            - Note that this does not necessarily require making a new
              array. init_data may return a reference to an array that
              reader owns, then update_data could update the same
              array, and update_data could return a reference to the
              same array.
        reader.switch_file(f, *args, **kwargs)
            - Switches which file is being read.
        reader.close()
            - Closes and makes safe the file.

    and the following attribute:

        reader.filename
            - Filename of read file
    """
    def __init__(self, f, *args, **kwargs):
        # Load file
        if isinstance(f, file):
            self.f = f
        elif isinstance(f, StringTypes):
            self.f = open(f, 'r')
        else:
            raise TypeError('f must be a file handle or filename.')
        self.f.seek(0)
        self.filename = self.f.name

        # Other init stuff...

    def close(self):
        self.f.close()

    def init_data(*args, **kwargs):
        """
        Initial read of the data.

        Returns a single array with the data in columns. May be a
        structured array or Pandas DataFrame.
        """
        data = np.empty((1, 1))
        return data

    def update_data():
        """
        Reads changes to the data file.

        Returns a single updated numpy/pandas array.

        Note that this does not necessarily require making a new array
        (and thereby consuming more resources). One could store the
        data array in the reader object and return a reference to that
        array from `init_data`, then update the same array in
        `update_data`, and return a reference to the same array.
        """
        data = np.empty((1, 1))
        return data

    def switch_file(self, f, *args, **kwargs):
        # Load file
        if isinstance(f, file):
            self.f = f
        elif isinstance(f, StringTypes):
            self.f = open(f, 'r')
        else:
            raise TypeError('f must be a file handle or filename.')
        self.f.seek(0)

        return self.init_data(*args, **kwargs)


class PixiereadoutReader(ReaderInterface):
    """
    Interface for reading the data from the PixieReadout board.
    """
    def __init__(self, f, header=True, *args, **kwargs):
        super(PixiereadoutReader, self).__init__(f, *args, **kwargs)

        self.header = {} #DELME?
        if header:
            self.header = self._read_header()
        else:
            self.header = {}

    def _read_header(self):
        hdict = {}
        _ = self.f.readline()  # Discard first line
        line = self.f.readline()
        phase, count = [int(s.split(':')[1], 16)
                        for s in line.strip().split('-')]
        hdict['phase'] = phase
        hdict['count'] = count
        hdict['headerlength'] = self.f.tell()
        return hdict

    def init_data(self, *args, **kwargs):
        if self.f.closed:
            raise ValueError('I/O operation on closed file.')
        self.args = args
        self.kwargs = kwargs

        self.f.seek(self.header['headerlength'])
        ar = []

        line = self.f.read(9)
        while line:
            if line.startswith('*'):
                ln = line[1:]
                # print "ln = ", repr(ln) #DELME
                vals = [self._bin_to_int(ln[2*n:2 + 2*n])
                            for n in range(len(ln)/2)]
                ar.append(vals)
                line = self.f.read(9)
            elif line.startswith('!'):
                line += self.f.readline()
                # print "line = ", repr(line) #DELME
                line = self.f.read(9)
            else:
                # print "failed line = ", repr(line) #DELME
                raise ValueError(
                    'Packet did not start with *!: {0}'.format(line))

        data = pd.DataFrame(ar)
        return data

    @staticmethod
    def _bin_to_int(binval):
        msb, lsb = [ord(val) for val in binval]
        val = (msb << 8) + lsb
        # print "msb, lsb, val = ", msb, lsb, val #DELME
        return val

