#include <p30F5011.h>

#include "pixiebias.h"

/*
 * ch - DAC              - Macro
 * 0  - Bias Generator   - DAC_BIAS_GEN
 * 1  - Bias Amplitude   - DAC_BIAS_AMP
 * 2  - Tickle Generator - DAC_TICKLE_GEN
 * 3  - Tickle Amplitude - DAC_TICKLE_AMP
 * 4  - Heater           - DAC_HEATER
 *
 * May use pre-defined macros to specify channels.
 */
void write_dac(unsigned val, int ch)
{
    unsigned short dummy;
    unsigned mask = 0x0100;  // Start with a 1 in RB8

    // Send the data
    SPI2STATbits.SPIROV = 0;		// Clear SPI receive overflow bit
    SPI2BUF = val;                      // Write to the SPI port
    while (!SPI2STATbits.SPIRBF);	// Wait for data to be ready on SPI port
    dummy = SPI2BUF;

    // Lower the DAC chip select
    mask <<= ch;             // Left-shifting by n moves 1 to RB(8+n)
    LATB &= ~mask;           // AND lowers bit at RB(8+n)

    // Not actually necessary, line is dropped low sufficiently long without it
    //    Nop();                   // t_LD = 60 ns = 1.2 cycles, 1 should be enough
                             // considering overhead in surrounding instructions

    // Raise the DAC chip select - DAC chip reads in values on rising edge of CS
    LATB |= mask;
}