#include <p30F5011.h>
#include <libpic30.h>
#include <stdio.h>
#include <stdlib.h>
#include <spi.h>
#include <timer.h>

#include "../common/psync_common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"
#include "psyncadc-swg.h"
#include "commutation_table.h"

static void update_commutation(void);
static void update_abs_position(void);
static void update_com_sine(int ch, int index);
static void update_velocity(void);
static long measure_encoder_delta(void);
static void update_data(void);

int tick = 0;
unsigned int tick2 = 0;
static char data_flag = 0;

int enc_pos = 0; // Encoder position [0-Encoder_MAX]
unsigned char found_home = 0;
static int lead_angle = 0;
long abs_pos = 0;
int velocity = 0;  // Latest measured velocity, in encoder counts per 1024 ticks
unsigned char new_velocity = 0;  // Whether or not a velocity measurement is current

int amplitude;  // Negative amplitude spins in reverse direction

int kick_counter;  // #DELME?

unsigned char mode = MODE_FREERUN;

void __attribute__((__interrupt__, __auto_psv__)) _OC1Interrupt(void)
{
    PIN_OSCOPE2 = 1;
    IFS0bits.OC1IF = 0; // reset Output Compare 1 interrupt flag

    //  update_freq_ramp();

    //  update_sines();     // Update the sine waves

    update_abs_position();

    // If (tick2 mod 1024) == 0; i.e. every 1024 ticks = 0.256 s
    // This gives (1 count)/(1024 ticks)*(4000 ticks/s)/(2500 counts/rev) = 1.5625 mHz precision
    // Want to increase update rate? (inversely proportional to precision... see above)
//    if (!(tick2 & 0x3ff)) {
    if (!(tick2 & 0x7f)) {
        update_velocity();
        if (mode == MODE_PID_VELOCITY)
            amplitude = compute_pid_vel(velocity);
    }

    if (mode == MODE_PID_POSITION)
        amplitude = compute_pid_pos(abs_pos);

//    else (mode == MODE_PID_VELOCITY)
//            amplitude = compute_pid_vel(vel)

    /* attempt at kicks, but it didn't work
    // #DELME?
    if (kick_counter > 0) {
        amplitude = 32767;
        kick_counter--;
        if (kick_counter == 0)
            amplitude = 0;
    }
    // end #DELME?
    */

    update_commutation();

    // Write encoder position out to the dac so we can look at it with the scope
    int dac3 = abs_pos;
    if (mode == MODE_PID_VELOCITY)
        dac3 = velocity<<2;
    DacWrite(DACREG_DAC, 3, dac3);

    // Write data
    if( data_flag ) {
        if (!(tick & 0b11111))  // (4000 Hz)/32 = 125 Hz data rate
            update_data();
    }
    
    // Look for characters in TX queue to send over UART
    TTYUpdateTXNoFrameFast4();

    // >= instead of > (??) #DELME #FIXME?
    if (++tick > 4000) {
        tick = 0;
    }
    if (++tick2 >= 4096) {
        tick2 = 0;
    }

    PIN_OSCOPE2 = 0; 
}

static void update_data(void)
{
    // CHARS_PER_DATA_WRITE = 12.  Update this in psquid.h if you change this function!!!
    TTYPutcFromInt('^');
    TTYPut16FromInt(amplitude);
    TTYPutcFromInt(' ');
    TTYPut32FromInt(abs_pos);
    TTYPutcFromInt('\r');
    TTYPutcFromInt('\n');
}

// Encoder readout interrupt (highest priority)

void __attribute__((__interrupt__, __no_auto_psv__)) _INT4Interrupt(void)
{
    IFS2bits.INT4IF = 0; // reset INT4 interrupt flag -- needed or int handler will not work right!!!

    if (PIN_ENCODER_B) {
        --enc_pos;
        if (enc_pos < 0)
            enc_pos = ENCODER_MAX;
    } else {
        ++enc_pos;
        if (enc_pos > ENCODER_MAX)
            enc_pos = 0;
    }

    if (PIN_ENCODER_HOME) {
        enc_pos = 0;
        found_home = 1;
    }

    PIN_TESTPOINT2 = (enc_pos < 100);
}

// Phase offsets for indexing into the sine table to generate three-phase
#define PHASE_OFFSET2   ((unsigned)(COMMUTATION_TABLE_LEN * 1.0 / 3 + 0.5))
#define PHASE_OFFSET3   ((unsigned)(COMMUTATION_TABLE_LEN * 2.0 / 3 + 0.5))

static void update_commutation(void)
{
    int enc = enc_pos;

    // We need two electrical cycles per real rotation
    if (enc > ENCODER_MAX / 2) // ENCODER_MAX/2 == 1249
        enc -= ENCODER_MAX / 2;

    // Divide by 2 to convert Encoder pos to table since table is only 625 long
    enc >>= 1;

    enc += lead_angle;
    if (enc >= COMMUTATION_TABLE_LEN)
        enc -= COMMUTATION_TABLE_LEN;

    update_com_sine(0, enc);
    update_com_sine(1, enc + PHASE_OFFSET3);
    update_com_sine(2, enc + PHASE_OFFSET2);
}

static void update_com_sine(int ch, int index)
{
    if (index >= COMMUTATION_TABLE_LEN)
        index -= COMMUTATION_TABLE_LEN;

    int tval = commutation_table[index];
    // #DELME
//    if (tval > 0)
//        tval = 32767;
//    else if (tval < 0)
//        tval = -32768;
//    else
//        tval = 0;
    // end #DELME

    // Scale the amplitude (not-needed if you have a separate attenuator DAC)
    long tmp = __builtin_mulss(tval, amplitude); // Note mulss for signed*signed
    tval = tmp >> 15;

    DacWrite(DACREG_DAC, ch, tval);
}

static void update_abs_position(void)
{
    static int old_enc_pos = 0;
    int cur_enc_pos = enc_pos;   // enc_pos is asynchronous, so snapshot it for this function

    int delta = cur_enc_pos - old_enc_pos;
    old_enc_pos = cur_enc_pos;

    // Check for encoder wrap-around. Starts aliasing at 8 kHz = 480 krpm for a 4 kHz interrupt rate.
    // Only works for max(delta) << 2^16.
    if (delta > ENCODER_PULSE_PER_REV/2)
        delta -= ENCODER_PULSE_PER_REV;
    else if (delta < -ENCODER_PULSE_PER_REV/2)
        delta += ENCODER_PULSE_PER_REV;

    abs_pos += delta;
}

// Want to build in divide by 1024? Reduces precision...
static void update_velocity(void)
{
    velocity = (int)measure_encoder_delta();
    new_velocity = 1;
}

// Measures change in encoder position (in encoder counts) since last called.
//
// If called at a fixed cadence, can be used to measure velocity.
static long measure_encoder_delta(void)
{
    long delta;
    static long last_pos = 0;
    long cur_pos = abs_pos;
    delta = cur_pos - last_pos;
    last_pos = cur_pos;

    return delta;
}

char find_home(unsigned char verbose)
{
    char str[100];
    int la; // Lead angle

    found_home = 0;  // Reset found_home flag to make sure search starts fresh

    if (verbose) {
        sprintf(str, "*PSG: Searching for home. Please wait...\r\n");
        TTYPuts(str);
    }

// #DELME?
//    if (found_home) {
//        if (verbose) {
//            sprintf(str, "*PSG: Found home!\r\n");
//            TTYPuts(str);
//        }
//        set_lead_angle(LEAD_ANGLE_OPTIMUM); // Set to optimal lead angle.
//        return EXIT_SUCCESS;
//    }

    for (la = 0; la < 360; la += 30) {
        if (verbose) {
            sprintf(str, "*PSG: Setting lead angle to %d degrees...\r\n", la);
            TTYPuts(str);
        }

        set_lead_angle(la);
        __delay32((long) (1 * CLOCKFREQ)); // Wait 1 s for DACs to power up
        if (get_found_home()) {
            if (verbose) {
                sprintf(str, "*PSG: Found home!\r\n");
                TTYPuts(str);
            }
            set_lead_angle(LEAD_ANGLE_OPTIMUM); // Set to optimal lead angle.
            return EXIT_SUCCESS;
        }
    }
    if (verbose) {
        sprintf(str, "*PSG: Could not find home! Setting amplitude to 0.\r\n");
        TTYPuts(str);
    }
    set_amplitude(0); // Prevent power dissipation in boost since could not find home.

    return EXIT_FAILURE;
}


//-------------- SETTERS / GETTERS --------------

void set_lead_angle(int degrees)
{
    lead_angle = (int) (((long) degrees * COMMUTATION_TABLE_LEN) / 360);
}

// Set sine wave amplitude in raw units (-32768 to 32767)
void set_amplitude(int amp)
{
    amplitude = amp;
}

// Set mode
void set_data_flag(char flag)
{
    data_flag = flag;
}

// Set mode
void set_mode(unsigned char new_mode)
{
    mode = new_mode;
}

// Set encoder to 0
void set_abs_encoder_home(void)
{
    abs_pos = 0;
}

int get_amplitude(void)
{
    return amplitude;
}

unsigned char get_mode(void)
{
    return mode;
}

char get_data_flag(void)
{
    return data_flag;
}

unsigned char get_found_home(void)
{
    return found_home;
}

// Get the current value of the absolute encoder.
long get_abs_encoder(void)
{
    return abs_pos;
}

// Get the current value of the velocity (in encoder counts/tick).
int get_velocity(void)
{
    new_velocity = 0;
    return velocity;
}

// A blocking version of get_velocity... waits on next valid velocity measurement before returning
int get_velocity_blocking(void)
{
    while (!new_velocity) {}  // Block until a new velocity measurement is available
    new_velocity = 0;
    return velocity;
}

int get_lead_angle(void)
{
    return lead_angle;
}

int get_lead_angle_degrees(void)
{
    return (int)(((long)lead_angle*360/COMMUTATION_TABLE_LEN));
}

// #DELME
int get_tick(void)
{
    return tick;
}

// #DELME
int get_tick2(void)
{
    return tick2;
}