#!/bin/env python

"""
pmotor.py
jlazear
2020-01-13

One-line description

Long description

Example:

<example code here>
"""
version = 20200113
releasestatus = 'beta'

import serial
import time

from pmotorreaders import PMotorDataReader as PMDR
from matplotlib.pyplot import *


def initialize(port='/dev/tty.usbserial-DA00E3US', baudrate=417000,
               fname='data.txt'):
    ser = serial.Serial(port=port, baudrate=baudrate)
    f = open(fname, 'wb')
    return ser, f

def send(tosend, ser=None, baudrate=417000,
         port='/dev/tty.usbserial-DA00E3US', eol='\r'):
    if ser is None:
        ser = serial.Serial(port=port, baudrate=baudrate)
    tosend = tosend + eol
    print "tosend = ", repr(tosend) #DELME
    ser.write(tosend)
    if ser is None:
        ser.close()

def send_amplitude(ser, amplitude):
    astr = str(amplitude)[:4]
    astr = 'a {0}'.format(astr)
    send(astr, ser)
    # ser.write('a {0}\r\n'.format(astr))

def send_data_on(ser):
    send('data on', ser)
    # ser.write('data on\r\n')

def send_data_off(ser):
    send('data off', ser)
    # ser.write('data off\r\n')

def write_current_data(ser, f):
    num = ser.inWaiting()
    written = 0
    while num != 0:
        towrite = ser.read(num)
        written += num
        f.write(towrite)
        num = ser.inWaiting()
    return written

def loop(amplitude, fname='data.txt', port='/dev/tty.usbserial-DA00E3US',
         baudrate=417000, init_wait=.1, wait=.01, timeout=5.):
    ser, f = initialize(port, baudrate, fname)
    ser.flushInput()
    ser.flushOutput()
    send_data_on(ser)
    time.sleep(init_wait)
    send_amplitude(ser, amplitude)
    t0 = time.time()
    tc = t0 + 0.
    i = 0
    try:
        while ((tc - t0) < timeout) and (i < 1000):
        # for i in range(10):  #DELME
            num = write_current_data(ser, f)
            time.sleep(wait)
            tc = time.time()
            i += 1
            # print i, num  #DELME
    except Exception as e:
        raise e
    finally:
        print "i_final = ", i #DELME
        print "Cleaning up..."
        send_data_off(ser)
        time.sleep(.1)
        num = write_current_data(ser, f)
        print "FINAL", num
        print "Data off..."
        ser.close()
        print "ser closed..."
        f.close()
        print "f closed..."
        print "Cleaned up."

    pmdr = PMDR(fname, header=False)
    data = pmdr.init_data()
    return data

def loop_cmd(cmd, fname='data.txt', port='/dev/tty.usbserial-DA00E3US',
         baudrate=417000, init_wait=.1, wait=.01, timeout=5.):
    ser, f = initialize(port, baudrate, fname)
    ser.flushInput()
    ser.flushOutput()
    send_data_on(ser)
    time.sleep(init_wait)
    send(cmd, ser)
    t0 = time.time()
    tc = t0 + 0.
    i = 0
    try:
        while ((tc - t0) < timeout) and (i < 1000):
        # for i in range(10):  #DELME
            num = write_current_data(ser, f)
            time.sleep(wait)
            tc = time.time()
            i += 1
            # print i, num  #DELME
    except Exception as e:
        raise e
    finally:
        print "i_final = ", i #DELME
        print "Cleaning up..."
        send_data_off(ser)
        time.sleep(.1)
        num = write_current_data(ser, f)
        print "FINAL", num
        print "Data off..."
        ser.close()
        print "ser closed..."
        f.close()
        print "f closed..."
        print "Cleaned up."

    pmdr = PMDR(fname, header=False)
    data = pmdr.init_data()
    return data

def plot_data(data):
    # amp = data['encoder']
    # enc = data['amplitude']
    amp = data['amplitude']
    enc = data['encoder']
    figure()
    plot(amp, label='amplitude')
    legend()

    figure()
    plot(enc, label='encoder')
    legend()

def condition_file(fname):
    with open(fname, 'rb') as f:
        datalist = []
        for line in f.readlines():
            if not line.startswith('OK!'):
                datalist.append(line)

    towrite = ''.join(datalist)
    with open(fname + 'mod', 'wb') as f:
        f.write(towrite)