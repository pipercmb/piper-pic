/* These functions are the low-level interface to an AD5754 Quad 16-bit DAC.
 * This DAC was included on PSyncADC1 but never used.
 * It was removed from PSyncADC2
 */

#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>

#include "psyncadc-swg.h"

void DacWrite(int reg, int ch, int data)
{
    int data_read = 0; // Data read back from the DAC

    PIN_LOAD_DAC = 0; // Single DAC update mode


    SPI1CONbits.MODE16 = 0; // Set to 8 bit mode
    PIN_DAC_SYNC = 0; // Set DAC mode to low (means dac output is updated immediately upon rising edge of DAC_SYNC)

    // Send control byte
    unsigned char cmd = ((reg & 0x7) << 3) | (ch & 0x7);
    SPI1BUF = cmd;
    while (!SPI1STATbits.SPIRBF); // Wait for xmit to be finished
    data_read = SPI1BUF; // Read in data from DAC, necessary to clear SPIRBF

    // Send data bytes
    SPI1CONbits.MODE16 = 1; // Set to 16 bit mode (also what's needed for ADC)
    SPI1BUF = data;
    while (!SPI1STATbits.SPIRBF); // Wait for xmit to be finished
    data_read = SPI1BUF; // Read in data from DAC, necessary to clear SPIRBF

    PIN_DAC_SYNC = 1; // Latch the data in the DAC

    Nop(); // Delay for latch (probably not needed)
}

void DacInit()
{
    // Set the DAC output ranges (4 = +/- 10V)
    // Datasheet says first communication with DAC should be to set output voltage ranges
    DacWrite(DACREG_OUTPUT_RANGE, 0, 4);
    DacWrite(DACREG_OUTPUT_RANGE, 1, 4);
    DacWrite(DACREG_OUTPUT_RANGE, 2, 4);
    DacWrite(DACREG_OUTPUT_RANGE, 3, 4);

    DacWrite(DACREG_POWER_CONTROL, 0, 0x0f); // Power on all 4 DACS + voltage reference

    DacWrite(DACREG_CONTROL, 1, 0b1100); // Enable thermal shutdown, 20 mA current clamp; all others disabled.
}

void DacTest()
{
    unsigned i;

    __delay32((long) (0.01 * CLOCKFREQ));

    while (1) {
        for (i = 0; i < 65535; ++i) {
            DacWrite(DACREG_DAC, 0, i);
            __delay32(250);
        }
    }
}
