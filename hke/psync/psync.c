#include <p30F5011.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>

#include "../common/stty.h"
#include "../common/tty_print.h"
#include "../common/psync_common.h"

#include "psync.h"


/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL8);          /* External 5 MHz clock with 8xPLL oscillator --> 10 MHz instruction cycle, Failsafe clock off */
_FWDT(WDT_OFF);                         /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN);           /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF);                    /* Code protect disabled */

void init_pic()
{
	// Set up values for GPIO pins
	PIN_LED = 1;		// Turn on LED1

	// Set the direction for GPIO
	TRISBbits.TRISB10 = 0;	// Set frame ack testpoint pin as an output
	TRISBbits.TRISB5 = 0;	// Set pic_ready_bar line to an output
	TRISCbits.TRISC1 = 0;	// Set LED as an output

	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;	

	// Oscope debug pins, set them to outputs
	TRISDbits.TRISD4 = 0;
	TRISGbits.TRISG15 = 0;
	PIN_OSCOPE1 = 0;
	PIN_OSCOPE2 = 0;

	// Short delay to wait for power supply voltages to settle
 	__delay32( (long)(0.01 * CLOCKFREQ) );

	// init the PSync-specific hardware (SPI2 port and Flip Flop)
	init_psync();
	
    // Setup the UART - 56 kbs
	unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
	unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
	OpenUART2( U2MODEvalue, U2STAvalue, BRG_SETTING );
	ConfigIntUART2( UART_RX_INT_DIS & UART_TX_INT_DIS );	// Disable TX and RX interrups

	// Setup timer 2 and set its period to 1 kHz
	// timer freq = 10MHz / period
	OpenTimer2( T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_PS_1_1 & T2_SOURCE_INT, 10000);
	EnableIntT2;

	PIN_LED = 0;		// Default to LED off for power saving
}


int main( void )
{
	init_pic(); 	// Setup the PIC internal peripherals

	if( PIN_JP1_1 == 1 )  {
		SpitFrameCounts();		// Go into spit mode forever
	}

	while(1) 
	{	
		char s[32];

		cmd_gets(s, sizeof(s));		// Get a command from the user
		process_cmd(s);				// Deal with the command
	}	
}

void OnIdle( void )
{
	extern unsigned long frame_count;	// Set by the interrupt handler
	static int prev = 1;
	int now = PIN_CAMERA_SHUTTER;

	static unsigned long start_frame_count;

	// On falling edge
	if( prev && !now )  {
		start_frame_count = frame_count;
		ClearTimer();
	}

	// On the rising edge, save the current frame count and clear our 1 kHz timer
	if( !prev && now )  {
		char buf[20];
		unsigned lengthMS = ReadTimer();
		sprintf(buf, " %u\r\n", lengthMS );

		TTYPutc('*');
		TTYPrintHex32( start_frame_count );
		TTYPuts(buf);

		// Blink the LED for 100 ms, for visual feedback, and for switch debounce
		PIN_LED = 1;
	 	__delay32( (long)(0.100 * CLOCKFREQ) );
		PIN_LED = 0;
	}

	prev = now;
}