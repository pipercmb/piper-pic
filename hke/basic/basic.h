//---------> Some useful stuff

#define BAUD_RATE		115200
#define CLOCKFREQ 20000000L		// 20.000 MHz clock (synthesised from 10 MHz crystal)
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)) - 1.00 + 0.5 ))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Adds 0.5 to convert from float to integer correctly

//-----> OUTPUT PINS
#define PIN_LED				LATCbits.LATC1	// Front pannel LED

#define PIN_CONVERT			LATDbits.LATD1	// ADC convert pin
#define PIN_485_ENABLE		LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

#define PIN_OSCOPE1			LATDbits.LATD4	// Debug pins for looking at with scope
#define PIN_OSCOPE2			LATGbits.LATG15

//------> INPUT PINS
#define PIN_ADDR0			PORTFbits.RF1
#define PIN_ADDR1			PORTGbits.RG1
#define PIN_ADDR2			PORTGbits.RG0
#define PIN_ADDR3			PORTFbits.RF0

#define PIN_FRAME_CLK		PORTDbits.RD3

#define PIN_JP5_3			PORTGbits.RG13
#define PIN_PUSHBUTTON  	PORTGbits.RG13

// Macros to turn interrupts on/off
#define _INTERRUPTS_OFF		asm volatile ("disi #0x3FFF")
#define _INTERRUPTS_ON		DISICNT = 0


// Allowable command return status values
#define	STATUS_OK		    0		// Everything is OK, not a failure
#define STATUS_FAIL_CMD	    1		// Unknown command
#define STATUS_FAIL_NARGS	2		// incorrect number of arguments
#define STATUS_FAIL_INVALID 3		// Invalid type of argument (was expecting a number, got "bob")
#define STATUS_FAIL_RANGE   4		// Argument is out of range


//---------> Function Prototypes
// basic.C
void init_pic( void );		// Init the PIC ports and hardware
void init_board( void );	// Init chips on the board
void isr_init( void );		// Init the interrupt handler

// process_cmd.c
void process_cmd( char *cmd );

// cmd_gets.c
void cmd_gets( char *str, int n );

// cmd_parse.c
void cmd_parse( char *str );
unsigned short parse_channel_list( int num_ch_args );
char tok_available( void );
char *tok_get_str( void );
unsigned char tok_get_uint8( void );
short tok_get_int16( void );
long tok_get_int32( void );
float tok_get_float( void );
char tok_valid_num( unsigned char digits, unsigned char neg, unsigned char dp );
char tok_valid_uint( char digits );
int parse_address( char h );
int tok_count_args( char *str );
void tok_decrement_num_args( void );
int tok_get_num_args( void );

// common_function.c
void print_address(void);
void print_status( char status );

