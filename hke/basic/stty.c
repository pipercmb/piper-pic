#include <p30F5011.h>
#include <uart.h>
#include <stdio.h>

#include "stty.h"

/*
 * Send a string over the UART
 */
void TTYPuts( char *s )
{
	while( *s )  {
		while( BusyUART2() );	// Wait till not busy
		putcUART2( *s );
		++s;
	}
	
}

/*
 * Send a character over the UART
 */
void TTYPutc( char c )
{
	while( BusyUART2() );	// Wait till not busy
	putcUART2( c );
}
