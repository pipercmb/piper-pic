#include <p30F5011.h>
#include <stdio.h>
#include <stdlib.h>

#include "basic.h"

unsigned int count = 0;
unsigned int prev_state = 0;

/*
 * Interrupt handler
 */
void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt( void ) 
{
	PIN_OSCOPE2 = 1;
 	IFS0bits.OC2IF = 0;		// reset Output Compare 2 interrupt flag -- needed or int handler will not work right!!!
	
	if (PIN_PUSHBUTTON == 1 && prev_state == 0)
	{
		++count;
		prev_state = 1;
	}
	else if (PIN_PUSHBUTTON == 0 && prev_state == 1)
	{
		prev_state = 0;
	}

	PIN_OSCOPE2 = 0;
}