#include <p30F5011.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "basic.h"
#include "stty.h"

// Local function prototypes
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_p( void );		// prints status
static char do_cmd_addr( void );	// prints out card address
static char do_cmd_reset( void ); 	// resets count to zero
static char do_cmd_set ( void );	// sets the count

/*
 * Process a user command contained in the command buffer.
 *
 * Must be preceeded by a call to cmd_parse()
 */
void process_cmd( char *cmd )
{
	char *s;
	unsigned char r;
	
	// Parse the command string
	cmd_parse(cmd);

	// Parse the command string
	s = tok_get_str();	// first token is the command name
	
	if( !strcmp( s, "led" ))		
		r = do_cmd_led();		// set the LED
	else if( !strcmp( s, "addr" ))	
		r = do_cmd_addr();		// read in hex address
	else if( !strcmp( s, "p" ))
		r = do_cmd_p();			// prints adc voltages
	else if( !strcmp( s, "reset"))
		r = do_cmd_reset();		// resets the count to zero
	else if( !strcmp( s, "set"))
		r = do_cmd_set();		// sets the count
	else 							
		r = STATUS_FAIL_CMD;	// User typed unknown command
	
	print_status( r );
}

/** LED
 *	Set LED light.
 *
 *	usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 */
static char do_cmd_led( void )
{
	char *arg;
	
	// Check that the next token is a valid number
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	
	arg = tok_get_str();
	if( !strcmp( arg, "on" ) )
		PIN_LED = 1;
	else if( !strcmp( arg, "off" ) )
		PIN_LED = 0;
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}


/** ADDR
 *	Prints hex address of the board.
 *
 *	usage:	
 *	No arguments
 */
static char do_cmd_addr( void )
{
/*
	unsigned char addr = get_addr();
	char str[16];
	sprintf( str, "%x\r\n", addr );
	TTYPuts( str );
*/
	return STATUS_OK;
}


/*
 * P
 */
static char do_cmd_p( void )
{
	int i;	
	char buffer[80];
	extern unsigned int count;
	unsigned int prev_count = 0;

	sprintf(buffer, "%u\r\n", count);
	TTYPuts(buffer);
	prev_count=count;

	return STATUS_OK;
}


/*
 * RESET
 */
static char do_cmd_reset( void )
{
	extern unsigned int count;

	count = 0;
	
	return STATUS_OK;
}


/*
 * SET
 */
static char do_cmd_set ( void )
{
	extern unsigned int count;

	
	return STATUS_OK;
}
