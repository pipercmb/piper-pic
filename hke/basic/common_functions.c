#include <p30F5011.h>
#include "stty.h"
#include "basic.h"

/*
 * Prints our card hex address like so
 * #3:\r\n
 */
void print_address(void)
{
/*
	extern unsigned char CardAddress;
	extern char *hex;

	// All responses must begin with #
	TTYPutc( '#' );
	TTYPutc( hex[ CardAddress & 0x0F ] );	// Print only last nibble of address
	TTYPuts( ":\r\n" );
*/
}


/* 
 * Print a human readable status message depending on the status code.
 */
void print_status( char status )
{
	switch( status )  {
		case STATUS_OK:
			TTYPuts( "OK" );
			break;
		case STATUS_FAIL_CMD:
			TTYPuts( "FAIL: unknown command" );
			break;
		case STATUS_FAIL_NARGS:
			TTYPuts( "FAIL: incorrect number of arguments" );
			break;
		case STATUS_FAIL_INVALID:
			TTYPuts( "FAIL: invalid argument format" );
			break;
		case STATUS_FAIL_RANGE:
			TTYPuts( "FAIL: argument out of range" );
			break;
		default:
			TTYPuts( "FAIL: unknown error" );
			break;
	}

	// All responses must end with ! to signal parser this is end of response
	TTYPuts( "!\r\n" );
}
