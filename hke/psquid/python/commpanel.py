"""
The communications control panel for PSquid.
"""
import wx
import wx.lib.filebrowsebutton as wxfbb
from misc import StaticWrapText


class CommPanel(wx.Panel):
    """
    The communications control panel for PSquid.
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)

        self.fmf = self.GetTopLevelParent()

        self.helptext = ("Select port that the PSquid is connected to, "
                         "then click 'Connect'.")

        self.create_sizers()
        self.populate_top()

    def create_sizers(self):
        """
        Create the sizers in the panel.
        """
        self.bsMain = wx.BoxSizer(wx.VERTICAL)

        self.bsTop = wx.BoxSizer(wx.VERTICAL)

        self.bsMain.Add(self.bsTop, 1, wx.EXPAND)

        self.SetSizer(self.bsMain)

    def populate_top(self, sizer=None):
        """
        Populate the top sizer.
        """
        if sizer is None:
            # sizer = self.sbsTop
            sizer = self.bsTop

        self.txtHelp = StaticWrapText(self, wx.ID_ANY, label=self.helptext)

        self.bsLoad = wx.BoxSizer(wx.VERTICAL)

        self.fbbFileBrowser = wxfbb.FileBrowseButton(self, -1,
                                                     labelText='Port')
        self.fbbFileBrowser.SetValue('/Users/jlazear/pty1') #'/dev/') #FIXME

        self.bLoad = wx.Button(self, -1, label='Connect')
        self.sbConnected = wx.StaticBox(self, label='Connected to')
        self.sbsConnected = wx.StaticBoxSizer(self.sbConnected, wx.VERTICAL)

        self.connected = self.make_connected_to()

        self.bsLoad.Add(self.txtHelp, 0, wx.EXPAND)
        self.bsLoad.Add((1, 10), 0, wx.EXPAND)
        self.bsLoad.Add(self.fbbFileBrowser, 0, wx.EXPAND | wx.BOTTOM)
        self.bsLoad.Add(self.bLoad, 0, wx.TOP | wx.ALIGN_CENTER)

        self.bsLoad.Add((10, 10), 0, wx.EXPAND)
        self.bsLoad.Add(wx.StaticLine(self, size=(-1, 2)), 0, wx.EXPAND)
        self.bsLoad.Add((10, 10), 0, wx.EXPAND)

        self.bsLoad.Add(self.sbsConnected, 0, wx.EXPAND)

        sizer.Add(self.bsLoad, 1, wx.EXPAND)

    def make_connected_to(self, sizer=None):
        if sizer is None:
            sizer = self.sbsConnected

        controls = {}

        bs = wx.GridBagSizer(3, 1)

        lblPortLabel = wx.StaticText(self, label='Port: ',
                                     style=wx.ALIGN_RIGHT)
        lblVerLabel = wx.StaticText(self, label='Version: ',
                                    style=wx.ALIGN_RIGHT)

        lblfont = wx.Font(16, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        lblPortLabel.SetFont(lblfont)
        lblVerLabel.SetFont(lblfont)

        lblPort = wx.StaticText(self, label='Not connected!')
        lblVer = wx.StaticText(self, label='-----')

        controls['port'] = lblPort
        controls['ver'] = lblVer
        controls['portlabel'] = lblPortLabel
        controls['verlabel'] = lblVerLabel

        bs.Add(lblPortLabel, (0, 0), flag=wx.EXPAND | wx.RIGHT)
        bs.Add(lblPort, (0, 1), flag=wx.EXPAND | wx.LEFT)
        bs.Add(lblVerLabel, (1, 0), flag=wx.EXPAND | wx.RIGHT)
        bs.Add(lblVer, (1, 1), flag=wx.EXPAND | wx.LEFT)
        bs.Add((1, 1), (2, 0), (1, 2), flag=wx.EXPAND)

        bs.AddGrowableCol(1, 1)
        bs.AddGrowableRow(2, 1)

        sizer.Add(bs, 1, wx.EXPAND)

        return controls



