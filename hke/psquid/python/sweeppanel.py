"""
The sweep control panel for PSquid.
"""
import wx
# from wx.lib.scrolledpanel import ScrolledPanel
from misc import DACsChoice, make_gbsizer_from_array
import numpy as np


class SweepPanel(wx.Panel):
# class SweepPanel(ScrolledPanel): #DELME?
    """
    The sweep control panel for PSquid.
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)
        # ScrolledPanel.__init__(self, parent=parent, id=wx.ID_ANY) #DELME?

        self.fmf = self.GetTopLevelParent()
        self.timer = wx.Timer(self)
        self.timer.Start(500)  # Send timer event every 50 ms

        self.helptext = ("Sweep a DAC.")
        # self.helptext = ("Specify channel you want to monitor, the number of "
        #                  "samples to average (must be >100), and the location"
        #                  " to save the data to.\n\nChecking 'Temp File' will "
        #                  "use a throw-away temporary file, so a filename is "
        #                  "not necessary in that case.\n\nThen click 'Collect "
        #                  "Data' to begin collecting and plotting data.\n\n"
        #                  "The 'Window Size' parameter specifies how many "
        #                  "post-averaging samples should be plotted. The "
        #                  "latest 'Window Size' samples will be plotted.\n\n"
        #                  "You may toggle whether or not the ADC or DAC is "
        #                  "shown with their check boxes.")

        self.create_sizers()
        self.populate_top()

        # self.SetupScrolling(scroll_x=False, scroll_y=True) #DELME?

    def create_sizers(self):
        """
        Create the sizers in the panel.
        """
        self.bsMain = wx.BoxSizer(wx.VERTICAL)

        self.bsTop = wx.BoxSizer(wx.VERTICAL)

        self.bsMain.Add(self.bsTop, 1, wx.EXPAND)

        self.SetSizer(self.bsMain)

    def populate_top(self, sizer=None):
        """
        Populate the top sizer.
        """
        if sizer is None:
            sizer = self.bsTop

        # Create controls
        # self.txtHelp = StaticWrapText(self, wx.ID_ANY, label=self.helptext)

        self.lblSwept = wx.StaticText(self, label='Sweep')
        self.chSwept = DACsChoice(self, wx.ID_ANY)

        txtfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        txtsize = wx.Size(70, 17)
        self.lblStart = wx.StaticText(self, label='Start Value')
        self.txtStart = wx.TextCtrl(self, size=txtsize)
        self.txtStart.SetFont(txtfont)
        self.sldStart = wx.Slider(self, minValue=0, maxValue=65535)

        self.lblEnd = wx.StaticText(self, label='End Value')
        self.txtEnd = wx.TextCtrl(self, size=txtsize)
        self.txtEnd.SetFont(txtfont)
        self.sldEnd = wx.Slider(self, minValue=0, maxValue=65535)

        self.lblStep = wx.StaticText(self, label='Step Size')
        self.txtStep = wx.TextCtrl(self, size=txtsize)
        self.txtStep.SetFont(txtfont)
        self.sldStep = wx.Slider(self, minValue=0, maxValue=65535)

        self.lblNsweeps = wx.StaticText(self, label='N Sweeps')
        self.spinNsweeps = wx.SpinCtrl(self, min=0, max=65535, initial=1)

        self.lblNavg = wx.StaticText(self, label='N Average')
        self.spinNavg = wx.SpinCtrl(self, min=4, max=65535, initial=100)

        self.lblNsettle = wx.StaticText(self, label='N Settle')
        self.spinNsettle = wx.SpinCtrl(self, min=1, max=65535, initial=100)

        self.lblFile = wx.StaticText(self, label='File')
        self.txtFile = wx.TextCtrl(self, value='sweep.txt')
        self.chkFile = wx.CheckBox(self, label='Temp File')
        self.bStart = wx.Button(self, label='Start Sweep')

        self.lblMonitor = wx.StaticText(self, label='Monitor')
        self.chMonitor = DACsChoice(self, auto=True)
        self.lblWSize = wx.StaticText(self, label='Window Size')
        self.spinWSize = wx.SpinCtrl(self, max=int(1.e9))

        self.lblShowx = wx.StaticText(self, label='X axis')
        self.chkIndexx = wx.CheckBox(self, wx.ID_ANY, "Index")
        self.chkSweptx = wx.CheckBox(self, wx.ID_ANY, "Swept")
        self.chkDACx = wx.CheckBox(self, wx.ID_ANY, "Monitor")
        self.chkADCx = wx.CheckBox(self, wx.ID_ANY, "ADC")
        bsChkx = wx.BoxSizer(wx.VERTICAL)
        bsChkx.AddMany([self.chkIndexx,
                        self.chkSweptx,
                        self.chkDACx,
                        self.chkADCx])

        self.lblShowy = wx.StaticText(self, label='Y axis')
        self.chkIndexy = wx.CheckBox(self, wx.ID_ANY, "Index")
        self.chkSwepty = wx.CheckBox(self, wx.ID_ANY, "Swept")
        self.chkDACy = wx.CheckBox(self, wx.ID_ANY, "Monitor")
        self.chkADCy = wx.CheckBox(self, wx.ID_ANY, "ADC")
        bsChky = wx.BoxSizer(wx.VERTICAL)
        bsChky.AddMany([self.chkIndexy,
                        self.chkSwepty,
                        self.chkDACy,
                        self.chkADCy])
        bsChk = wx.BoxSizer(wx.HORIZONTAL)
        bsChk.AddMany([self.lblShowx, bsChkx, ((20, -1), 1),
                       self.lblShowy, bsChky])

        self.lblAutoscale = wx.StaticText(self, label='Autoscale')
        self.chkAutoscalex = wx.CheckBox(self, label='X axis')
        self.chkAutoscaley = wx.CheckBox(self, label='Y axis')
        bsChkAuto = wx.BoxSizer(wx.VERTICAL)
        bsChkAuto.AddMany([self.chkAutoscalex, self.chkAutoscaley])

        # Add controls to sizers
        arr = np.array([[(-1, 1), None, None],
                        [self.lblSwept, self.chSwept, self.chSwept],
                        [self.lblStart, self.sldStart, self.txtStart],
                        [self.lblEnd, self.sldEnd, self.txtEnd],
                        [self.lblStep, self.sldStep, self.txtStep],
                        [self.lblNsweeps, self.spinNsweeps, self.spinNsweeps],
                        [(-1, 1), None, None],
                        [self.lblNavg, self.spinNavg, self.spinNavg],
                        [self.lblNsettle, self.spinNsettle, self.spinNsettle],
                        [(-1, 1), None, None],
                        [self.lblFile, self.txtFile, self.txtFile],
                        [None, self.chkFile, self.chkFile],
                        [(-1, 1), None, None],
                        [self.bStart, self.bStart, self.bStart],
                        [(-1, 1), None, None],
                        ['hline', 'hline', 'hline'],
                        [self.lblMonitor, self.chMonitor, self.chMonitor],
                        [self.lblWSize, self.spinWSize, self.spinWSize],
                        [bsChk, bsChk, bsChk],
                        [(-1, 1), None, None],
                        [self.lblAutoscale, bsChkAuto, bsChkAuto],
                        [(-1, 1), None, None]],
                        dtype='object')

        lflag = wx.EXPAND | wx.RIGHT
        rflag = wx.EXPAND | wx.LEFT
        cflag = wx.ALIGN_CENTER

        styles = np.array([[None, None, None],
                           [rflag, lflag, lflag],
                           [rflag, None, lflag],
                           [rflag, None, lflag],
                           [rflag, None, lflag],
                           [rflag, lflag, lflag],
                           [None, None, None],
                           [rflag, None, lflag],
                           [rflag, None, lflag],
                           [None, None, None],
                           [rflag, lflag, lflag],
                           [None, lflag, lflag],
                           [None, None, None],
                           [cflag, cflag, cflag],
                           [None, None, None],
                           [lflag, lflag, lflag],
                           [rflag, lflag, lflag],
                           [rflag, lflag, lflag],
                           [cflag, cflag, cflag],
                           [None, None, None],
                           [rflag, lflag, lflag],
                           [None, None, None]],
                           dtype='object')

        self.gbs = make_gbsizer_from_array(self, arr, styles, vertmargin=0,
                                           vminsize=5)

        # self.gbs.Add((30, -1), (0, 3)) #DELME?
        # self.gbs.AddGrowableCol(3, 1) #DELME?
        self.gbs.AddGrowableCol(0, 1)
        self.gbs.AddGrowableCol(4, 1)
        self.gbs.AddGrowableRow(14, 1)

        sizer.Add(self.gbs, 1, wx.EXPAND)

