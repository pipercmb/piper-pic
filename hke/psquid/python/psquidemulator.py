#!/bin/env python

"""
psquidemulator.py
jlazear
2013-06-21

One-line description

Long description

Example:

<example code here>
"""
version = 20130621
releasestatus = 'beta'

from numpy import *
import cereal
import serial
from psquid import PSquidError, PSquidArgumentError
from psquid import PSquidTimeoutError, PSquidInvalidStateError
import time
import random


class PSquidEmulator(cereal.Cereal):
    """
    Emulates the PSquid hardware board.
    """
    def __init__(self, port, baudrate=115200, timeout=1, **kwargs):
        super(PSquidEmulator, self).__init__(port=port,
                                             baudrate=baudrate,
                                             timeout=timeout,
                                             **kwargs)
        self.state = 'do nothing'

        # Dictionary of actions to take according to the state
        self._statedict = {'do nothing': self._do_nothing,
                           'sweep': self._sweep,
                           'data': self._data}

        # Dictionary of functions to call to change the state
        self._setstatedict = {'do nothing': self._set_do_nothing,
                              'sweep': self._set_sweep,
                              'data': self._set_data}

        # Some internal constants
        self._adcphase = random.random()*2*pi
        self._dacphase = random.random()*2*pi
        self._sweepphase = 0

    def run(self):
        """
        Essentially a copy of Cereal.run(), except with
        self.handledata() added into the loop.
        """
        while not self.stopped.isSet():
            try:
                if self.openflag:
#                    self.ser.open()
                    try:
                        iw = self.ser.inWaiting()
                        iw = max(1, int(iw))
                        toadd = self.ser.read(self.ser.inWaiting())
                    except (ValueError, TypeError):
                        toadd = ''
                    except (OSError, serial.SerialException):
                        toadd = ''
                    with self.bufferlock:
                        self.buffer += toadd
                time.sleep(0.1)
                self.handledata()   # Deal with data by state
            except KeyboardInterrupt:
                self.stopped.set()

    #-----------------------------------------------------------#
    #-------------------- Utility Functions --------------------#
    #-----------------------------------------------------------#
    def set_state(self, newstate, *args, **kwargs):
        """
        Changes the state.
        """
        try:
            self._setstatedict[newstate](*args, **kwargs)
        except KeyError:
            raise PSquidInvalidStateError(newstate)

    def _set_do_nothing(self, *args, **kwargs):
        """
        Change to the 'do nothing' state.
        """
        self.flushInput()
        self.state = 'do nothing'

    def _set_sweep(self, *args, **kwargs):
        """
        Change to the 'sweep' state. Must pass in a file handle or
        filename.
        """
        self.flushInput()
        self.state = 'sweep'
        self.t0 = time.time()
        nsweeps = float(args[-1])
        ntimer = float(args[-2])/100.
        navg = float(args[-3])/100.
        self.duration = nsweeps*(ntimer + navg)
        print "duration = ", self.duration #DELME
        if nsweeps != 0:
            self.tf = self.t0 + self.duration
        else:
            self.tf = None
        print "t0, tf = ", self.t0, self.tf #DELME

    def _set_data(self, *args, **kwargs):
        """
        Change to the 'data' state. Must pass in a file handle or
        filename.
        """
        self.flushInput()
        self.state = 'data'

    def handledata(self):
        """
        Handle the data according to the state
        """
        self._statedict[self.state]()  # Call appropriate function
        time.sleep(.1)

    def _do_nothing(self):
        """
        The 'do nothing' state. Used when the PSquid board is not
        reporting anything back except command responses.
        """
        if self.buffer.endswith('\r'):
            cmdstr = self.readline(eol='\r')
            cmdsplit = cmdstr.split(' ')
            cmd = cmdsplit.pop(0)
            print "cmdsplit = ", cmdsplit #DELME
            self.write('*PSQUID: {cmd}\r\nOK!\r\n'.format(cmd=cmd))
            if cmd == 'sweep':
                self.set_state('sweep', *cmdsplit)
            elif cmd == 'data':
                self.set_state('data')

    def _sweep(self):
        """
        The 'sweep' state. Used when the PSquid board is performing a
        sweep and reporting back data triplets.
        """
        # if self.buffer.endswith('sweep_off\r'):
        t = time.time()
        tf = 2*t if (self.tf is None) else self.tf
        if ('sweep_off' in self.buffer) or (t > tf):
            self.set_state('do nothing')
            self.write('*PSQUID: sweep_off\r\nOK!\r\n')
            return

        dac = self._to_hex(self._sweep_generator().next())
        rdac = self._to_hex(1000*self._dac_sine_generator().next())
        adc = self._to_hex(1000*self._adc_sine_generator().next())

        towrite = '{0} {1} {2}\r\n'.format(dac[-4:], rdac, adc)
        self.write(towrite)

        # rand1 = hex(randint(0, 2**32))[2:].upper()
        # rand2 = hex(randint(0, 2**32))[2:].upper()

    def _data(self):
        """
        The 'data' state. Used when the PSquid board is in the data
        averaging state and is reporting back data pairs.
        """
        # if self.buffer.endswith('data_off\r'):
        if 'data_off' in self.buffer:
            self.set_state('do nothing')
            self.write('*PSQUID: sweep_off\r\nOK!\r\n')
            return

        dac = self._to_hex(1000*self._dac_sine_generator().next())
        adc = self._to_hex(1000*self._adc_sine_generator().next())

        towrite = '{0} {1}\r\n'.format(dac, adc)
        self.write(towrite)

    def _adc_sine_generator(self, A=65535, f=1.):
        while True:
            self._adcphase += .01
            val = A*(sin(f*self._adcphase) + 1)/2.
            yield val

    def _dac_sine_generator(self, A=65535, f=1.):
        while True:
            self._dacphase += .01
            yield A*(sin(f*self._dacphase) + 1)/2

    def _sweep_generator(self, A=65535, f=1.):
        while True:
            self._sweepphase += .01
            yield mod(A*self._sweepphase*f, A)

    @staticmethod
    def _to_hex(value):
        hexstr = "{0:08X}".format(int(value))
        return hexstr
