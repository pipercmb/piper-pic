#!/bin/env python

"""
psquidgui.py
jlazear
2013-07-25

Top level GUI objects for the PSquid GUI.

Usage:

<from command line>
$ python psquidgui.py
"""
version = 20130725
releasestatus = 'beta'

import matplotlib
matplotlib.interactive(False)
matplotlib.use('WXAgg')
import wx
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.figure import Figure

from psquid import PSquid
from pyoscope import PyOscope

from commpanel import CommPanel
from dacpanel import DACPanel
from datapanel import DataPanel
from sweeppanel import SweepPanel
from glockpanel import GraphicalLockPanel
from mlockpanel import ManualLockPanel
from bindings import Binder


class NotebookFrame(wx.Notebook):
    """
    The notebook frame.
    """
    def __init__(self, parent, id=wx.ID_ANY, style=wx.BK_DEFAULT):
        wx.Notebook.__init__(self, parent, id=id, style=style)

        self.pages = []

        self.panelComm = CommPanel(self)
        self.pages.append(self.panelComm)

        self.panelData = DataPanel(self)
        self.pages.append(self.panelData)

        self.panelSweep = SweepPanel(self)
        self.pages.append(self.panelSweep)

        self.panelGLock = GraphicalLockPanel(self)
        self.pages.append(self.panelGLock)

        self.panelMLock = ManualLockPanel(self)
        self.pages.append(self.panelMLock)

        self.AddPage(self.panelComm, "Comm")
        self.AddPage(self.panelData, "Mon")
        self.AddPage(self.panelSweep, "Sweep")
        self.AddPage(self.panelGLock, "Lock (G)")
        self.AddPage(self.panelMLock, "Lock (M)")


class MainFrame(wx.Frame):
    """
    The main frame that holds all of the others. Really just an
    expandable container for the notebook.
    """
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "PSquid",
                          size=(1100, 850))

        self.make_menubar()

        self.panel = wx.Panel(self)

        # Splitter pane
        self.splMain = wx.SplitterWindow(self.panel, wx.ID_ANY,
                                         style=wx.SP_LIVE_UPDATE)
        self.splMain.SetMinimumPaneSize(350)

        # Plot figure canvas
        self.wPlot = wx.Window(self.splMain, style=wx.BORDER_SUNKEN)
        bsPlot, cPlot, tbPlot = self.make_canvas(self.wPlot)
        self.bsPlot = bsPlot
        self.cPlot = cPlot
        self.tbPlot = tbPlot

        self.wPlot.SetSizer(self.bsPlot)

        self.timer = wx.Timer(self)  # Timer for plot updating
        self.timer.Start(200)  # 200 ms = 5 Hz = 5 fps

        # Control panel notebook and DACs controller
        self.wRight = wx.Window(self.splMain)
        self.bsRight = wx.BoxSizer(wx.VERTICAL)
        self.notebook = NotebookFrame(self.wRight)
        self.pDacs = DACPanel(self.wRight)

        self.bsRight.Add(self.notebook, 1, wx.EXPAND | wx.BOTTOM)
        self.bsRight.Add(self.pDacs, 0, wx.EXPAND | wx.TOP)
        self.wRight.SetSizer(self.bsRight)

        # Add subwindows to splitter
        self.splMain.SplitVertically(self.wPlot, self.wRight, 800)

        self.bsMain = wx.BoxSizer(wx.VERTICAL)
        self.bsMain.Add(self.splMain, 1, wx.ALL | wx.EXPAND)

        self.panel.SetSizer(self.bsMain)

        self.Layout()

    def make_menubar(self):
        self.menuBar = wx.MenuBar()
        self.menus = []

        # File
        self.mFile = wx.Menu()
        self.miQuit = self.mFile.Append(wx.ID_EXIT, '&Quit', 'Quit')
        self.menus.append(self.mFile)

        # Plot
        self.mPlot = wx.Menu()
        self.miRealtime = self.mPlot.Append(wx.ID_ANY, '&Realtime',
                                            'Realtime', wx.ITEM_CHECK)
        self.menus.append(self.mPlot)

        # DACs
        self.mDACs = wx.Menu()
        self.miVolts = self.mDACs.Append(wx.ID_ANY, '&Volts', 'Volts',
                                         wx.ITEM_RADIO)
        self.miRaw = self.mDACs.Append(wx.ID_ANY, '&Raw', 'DAC Counts',
                                       wx.ITEM_RADIO)
        self.menus.append(self.mDACs)

        self.menuBar.Append(self.mFile, "&File")
        self.menuBar.Append(self.mPlot, "&Plot")
        self.menuBar.Append(self.mDACs, "&DACs")

        self.SetMenuBar(self.menuBar)

    def make_canvas(self, parent, fig=None, tb=True):
        if fig is None:
            fig, _, _ = self.init_plot()
        else:
            fig = fig

        canvas = FigCanvas(parent, -1, fig)

        bsPlot = wx.BoxSizer(wx.VERTICAL)
        bsPlot.Add(canvas, 1, flag=wx.GROW)

        if tb:
            toolbar = NavigationToolbar(canvas)
            toolbar.Realize()
            bsPlot.Add(toolbar, 0, wx.LEFT | wx.EXPAND)
        else:
            toolbar = None

        return bsPlot, canvas, toolbar

    def init_plot(self, plotsize=(6., 4.), data=None):
        fig = Figure(plotsize, dpi=100)

        axes = fig.add_subplot(111)

        if data is not None:
            xs, ys = data
            lines = [axes.plot(xs, ys)]
        else:
            lines = [[]]

        return fig, axes, lines

    def replace_figure(self, newfigure):
        """
        Replaces the canvas and figure with the specified figure and its
        associated toolbar. The old figure, canvas, and sizer are destroyed.
        """
        oldcanvas = self.cPlot
        oldtoolbar = self.tbPlot
        # oldsizer = self.bsPlot  # Automatically destroyed when unattached

        bsPlot, cPlot, tbPlot = self.make_canvas(self.wPlot,
                                                 fig=newfigure)
        self.bsPlot = bsPlot
        self.cPlot = cPlot
        self.tbPlot = tbPlot

        self.wPlot.SetSizer(self.bsPlot)
        self.bsPlot.Layout()

        oldcanvas.Destroy()
        oldtoolbar.Destroy()


from wx.lib.mixins.inspection import InspectionMixin  #DELME

class PSquidApp(wx.App, InspectionMixin):  #DELME Remove InspectionMixin
    def OnInit(self):
        self.Init()  #DELME For InspectionMixin
        # Make the MainFrame and GraphFrame
        fMainFrame = MainFrame()
        self.fMainFrame = fMainFrame

        self.fMainFrame.Show()
        self.fMainFrame.panel.SendSizeEvent()
        for page in self.fMainFrame.notebook.pages:
            page.SendSizeEvent()
        self.SetTopWindow(self.fMainFrame)

        return 1

if __name__ == '__main__':

    # Create GUI frame
    app = PSquidApp(0)

    # Create model objects
    device = None  # '/dev/tty.usbserial-A10180PC'
    baudrate = 417000
    p = PSquid(device, throttle=False, baudrate=baudrate)
    rt = PyOscope(interactive=False)

    # Create binder and bind GUI to model
    binder = Binder(p, app, rt)
    binder.bind()

    # Start event handling loop (will block)
    app.MainLoop()


