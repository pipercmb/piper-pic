"""
The data control panel for PSquid.
"""
import wx
# from misc import StaticWrapText, DACsChoice, make_gbsizer_from_array
from misc import DACsChoice, make_gbsizer_from_array
import numpy as np


class DataPanel(wx.Panel):
    """
    The data panel for PSquid.
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)

        self.fmf = self.GetTopLevelParent()
        self.timer = wx.Timer(self)
        self.timer.Start(500)  # Send timer event every 50 ms

        self.helptext = ("Specify channel you want to monitor, the number of "
                         "samples to average (must be >100), and the location"
                         " to save the data to.")
        # self.helptext = ("Specify channel you want to monitor, the number of "
        #                  "samples to average (must be >100), and the location"
        #                  " to save the data to.\n\nChecking 'Temp File' will "
        #                  "use a throw-away temporary file, so a filename is "
        #                  "not necessary in that case.\n\nThen click 'Collect "
        #                  "Data' to begin collecting and plotting data.\n\n"
        #                  "The 'Window Size' parameter specifies how many "
        #                  "post-averaging samples should be plotted. The "
        #                  "latest 'Window Size' samples will be plotted.\n\n"
        #                  "You may toggle whether or not the ADC or DAC is "
        #                  "shown with their check boxes.")

        self.create_sizers()
        self.populate_top()

    def create_sizers(self):
        """
        Create the sizers in the panel.
        """
        self.bsMain = wx.BoxSizer(wx.VERTICAL)

        self.bsTop = wx.BoxSizer(wx.VERTICAL)

        self.bsMain.Add(self.bsTop, 1, wx.EXPAND)

        self.SetSizer(self.bsMain)

    def populate_top(self, sizer=None):
        """
        Populate the top sizer.
        """
        if sizer is None:
            sizer = self.bsTop

        # gbs = wx.GridBagSizer(3, 3)
        # self.gbsData = gbs

        # sizer.Add(gbs, 1, wx.EXPAND)

        # Create controls
        # self.txtHelp = StaticWrapText(self, wx.ID_ANY, label=self.helptext)

        self.lblDACs = wx.StaticText(self, label='Show DAC ')
        self.chDACs = DACsChoice(self, wx.ID_ANY)
        self.lblNavg = wx.StaticText(self, label='N Average')
        self.spinNavg = wx.SpinCtrl(self, min=2, max=65535, initial=100)
        self.lblFile = wx.StaticText(self, label='File')
        self.txtFile = wx.TextCtrl(self, value='data.txt')
        self.chkFile = wx.CheckBox(self, label='Temp File')
        self.bStart = wx.Button(self, label='Collect Data', style=wx.CENTER)

        self.lblWSize = wx.StaticText(self, label='Window Size')
        self.spinWSize = wx.SpinCtrl(self, max=int(1.e9))
        self.lblShow = wx.StaticText(self, label='Show')
        self.chkDAC = wx.CheckBox(self, wx.ID_ANY, "DAC")
        self.chkADC = wx.CheckBox(self, wx.ID_ANY, "ADC")
        bsChk = wx.BoxSizer(wx.VERTICAL)
        bsChk.AddMany([self.chkDAC, self.chkADC])
        self.lblAutoscale = wx.StaticText(self, label='Autoscale')
        self.chkAutoscalex = wx.CheckBox(self, label='X axis')
        self.chkAutoscaley = wx.CheckBox(self, label='Y axis')
        bsChkAuto = wx.BoxSizer(wx.VERTICAL)
        bsChkAuto.AddMany([self.chkAutoscalex, self.chkAutoscaley])

        # Add controls to sizers
        arr = np.array([[(-1, 20), None],
                       [self.lblDACs, self.chDACs],
                       [self.lblNavg, self.spinNavg],
                       [self.lblFile, self.txtFile],
                       [None, self.chkFile],
                       [(-1, 5), None],
                       [self.bStart, self.bStart],
                       [(-1, 5), None],
                       ['hline', None],
                       [self.lblWSize, self.spinWSize],
                       [self.lblShow, bsChk],
                       [(-1, 5), None],
                       [self.lblAutoscale, bsChkAuto],
                       [(-1, 5), None]],
                       dtype='object')

        lflag = wx.EXPAND | wx.RIGHT
        rflag = wx.EXPAND | wx.LEFT
        cflag = wx.ALIGN_CENTER

        styles = np.array([[None, None],
                           [rflag, lflag],
                           [rflag, lflag],
                           [rflag, lflag],
                           [None, lflag],
                           [None, None],
                           [cflag, cflag],
                           [None, None],
                           [None, None],
                           [rflag, lflag],
                           [rflag, lflag],
                           [None, None],
                           [rflag, lflag],
                           [None, None]],
                          dtype='object')

        self.gbs = make_gbsizer_from_array(self, arr, styles, vertmargin=2)

        self.gbs.AddGrowableCol(0, 1)
        self.gbs.AddGrowableCol(3, 1)
        self.gbs.AddGrowableRow(7, 1)

        sizer.Add(self.gbs, 1, wx.EXPAND)


        # bs.Add((10, 1), (2, 0), flag=wx.EXPAND)
        # bs.Add((10, 1), (2, 3), flag=wx.EXPAND)
        # bs.Add(self.txtHelp, (0, 0), (1, 4), flag=wx.EXPAND | wx.CENTER)
        # bs.Add((-1, 10), (1, 0), (1, 4))
        # bs.Add(self.lblDACs, (2, 1), flag=wx.EXPAND | wx.RIGHT)
        # bs.Add(self.chDACs, (2, 2), (1, 1), flag=wx.EXPAND | wx.LEFT)
        # bs.Add(self.lblNavg, (3, 1), flag=wx.EXPAND | wx.RIGHT)
        # bs.Add(self.spinNavg, (3, 2), flag=wx.EXPAND | wx.LEFT)
        # bs.Add(self.lblFile, (4, 1), flag=wx.EXPAND | wx.RIGHT)
        # bs.Add(self.txtFile, (4, 2), flag=wx.EXPAND | wx.LEFT)
        # bs.Add(self.chkFile, (5, 2), flag=wx.EXPAND | wx.LEFT)
        # bs.Add(self.bStart, (7, 2), (1, 2), flag=wx.CENTER)
        # bs.Add((-1, 10), (8, 0), (1, 4), flag=wx.EXPAND)

        # bs.Add(wx.StaticLine(self, size=(-1, 2)), (9, 0), (1, 4),
        #        flag=wx.EXPAND)

        # bs.Add(self.lblWSize, (10, 1), flag=wx.EXPAND | wx.RIGHT)
        # bs.Add(self.spinWSize, (10, 2), flag=wx.EXPAND | wx.LEFT)
        # bs.Add(self.lblShow, (11, 1), flag=wx.EXPAND | wx.RIGHT)
        # bs.Add(bsChk, (11, 2), flag=wx.EXPAND | wx.LEFT)


        # bs.Add((-1, 1), (12, 0), (1, 4), flag=wx.EXPAND)

        # bs.AddGrowableCol(0, 1)
        # bs.AddGrowableCol(3, 1)
        # bs.AddGrowableRow(8, 1)

