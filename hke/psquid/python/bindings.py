"""
The data control panel for PSquid.
"""
import wx
import time
import numpy as np

from psquid import PSquidTimeoutError
from readers import HexReader
from psquidreaders import PSquidDataReader, PSquidRawDataReader, \
                          PSquidSweepReader
from misc import DACValidator, IntegerValidator, LockController


class Binder(object):
    """
    Responsible for binding a PSquid object to its GUI.
    """
    def __init__(self, psquid, gui, pyo):
        self.psquid = psquid
        self.pyo = pyo
        self.gui = gui

        self.raw = False
        self.collector = None
        self.locked = False
        self.lockindex = None

        self.piddict = {'p': 10, 'pshift': 4,
                        'i': 100, 'ishift': 8}

        self.make_binders()

    def make_binders(self):
        """
        Make the binding classes.
        """
        fmf = self.gui.fMainFrame
        notebook = fmf.notebook

        self.mainbindings = MainBindings(self, self.gui)

        dacpanel = fmf.pDacs
        self.dacpanelbindings = DACPanelBindings(self, dacpanel)

        commpanel = notebook.panelComm
        self.commpanelbindings = CommPanelBindings(self, commpanel)

        datapanel = notebook.panelData
        self.datapanelbindings = DataPanelBindings(self, datapanel)

        sweeppanel = notebook.panelSweep
        self.sweeppanelbindings = SweepPanelBindings(self, sweeppanel)

        glockpanel = notebook.panelGLock
        self.glockpanelbindings = GLockPanelBindings(self, glockpanel)

        mlockpanel = notebook.panelMLock
        self.mlockpanelbindings = MLockPanelBindings(self, mlockpanel)

    def bind(self):
        """
        Bind everything.
        """
        self.bind_main()
        self.bind_pyo()
        self.bind_dacpanel()
        self.bind_commpanel()
        self.bind_datapanel()
        self.bind_sweeppanel()
        self.bind_glockpanel()
        self.bind_mlockpanel()

    def bind_main(self):
        """
        Bind the main panel (probably empty).
        """
        self.mainbindings.bind()

    def bind_pyo(self):
        """
        Connect the PyOscope figure to the Canvas.
        """
        pass #DELME

    def bind_dacpanel(self):
        """
        Bind the DAC panel.
        """
        self.dacpanelbindings.bind()

    def bind_commpanel(self):
        """
        Bind the Comm panel.
        """
        self.commpanelbindings.bind()

    def bind_datapanel(self):
        """
        Bind the Data panel.
        """
        self.datapanelbindings.bind()

    def bind_sweeppanel(self):
        """
        Bind the Sweep panel.
        """
        self.sweeppanelbindings.bind()

    def bind_glockpanel(self):
        """
        Bind the Graphical Lock panel.
        """
        self.glockpanelbindings.bind()

    def bind_mlockpanel(self):
        """
        Bind the Manual Lock panel.
        """
        self.mlockpanelbindings.bind()

    def set_units(self, units='volts'):
        """
        Change the displayed units for the DAC text controls.
        """
        units = units.lower()
        unitsdict = {'volts': self._set_volts,
                     'counts': self._set_counts}
        unitsdict[units]()

    def _set_volts(self):
        """
        Set DAC units to volts.
        """
        self.raw = False

        # Rescale text controls
        dactxts = self.dacpanelbindings.txtctrls
        sweeptxts = self.sweeppanelbindings.txtctrls
        glocktxts = self.glockpanelbindings.rangetxtctrls
        mlocktxts = self.mlockpanelbindings.txtctrls
        txtctrls = dactxts + sweeptxts + glocktxts + mlocktxts

        for txt in txtctrls:
            validator = txt.GetValidator()
            oldindex = validator._index(txt.GetValue())
            validator.raw = False
            # validator.scaledrange = (0., 1.)
            # validator.num = 65536
            newval = validator._value(oldindex)
            txt.SetValue(str(newval))

        # GLockPanel's slider is a bit different since it only has stepped
        # values of the DAC, rather than the full range available to it.
        glocktxt = self.glockpanelbindings.txtStepValue
        glockvalidator = glocktxt.GetValidator()
        oldindex = glockvalidator._index(glocktxt.GetValue())
        glockvalidator.raw = False
        newval = glockvalidator._value(oldindex)
        glocktxt.SetValue(str(newval))

        # Rescale plot
        def callback(s):
            s.data = s.data/65535.
        self.pyo.set_callback(callback)

        try:
            for ax in self.pyo.axes.flatten():
                minxval, maxxval = ax.get_xlim()
                minyval, maxyval = ax.get_ylim()

                minxval, maxxval = minxval/65535., maxxval/65535.
                minyval, maxyval = minyval/65535., maxyval/65535.

                ax.set_xlim(minxval, maxxval)
                ax.set_ylim(minyval, maxyval)
        except AttributeError:
            pass

    def _set_counts(self):
        """
        Set DAC units to counts.
        """
        self.raw = True

        # Rescale text controls
        dactxts = self.dacpanelbindings.txtctrls
        sweeptxts = self.sweeppanelbindings.txtctrls
        glocktxts = self.glockpanelbindings.rangetxtctrls
        mlocktxts = self.mlockpanelbindings.txtctrls
        txtctrls = dactxts + sweeptxts + glocktxts + mlocktxts

        for txt in txtctrls:
            validator = txt.GetValidator()
            oldindex = validator._index(txt.GetValue())
            validator.raw = True
            # validator.rawrange = (0, 65535)
            # validator.num = 65536
            newval = validator._value(oldindex)
            txt.SetValue(str(int(newval)))

        # GLockPanel's slider is a bit different since it only has stepped
        # values of the DAC, rather than the full range available to it.
        glocktxt = self.glockpanelbindings.txtStepValue
        glockvalidator = glocktxt.GetValidator()
        oldindex = glockvalidator._index(glocktxt.GetValue())
        glockvalidator.raw = True
        newval = glockvalidator._value(oldindex)
        glocktxt.SetValue(str(int(newval)))

        # Rescale plot
        def callback(s):
            pass
        self.pyo.set_callback(callback)

        try:
            for ax in self.pyo.axes.flatten():
                minxval, maxxval = ax.get_xlim()
                minyval, maxyval = ax.get_ylim()

                minxval, maxxval = minxval*65535., maxxval*65535.
                minyval, maxyval = minyval*65535., maxyval*65535.

                ax.set_xlim(minxval, maxxval)
                ax.set_ylim(minyval, maxyval)
        except AttributeError:
            pass



class MainBindings(object):
    """
    Callback functions for the Main panel bindings.
    """
    def __init__(self, binder, gui):
        self.binder = binder
        self.gui = gui
        self.fmf = self.gui.fMainFrame
        self.psquid = self.binder.psquid
        self.pyo = self.binder.pyo

        self.timer = self.fmf.timer

    def bind(self):
        self.fmf.bindings = self

        # Initialize
        self.fmf.replace_figure(self.pyo.fig)  # Swap out placeholder figure
        self.pyo.canvas = self.pyo.fig.canvas  # Canvas created in prev line
        self.canvas = self.pyo.canvas
        self.pyo.fig.canvas.SendSizeEventToParent()  # Force resize/redraw

        # File
        self.fmf.Bind(wx.EVT_CLOSE, self.on_close, self.fmf)
        self.fmf.Bind(wx.EVT_MENU, self.on_close, self.fmf.miQuit)

        # Plot
        self.fmf.Bind(wx.EVT_MENU, self.on_realtime, self.fmf.miRealtime)

        # DACs
        self.fmf.miVolts.Check()
        self.fmf.Bind(wx.EVT_MENU, self.on_raw, self.fmf.miRaw)
        self.fmf.Bind(wx.EVT_MENU, self.on_volts, self.fmf.miVolts)

        # Bind timer
        self.fmf.Bind(wx.EVT_TIMER, self.on_timer, self.timer)

    def on_close(self, event):
        # Stop timers
        timers = [self.timer,
                  self.binder.dacpanelbindings.timer,
                  self.binder.datapanelbindings.timer,
                  self.binder.sweeppanelbindings.timer,
                  self.binder.glockpanelbindings.timer,
                  self.binder.mlockpanelbindings.timer]
        for timer in timers:
            timer.Stop()

        # Let the event queue flush out
        wx.Yield()

        # Clean up the interfaces and objects
        self.pyo.stop()
        # self.pyo.close()
        self.psquid.close()
        self.fmf.Destroy()

    def on_realtime(self, event):
        self.psquid.connection.realtime = self.fmf.miRealtime.IsChecked()

    def on_raw(self, event):
        # dpb = self.binder.dacpanelbindings
        # dpb.set_units('counts')
        self.binder.set_units('counts')

    def on_volts(self, event):
        # dpb = self.binder.dacpanelbindings
        # dpb.set_units('volts')
        self.binder.set_units('volts')

    def on_timer(self, event):
        """
        Plot update loop. The way PyOscope is written, this should simply call
        the pyoscope update() method and then redraw the canvas.
        """
        try:
            self.pyo._update()
            self.canvas.draw()
        except Exception as e:
            self.timer.Stop()
            raise e


class DACPanelBindings(object):
    """
    Callback functions for the DAC Panel bindings.

    Note that the order of DACs on the GUI is different from order of DACs in
    the PIC code. The GUI index is used in all instances here, and the
    translation to PIC index is performed by the timer event callback.

    GUI Index  --  GUI DAC  --  PIC Index
    0              Det Bias     3
    1              S1 Bias      0
    2              S1 FB        4
    3              S2 Bias      1
    4              S2 FB        5
    5              S3 Bias      2
    6              S3 FB        6
    7              Offset       7
    8              Gain         8
    """
    def __init__(self, binder, dacpanel):
        self.binder = binder
        self.psquid = self.binder.psquid
        self.dacpanel = dacpanel
        self.labels = self.dacpanel.lblDacs
        self.sliders = self.dacpanel.sldDacs
        self.txtctrls = self.dacpanel.txtDacs
        self.picindices = [3, 0, 4, 1, 5, 2, 6, 7, 8]

        controls = self.sliders + self.txtctrls
        ids = range(len(self.sliders)) + range(len(self.txtctrls))
        self.iddict = dict(zip(controls, ids))

        self.timer = dacpanel.timer

        self.dirtydacs = [False for i in self.sliders]
        self.dirtyvalues = [0 for i in self.sliders]

        self.dirtyvalues[8] = 65535
        self.dirtydacs[8] = True

        sld = self.sliders[8]
        sld.SetValue(65535)

    def bind(self):
        self.dacpanel.bindings = self

        # Bind sliders
        for slider in self.sliders:
            slider.Bind(wx.EVT_SCROLL, self.on_slider)

        # Bind text controls
        for txt in self.txtctrls:
            validator = DACValidator(self.binder.raw)
            txt.SetValidator(validator)
            txt.Bind(wx.EVT_CHAR, self.on_char)

        # Initialize controls
        for i in range(len(self.sliders)):
            self.update_dac_txt(i)

        # Bind timer
        self.dacpanel.Bind(wx.EVT_TIMER, self.on_timer, self.timer)

    def schedule_dac(self, index, val=None):
        if val is None:
            sld = self.sliders[index]
            val = sld.GetValue()
        self.dirtydacs[index] = True
        self.dirtyvalues[index] = val

    def dirty_all(self):
        """
        Sets all of the DAC dirty flags to True, so all DACs get updated.

        Does not change the DAC values.
        """
        for i in range(len(self.dirtydacs)):
            self.dirtydacs[i] = True
            time.sleep(.1)
#        self.dirtydacs = [True for val in self.dirtydacs]

    def on_slider(self, event):
        obj = event.GetEventObject()
        index = self.iddict[obj]

        # Send new value to text control
        self.update_dac_txt(index)

        # Schedule DAC for updating
        self.schedule_dac(index)

    def on_char(self, event):
        key = event.GetKeyCode()
        obj = event.GetEventObject()
        # try:
        #     curval = float(obj.GetValue())
        # except ValueError:
        #     curval = 0.
        index = self.iddict[obj]

        if key == 13:  # User pressed <Enter>
            self.update_dac_slider(index)
            self.schedule_dac(index)
        elif key == 315:  # <Up arrow>
            self.update_dac_slider(index)
            self.schedule_dac(index)
        elif key == 317:  # <Down arrow>
            self.update_dac_slider(index)
            self.schedule_dac(index)
        else:
            event.Skip()

    def on_timer(self, event):
        """
        Every time the DAC timer sends an event, update the first dirty DAC.
        """
        if not self.psquid.connected:
            ctrls = self.labels + self.sliders + self.txtctrls
            for ctrl in ctrls:
                ctrl.Disable()
            return
        else:
            ctrls = self.labels + self.sliders + self.txtctrls
            for i, lbl in enumerate(self.labels):
                sld = self.sliders[i]
                txt = self.txtctrls[i]
            # for i, ctrl in enumerate(ctrls):
                if i != self.binder.lockindex:
                    lbl.Enable()
                    sld.Enable()
                    txt.Enable()
                else:
                    lbl.Disable()
                    sld.Disable()
                    txt.Disable()

        try:
            index = self.dirtydacs.index(True)
        except ValueError:
            return

        picindex = self.picindices[index]
        value = self.dirtyvalues[index]
        self.psquid.dac(picindex, value, raw=True)
        self.dirtydacs[index] = False

    def update_dac_txt(self, index):
        """
        Update the specified DAC text control to display the new value.
        """
        sld = self.sliders[index]
        txt = self.txtctrls[index]
        validator = txt.GetValidator()

        val = sld.GetValue()
        val = val*(1 if self.binder.raw else 1./65535)
        val = validator._validate(val)

        txt.SetValue(str(val))

    def update_dac_slider(self, index):
        """
        Update the specified DAC slider to display the new value.
        """
        sld = self.sliders[index]
        txt = self.txtctrls[index]
        validator = txt.GetValidator()

        val = float(txt.GetValue())
        # val = int(val*(1 if self.binder.raw else 65535))
        val = validator._index(val)

        sld.SetValue(val)

    def update_dac_both(self, index, value):
        """
        Updated both the text control and slider of specified DAC to display
        the specified value.
        """
        sld = self.sliders[index]
        txt = self.txtctrls[index]
        sldval = sld.GetValidator()
        txtval = txt.GetValidator()
        print "sldval = ", repr(sldval) #DELME
        print "txtval = ", repr(txtval) #DELME

        txtvalue = value*(1 if self.binder.raw else 1./65535)
        txtvalue = txtval._validate(txtvalue)

        sldvalue = float(value)
        sldvalue = sldval._index(sldvalue)

        txt.SetValue(str(txtvalue))
        sld.SetValue(sldvalue)


class CommPanelBindings(object):
    """
    Callback functions for the Comm Panel bindings.
    """
    def __init__(self, binder, commpanel):
        self.binder = binder
        self.psquid = self.binder.psquid
        self.commpanel = commpanel

        self.dev = commpanel.fbbFileBrowser
        self.bLoad = commpanel.bLoad

    def bind(self):
        self.commpanel.bindings = self

        self.bLoad.Bind(wx.EVT_BUTTON, self.on_load)
        self.dev.Bind(wx.EVT_CHAR_HOOK, self.on_load)

    def on_load(self, event):
        # Check if button pressed or <Enter> pressed
        try:
            code = event.GetKeyCode()
            if code != 13:  # Only fire event if pressed <Enter>
                event.Skip()
                return
        except AttributeError:
            pass

        devname = self.dev.GetValue()

        try:
            wx.BeginBusyCursor()
            self.psquid.set_port(devname)
        except ValueError as e:
            wx.EndBusyCursor()
            msg = str(e)
            dlg = wx.MessageDialog(self.commpanel, msg, 'Error',
                                   wx.OK | wx.ICON_INFORMATION)
            dlg.ShowModal()
            dlg.Destroy()
            return

        port = self.commpanel.connected['port']
        ver = self.commpanel.connected['ver']

        try:
            wx.BeginBusyCursor()
            verstr = self.psquid.ver(readresponse=True)
        except PSquidTimeoutError:
            verstr = 'Unknown!'
        finally:
            wx.EndBusyCursor()

        port.SetLabelText(devname)
        ver.SetLabelText(verstr)

        self.bLoad.SetLabelText('Re-Connect')
        self.bLoad.SendSizeEventToParent()


class DataPanelBindings(object):
    """
    Callback functions for the Data Panel bindings.

    Note that the order of DACs on the GUI is different from order of DACs in
    the PIC code. The GUI index is used in all instances here, and the
    translation to PIC index is performed by the timer event callback.

    GUI Index  --  GUI DAC  --  PIC Index
    0              Det Bias     3
    1              S1 Bias      0
    2              S1 FB        4
    3              S2 Bias      1
    4              S2 FB        5
    5              S3 Bias      2
    6              S3 FB        6
    7              Offset       7
    8              Gain         8
    """
    def __init__(self, binder, datapanel):
        self.binder = binder
        self.psquid = self.binder.psquid
        self.pyo = self.binder.pyo
        self.datapanel = datapanel
        self.picindices = [3, 0, 4, 1, 5, 2, 6, 7, 8]

        # Controls
        self.lblDACs = self.datapanel.lblDACs
        self.lblNavg = self.datapanel.lblNavg
        self.lblFile = self.datapanel.lblFile
        self.lblWSize = self.datapanel.lblWSize
        self.lblShow = self.datapanel.lblShow
        self.lblAutoscale = self.datapanel.lblAutoscale

        self.timer = self.datapanel.timer
        self.chDACs = self.datapanel.chDACs
        self.spinNavg = self.datapanel.spinNavg
        self.txtFile = self.datapanel.txtFile
        self.chkFile = self.datapanel.chkFile
        self.bStart = self.datapanel.bStart

        self.spinWSize = self.datapanel.spinWSize
        self.chkDAC = self.datapanel.chkDAC
        self.chkADC = self.datapanel.chkADC
        self.chkAutoscalex = self.datapanel.chkAutoscalex
        self.chkAutoscaley = self.datapanel.chkAutoscaley

        self.controls = [self.lblDACs, self.lblNavg, self.lblFile,
                         self.lblWSize, self.lblShow, self.chDACs,
                         self.spinNavg, self.txtFile, self.chkFile,
                         self.bStart, self.spinWSize, self.chkDAC,
                         self.chkADC, self.lblAutoscale, self.chkAutoscalex,
                         self.chkAutoscaley]

        # Sub controls
        self.txtNavg = self.spinNavg.Children[0]
        self.txtWSize = self.spinWSize.Children[0]

        # State variables
        self.collecting = False

    def bind(self):
        self.datapanel.bindings = self
        # Canvas not created until Bindings.bind()
        self.canvas = self.pyo.canvas

        # Initialize controls
        self.initialize()

        # Bind controls
        self.chDACs.Bind(wx.EVT_CHOICE, self.on_DACs)
        self.bStart.Bind(wx.EVT_BUTTON, self.on_start)
        self.chkFile.Bind(wx.EVT_CHECKBOX, self.on_temp_file)

        self.spinWSize.Bind(wx.EVT_SPINCTRL, self.on_wsize_spin)
        self.txtWSize.Bind(wx.EVT_CHAR, self.on_wsize_text)
        # self.datapanel.Bind(wx.EVT_SPINCTRL, self.on_wsize, self.spinWSize)
        # self.datapanel.Bind(wx.EVT_TEXT, self.on_wsize, self.spinWSize)
        self.chkDAC.Bind(wx.EVT_CHECKBOX, self.on_dac_adc)
        self.chkADC.Bind(wx.EVT_CHECKBOX, self.on_dac_adc)
        self.chkAutoscalex.Bind(wx.EVT_CHECKBOX, self.on_autoscale)
        self.chkAutoscaley.Bind(wx.EVT_CHECKBOX, self.on_autoscale)

        # Bind timer
        self.datapanel.Bind(wx.EVT_TIMER, self.on_timer, self.timer)

    def initialize(self):
        self.datapanel.Disable()

        self.spinNavg.SetValue(100)
        self.txtNavg.SetValidator(IntegerValidator(minval=2))

        self.txtFile.SetValue('data.txt')
        self.txtFile.Disable()
        self.chkFile.SetValue(True)

        self.spinWSize.SetValue(0)
        self.txtWSize.SetValidator(IntegerValidator(minval=0))

        self.chkDAC.SetValue(True)
        self.chkADC.SetValue(True)
        self.chkAutoscalex.SetValue(True)
        self.chkAutoscaley.SetValue(True)

    def _data(self):
        """
        Trigger a PSQUID data command.
        """
        dacindex = self.chDACs.GetSelection()
        picindex = self.picindices[dacindex]
        raw = self.binder.raw
        navg = self.spinNavg.GetValue()
        fname = self.txtFile.GetValue()
        fmf = self.binder.gui.fMainFrame

        # Cannot do realtime plotting for low navg
        if navg < 100:
            fmf.miRealtime.Check(False)  #DELME #HERE

        # Reset state before starting new task
        state = self.psquid.connection.state
        if state == 'data':
            self.psquid.data_off()
            time.sleep(0.2)
        elif state == 'sweep':
            self.psquid.sweep_off()
            time.sleep(0.2)
        elif state == 'raw_data':
            self.psquid.raw_data_off()
            time.sleep(0.2)

        self.f = self.psquid.data(picindex, navg, f=fname)
        time.sleep(1)  # Don't want to read an empty file

        self.pyo.switch_file(self.f, reader=PSquidDataReader)

        if not raw:
            def callback(s):
                s.data = s.data/65535.
        else:
            def callback(s):
                pass
        self.pyo.set_callback(callback)

    def _plot(self):
        """
        Make a plot of the currently active data.
        """
        if self.collecting:
            wsize = int(self.spinWSize.GetValue())
            showdac = self.chkDAC.GetValue()
            showadc = self.chkADC.GetValue()

            ys = []
            if showdac:
                ys.append('locked')
            if showadc:
                ys.append('adc')

            self.pyo.plot(ys, legend=False, windowsize=wsize) #DELME
#            self.pyo.plot(ys, legend=True, windowsize=wsize) #FIXME
            self.canvas.draw()

    def on_DACs(self, event):
        if self.collecting:
            # Update control
            dacindex = event.GetSelection()
            self.chDACs.SetSelection(dacindex)

            # Re-send data command
            try:
                self._data()
            except Exception as e:
                msg = str(e)
                dlg = wx.MessageDialog(self.datapanel, msg, 'Error',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                # dlg.Destroy()
            return
        else:
            event.Skip()  # Default handler works when not plotting

    def on_start(self, event):
        if self.collecting:
            self.binder.collector = None
            self.psquid.data_off()
            self.bStart.SetLabelText('Collect Data')
            self.binder.dacpanelbindings.dirty_all()
        else:
            self.binder.collector = 'data'
            self.txtFile.Disable()
            self.chkFile.Disable()
            self.spinNavg.Disable()

            try:
                self._data()
            except Exception as e:
                msg = str(e)
                dlg = wx.MessageDialog(self.datapanel, msg, 'Error',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                # dlg.Destroy()
                return

            self.bStart.SetLabelText('Stop Collecting')

        self.collecting = not self.collecting
        self._plot()
        self.bStart.SendSizeEventToParent()

    def on_char(self, event):
        key = event.GetKeyCode()
        obj = event.GetEventObject()

        if key == 13:  # User pressed <Enter>
            pass
        else:
            event.Skip()

    def on_temp_file(self, event):
        if not self.collecting:
            state = event.IsChecked()
            self.txtFile.Enable(not state)
        event.Skip()

    def on_wsize_spin(self, event):
        wsize = int(self.spinWSize.GetValue())
        self.txtWSize.SetSelection(-1, -1)
        self.pyo.windowsize(wsize)
        event.Skip()

    def on_wsize_text(self, event):
        key = event.GetKeyCode()
        wsize = int(self.spinWSize.GetValue())

        if key == 13:
            self.pyo.windowsize(wsize)
        elif key == 315:  # <Up arrow>
            newwsize = wsize + 1
            if newwsize == 1:
                newwsize = 2
            self.spinWSize.SetValue(newwsize)
            self.txtWSize.SetSelection(-1, -1)
            self.pyo.windowsize(newwsize)
        elif key == 317:  # <Down arrow>
            newwsize = wsize - 1
            if newwsize == 1:
                newwsize = 0
            self.spinWSize.SetValue(newwsize)
            # self.txtWSize.SetValue(str(newwsize)) #DELME?
            self.txtWSize.SetSelection(-1, -1)
            self.pyo.windowsize(newwsize)
        elif key < wx.WXK_SPACE or key == wx.WXK_DELETE or key > 255:
            event.Skip()
        elif chr(key) in '0123456789':
            event.Skip()

    def on_dac_adc(self, event):
        self._plot()
        event.Skip()

    def on_autoscale(self, event):
        # Update control
        val = event.IsChecked()
        obj = event.GetEventObject()
        obj.SetValue(val)

        # Set scaling
        xauto = self.chkAutoscalex.GetValue()
        yauto = self.chkAutoscaley.GetValue()

        self.pyo.autoscale(xauto, yauto)

    def on_timer(self, event):
        """
        Use a timer to check the state of the connection and toggle controls.

        Much of this is redundant, but it ensures that the controls are in the
        correct state even if something goes wrong.
        """
        if not self.psquid.connected:
            for ctrl in self.controls:
                ctrl.Disable()
            return
        else:
            for ctrl in self.controls:
                ctrl.Enable()

        # if self.psquid.connection.state != 'data':
        #     self.collecting = False
        #     self.binder.collector = None

        if self.collecting and (self.binder.collector == 'data'):
            self.txtFile.Disable()
            self.chkFile.Disable()
            self.spinNavg.Disable()
            self.bStart.SetLabelText('Stop Collecting')
        else:
            self.txtFile.Enable()
            self.chkFile.Enable()
            self.spinNavg.Enable()
            self.collecting = False
            self.bStart.SetLabelText('Collect Data')
        self.bStart.SendSizeEventToParent()

        if self.chkFile.GetValue():
            self.txtFile.Disable()


class SweepPanelBindings(object):
    """
    Callback functions for the Sweep Panel bindings.

    Note that the order of DACs on the GUI is different from order of DACs in
    the PIC code. The GUI index is used in all instances here, and the
    translation to PIC index is performed by the timer event callback.

    GUI Index  --  GUI DAC  --  PIC Index
    0              Det Bias     3
    1              S1 Bias      0
    2              S1 FB        4
    3              S2 Bias      1
    4              S2 FB        5
    5              S3 Bias      2
    6              S3 FB        6
    7              Offset       7
    8              Gain         8
    """
    def __init__(self, binder, sweeppanel):
        self.binder = binder
        self.psquid = self.binder.psquid
        self.pyo = self.binder.pyo
        self.sweeppanel = sweeppanel
        self.picindices = [3, 0, 4, 1, 5, 2, 6, 7, 8]

        # Controls
        self.lblSwept = self.sweeppanel.lblSwept
        self.lblStart = self.sweeppanel.lblStart
        self.lblEnd = self.sweeppanel.lblEnd
        self.lblStep = self.sweeppanel.lblStep
        self.lblNsweeps = self.sweeppanel.lblNsweeps
        self.lblNavg = self.sweeppanel.lblNavg
        self.lblNsettle = self.sweeppanel.lblNsettle
        self.lblFile = self.sweeppanel.lblFile
        self.lblMonitor = self.sweeppanel.lblMonitor
        self.lblWSize = self.sweeppanel.lblWSize
        self.lblShowx = self.sweeppanel.lblShowx
        self.lblShowy = self.sweeppanel.lblShowy
        self.lblAutoscale = self.sweeppanel.lblAutoscale

        self.timer = self.sweeppanel.timer
        self.chSwept = self.sweeppanel.chSwept
        self.sldStart = self.sweeppanel.sldStart
        self.txtStart = self.sweeppanel.txtStart
        self.sldEnd = self.sweeppanel.sldEnd
        self.txtEnd = self.sweeppanel.txtEnd
        self.sldStep = self.sweeppanel.sldStep
        self.txtStep = self.sweeppanel.txtStep
        self.spinNsweeps = self.sweeppanel.spinNsweeps
        self.spinNavg = self.sweeppanel.spinNavg
        self.spinNsettle = self.sweeppanel.spinNsettle
        self.txtFile = self.sweeppanel.txtFile
        self.chkFile = self.sweeppanel.chkFile
        self.bStart = self.sweeppanel.bStart

        self.chMonitor = self.sweeppanel.chMonitor
        self.spinWSize = self.sweeppanel.spinWSize
        self.chkIndexx = self.sweeppanel.chkIndexx
        self.chkSweptx = self.sweeppanel.chkSweptx
        self.chkDACx = self.sweeppanel.chkDACx
        self.chkADCx = self.sweeppanel.chkADCx
        self.chkIndexy = self.sweeppanel.chkIndexy
        self.chkSwepty = self.sweeppanel.chkSwepty
        self.chkDACy = self.sweeppanel.chkDACy
        self.chkADCy = self.sweeppanel.chkADCy

        self.chkAutoscalex = self.sweeppanel.chkAutoscalex
        self.chkAutoscaley = self.sweeppanel.chkAutoscaley

        self.controls = [self.lblSwept, self.lblStart, self.lblEnd,
                         self.lblStep, self.lblNsweeps, self.lblNavg,
                         self.lblNsettle, self.lblFile, self.lblMonitor,
                         self.lblWSize, self.lblShowx, self.lblShowy,
                         self.lblAutoscale, self.chSwept,
                         self.sldStart, self.txtStart, self.sldEnd,
                         self.txtEnd, self.sldStep, self.txtStep,
                         self.spinNsweeps, self.spinNavg, self.spinNsettle,
                         self.txtFile, self.chkFile, self.bStart,
                         self.chMonitor, self.spinWSize, self.chkIndexx,
                         self.chkSweptx, self.chkDACx, self.chkADCx,
                         self.chkIndexy, self.chkSwepty , self.chkDACy,
                         self.chkADCy, self.chkAutoscalex, self.chkAutoscaley]

        self.txtctrls = [self.txtStart, self.txtEnd, self.txtStep]
        self.sldctrls = [self.sldStart, self.sldEnd, self.sldStep]
        self.spinctrls = [self.spinNsweeps, self.spinNavg, self.spinNsettle,
                          self.spinWSize]
        self.axischks = [self.chkIndexx, self.chkSweptx, self.chkDACx,
                         self.chkADCx, self.chkIndexy, self.chkSwepty,
                         self.chkDACy, self.chkADCy]

        self.txtNsweeps = self.spinNsweeps.Children[0]
        self.txtNavg = self.spinNavg.Children[0]
        self.txtNsettle = self.spinNsettle.Children[0]
        self.txtWSize = self.spinWSize.Children[0]
        self.spintxtctrls = [self.txtNsweeps, self.txtNavg, self.txtNsettle,
                            self.txtWSize]

        # State variables
        self.collecting = False

    def bind(self):
        self.sweeppanel.bindings = self
        # Canvas not created until Bindings.bind()
        self.canvas = self.pyo.canvas

        # Initialize controls
        self.initialize()

        # Bind controls
        self.chSwept.Bind(wx.EVT_CHOICE, self.on_choice_DAC)
        self.chMonitor.Bind(wx.EVT_CHOICE, self.on_choice_DAC)
        for txt in self.txtctrls:
            txt.Bind(wx.EVT_CHAR, self.on_txt_char)
        for sld in self.sldctrls:
            sld.Bind(wx.EVT_SCROLL, self.on_slider)
        for spin in self.spinctrls:
            spin.Bind(wx.EVT_SPINCTRL, self.on_spin)
        for txt in self.spintxtctrls:
            txt.Bind(wx.EVT_CHAR, self.on_text)

        for chk in self.axischks:
            chk.Bind(wx.EVT_CHECKBOX, self.on_axis_checkbox)

        self.bStart.Bind(wx.EVT_BUTTON, self.on_start)
        self.chkFile.Bind(wx.EVT_CHECKBOX, self.on_temp_file)

        self.chkAutoscalex.Bind(wx.EVT_CHECKBOX, self.on_autoscale)
        self.chkAutoscaley.Bind(wx.EVT_CHECKBOX, self.on_autoscale)

        # Bind timer
        self.sweeppanel.Bind(wx.EVT_TIMER, self.on_timer, self.timer)

    def initialize(self):
        for txt in self.txtctrls:
            validator = DACValidator(self.binder.raw)
            txt.SetValidator(validator)

        self.sldStart.SetValue(0)
        self.update_dac_txt('start')

        self.sldEnd.SetValue(65535)
        self.update_dac_txt('end')

        self.sldStep.SetValue(6554)
        self.update_dac_txt('step')

        self.spinNsweeps.SetValue(1)
        self.txtNsweeps.SetValidator(IntegerValidator(minval=0))
        self.spinNavg.SetValue(100)
        self.txtNavg.SetValidator(IntegerValidator(minval=4))
        self.spinNsettle.SetValue(100)
        self.txtNsettle.SetValidator(IntegerValidator(minval=1))

        self.txtFile.SetValue('sweep.txt')
        self.txtFile.Disable()
        self.chkFile.SetValue(True)

        self.spinWSize.SetValue(0)
        self.txtWSize.SetValidator(IntegerValidator(minval=0))

        self.chkIndexx.SetValue(False)
        self.chkSweptx.SetValue(True)
        self.chkDACx.SetValue(False)
        self.chkADCx.SetValue(False)

        self.chkIndexy.SetValue(False)
        self.chkSwepty.SetValue(False)
        self.chkDACy.SetValue(True)
        self.chkADCy.SetValue(True)

        self.chkAutoscalex.SetValue(True)
        self.chkAutoscaley.SetValue(True)

    def _sweep(self):
        """
        Trigger a PSQUID sweep command.
        """
        sweepindex = self.chSwept.GetSelection()
        monindex = self.chMonitor.GetSelection()
        picsweepindex = self.picindices[sweepindex]
        if monindex != 0:
            monsweepindex = self.picindices[monindex-1]
        else:
            monsweepindex = None
        raw = self.binder.raw

        start = self.sldStart.GetValue()
        end = self.sldEnd.GetValue()
        step = self.sldStep.GetValue()
        if start >= end:
            low = end
            high = start
            step = -step
        else:
            low = start
            high = end

        nsweeps = self.spinNsweeps.GetValue()
        navg = self.spinNavg.GetValue()
        nsettle = self.spinNsettle.GetValue()
        fname = self.txtFile.GetValue()
        fmf = self.binder.gui.fMainFrame

        # Cannot do realtime plotting for low navg
        if navg < 100:
            fmf.miRealtime.Check(False)

        # Reset state before starting new task
        state = self.psquid.connection.state
        if state == 'data':
            self.psquid.data_off()
            time.sleep(0.2)
        if state == 'raw_data':
            self.psquid.raw_data_off()
            time.sleep(0.2)
        elif state == 'sweep':
            self.psquid.sweep_off()
            time.sleep(0.2)

        self.f = self.psquid.sweep(ch=picsweepindex,
                                   read_ch=monsweepindex,
                                   low=low,
                                   high=high,
                                   delta=step,
                                   navg=navg,
                                   ntimer=nsettle,
                                   nsweeps=nsweeps,
                                   raw=True,
                                   f=fname)
        time.sleep(1)  # Don't want to read an empty file

        self.pyo.switch_file(self.f, reader=PSquidSweepReader)

        if not raw:
            def callback(s):
                s.data = s.data/65535.
        else:
            def callback(s):
                pass
        self.pyo.set_callback(callback)

    def _plot(self):
        """
        Make a plot of the currently active data.
        """
        if self.collecting:
            wsize = int(self.spinWSize.GetValue())

            xs = self.get_data_list('x')
            ys = self.get_data_list('y')

#            self.pyo.plot(xs, ys, legend=True, windowsize=wsize) #FIXME
            self.pyo.plot(xs, ys, legend=False, windowsize=wsize) #DELME
            self.canvas.draw()

    def get_data_list(self, axis):
        """Get which data series should be plotted on the specified axis"""
        ctrlsdict = {'x': [self.chkIndexx, self.chkSweptx, self.chkDACx,
                           self.chkADCx],
                     'y': [self.chkIndexy, self.chkSwepty, self.chkDACy,
                           self.chkADCy]}
        ctrls = ctrlsdict[axis]
        namelist = [None, 'swept', 'locked', 'adc']
        mask = [ctrl.GetValue() for ctrl in ctrls]
        plotlist = []
        for i, m in enumerate(mask):
            if m:
                plotlist.append(namelist[i])

        return plotlist

    def on_slider(self, event):
        obj = event.GetEventObject()

        # Send new value to text control
        self.update_dac_txt(obj)

    def on_txt_char(self, event):
        key = event.GetKeyCode()
        obj = event.GetEventObject()

        if key == 13:  # User pressed <Enter>
            self.update_dac_slider(obj)
        elif key == 315:  # <Up arrow>
            self.update_dac_slider(obj)
        elif key == 317:  # <Down arrow>
            self.update_dac_slider(obj)
        else:
            event.Skip()

    def on_choice_DAC(self, event):
        if self.collecting:
            # Update control
            dacindex = event.GetSelection()
            self.chSwept.SetSelection(dacindex)

            # Re-send data command
            try:
                self._sweep()
            except Exception as e:
                msg = str(e)
                dlg = wx.MessageDialog(self.sweeppanel, msg, 'Error',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                # dlg.Destroy()
            return
        else:
            event.Skip()  # Default handler works when not plotting

    def on_start(self, event):
        if self.collecting:
            self.binder.collector = None
            self.psquid.sweep_off()
            self.bStart.SetLabelText('Start Sweep')
            self.enable_sweep_params(True)
            self.binder.dacpanelbindings.dirty_all()
        else:
            self.binder.collector = 'sweep'
            self.enable_sweep_params(False)

            try:
                self._sweep()
            except Exception as e:
                msg = str(e)
                dlg = wx.MessageDialog(self.sweeppanel, msg, 'Error',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
                # dlg.Destroy()
                return

            self.bStart.SetLabelText('Stop Sweep')

        self.collecting = not self.collecting
        self._plot()
        self.bStart.SendSizeEventToParent()

    def on_temp_file(self, event):
        if not self.collecting:
            state = event.IsChecked()
            self.txtFile.Enable(not state)
        event.Skip()

    def on_spin(self, event):
        spin = event.GetEventObject()

        txtdict = {self.spinNsweeps: self.txtNsweeps,
                   self.spinNavg: self.txtNavg,
                   self.spinNsettle: self.txtNsettle,
                   self.spinWSize: self.txtWSize}
        txt = txtdict[spin]

        val = int(spin.GetValue())
        txt.SetSelection(-1, -1)
        if spin is self.spinWSize:
            self.pyo.windowsize(val)
        event.Skip()

    def on_text(self, event):
        key = event.GetKeyCode()
        txt = event.GetEventObject()

        spindict = {self.txtNsweeps: self.spinNsweeps,
                    self.txtNavg: self.spinNavg,
                    self.txtNsettle: self.spinNsettle,
                    self.txtWSize: self.spinWSize}
        spin = spindict[txt]
        val = int(txt.GetValue())

        if key == 13:
            if txt is self.txtWSize:
                self.pyo.windowsize(val)
        elif key == 315:  # <Up arrow>
            val = val + 1
            if txt is self.txtWSize:
                if val == 1:  # Skip 1 for window size
                    val = 2

            maxval = spin.GetMax()
            if val <= maxval:
                if txt is self.txtWSize:
                    self.pyo.windowsize(val)
                spin.SetValue(val)

            txt.SetSelection(-1, -1)
        elif key == 317:  # <Down arrow>
            val = val - 1
            if txt is self.txtWSize:
                if val == 1:  # Skip 1 for window size
                    val = 0

            minval = spin.GetMin()
            if val >= minval:
                if txt is self.txtWSize:
                    self.pyo.windowsize(val)
                spin.SetValue(val)

            txt.SetSelection(-1, -1)
        elif key < wx.WXK_SPACE or key == wx.WXK_DELETE or key > 255:
            event.Skip()
        elif chr(key) in '0123456789':
            event.Skip()

    def on_axis_checkbox(self, event):
        self._plot()
        event.Skip()

    def on_autoscale(self, event):
        # Update control
        val = event.IsChecked()
        obj = event.GetEventObject()
        obj.SetValue(val)

        # Set scaling
        xauto = self.chkAutoscalex.GetValue()
        yauto = self.chkAutoscaley.GetValue()

        self.pyo.autoscale(xauto, yauto)

    def on_timer(self, event):
        """
        Use a timer to check the state of the connection and toggle controls.

        Much of this is redundant, but it ensures that the controls are in the
        correct state even if something goes wrong.
        """
        if not self.psquid.connected:
            for ctrl in self.controls:
                ctrl.Disable()
            return
        else:
            for ctrl in self.controls:
                ctrl.Enable()

        # if self.psquid.connection.state != 'sweep':
        #     self.collecting = False
        #     self.binder.collector = None

        if self.collecting and (self.binder.collector == 'sweep'):
            self.enable_sweep_params(False)
            self.bStart.SetLabelText('Stop Sweep')
        else:
            self.enable_sweep_params(True)
            self.collecting = False
            self.bStart.SetLabelText('Start Sweep')
        self.bStart.SendSizeEventToParent()

        if self.chkFile.GetValue():
            self.txtFile.Disable()

    def enable_sweep_params(self, state=True):
        """Enable or disable the sweep params boxes"""
        ctrls = [self.sldStart,
                 self.txtStart,
                 self.sldEnd,
                 self.txtEnd,
                 self.sldStep,
                 self.txtStep,
                 self.txtFile,
                 self.chkFile,
                 self.spinNsweeps,
                 self.spinNavg,
                 self.spinNsettle]

        for ctrl in ctrls:
            ctrl.Enable(state)

        state = self.chkFile.IsChecked()
        self.txtFile.Enable(not state)


    def update_dac_txt(self, which):
        slddict = {'start': self.sldStart,
                   'end': self.sldEnd,
                   'step': self.sldStep,
                   0: self.sldStart,
                   1: self.sldEnd,
                   2: self.sldStep,
                   self.sldStart: self.sldStart,
                   self.sldEnd: self.sldEnd,
                   self.sldStep: self.sldStep,
                   self.txtStart: self.sldStart,
                   self.txtEnd: self.sldEnd,
                   self.txtStep: self.sldStep}

        txtdict = {'start': self.txtStart,
                   'end': self.txtEnd,
                   'step': self.txtStep,
                   0: self.txtStart,
                   1: self.txtEnd,
                   2: self.txtStep,
                   self.sldStart: self.txtStart,
                   self.sldEnd: self.txtEnd,
                   self.sldStep: self.txtStep,
                   self.txtStart: self.txtStart,
                   self.txtEnd: self.txtEnd,
                   self.txtStep: self.txtStep}

        try:
            which = which.lower()
        except AttributeError:
            pass
        sld = slddict[which]
        txt = txtdict[which]
        validator = txt.GetValidator()

        val = sld.GetValue()
        val = validator._value(val)
        txt.SetValue(str(val))

    def update_dac_slider(self, which):
        slddict = {'start': self.sldStart,
                   'end': self.sldEnd,
                   'step': self.sldStep,
                   0: self.sldStart,
                   1: self.sldEnd,
                   2: self.sldStep,
                   self.sldStart: self.sldStart,
                   self.sldEnd: self.sldEnd,
                   self.sldStep: self.sldStep,
                   self.txtStart: self.sldStart,
                   self.txtEnd: self.sldEnd,
                   self.txtStep: self.sldStep}

        txtdict = {'start': self.txtStart,
                   'end': self.txtEnd,
                   'step': self.txtStep,
                   0: self.txtStart,
                   1: self.txtEnd,
                   2: self.txtStep,
                   self.sldStart: self.txtStart,
                   self.sldEnd: self.txtEnd,
                   self.sldStep: self.txtStep,
                   self.txtStart: self.txtStart,
                   self.txtEnd: self.txtEnd,
                   self.txtStep: self.txtStep}

        try:
            which = which.lower()
        except AttributeError:
            pass
        sld = slddict[which]
        txt = txtdict[which]
        validator = txt.GetValidator()

        val = float(txt.GetValue())
        val = validator._index(val)

        sld.SetValue(val)


class GLockPanelBindings(object):
    """
    Callback functions for the Graphical Lock Panel bindings.

    Note that the order of DACs on the GUI is different from order of DACs in
    the PIC code. The GUI index is used in all instances here, and the
    translation to PIC index is performed by the timer event callback.

    GUI Index  --  GUI DAC  --  PIC Index
    0              Det Bias     3
    1              S1 Bias      0
    2              S1 FB        4
    3              S2 Bias      1
    4              S2 FB        5
    5              S3 Bias      2
    6              S3 FB        6
    7              Offset       7
    8              Gain         8
    """
    def __init__(self, binder, glockpanel):
        self.binder = binder
        self.psquid = self.binder.psquid
        self.pyo = self.binder.pyo
        self.lockpanel = glockpanel
        self.picindices = [3, 0, 4, 1, 5, 2, 6, 7, 8]

        # Controls
        self.lblStepValue = self.lockpanel.lblStepValue
        self.sbStep = self.lockpanel.sbStep
        self.sbSweep = self.lockpanel.sbSweep
        self.sbPID = self.lockpanel.sbPID
        self.lblStep = self.lockpanel.lblStep
        self.lblStartStep = self.lockpanel.lblStartStep
        self.lblEndStep = self.lockpanel.lblEndStep
        self.lblStepStep = self.lockpanel.lblStepStep
        self.lblSweep = self.lockpanel.lblSweep
        self.lblMonitor = self.lockpanel.lblMonitor
        self.lblStartSweep = self.lockpanel.lblStartSweep
        self.lblEndSweep = self.lockpanel.lblEndSweep
        self.lblStepSweep = self.lockpanel.lblStepSweep
        self.lblNsweeps = self.lockpanel.lblNsweeps
        self.lblNavg = self.lockpanel.lblNavg
        self.lblNsettle = self.lockpanel.lblNsettle
        self.lblPeq = self.lockpanel.lblPeq
        self.lblPover = self.lockpanel.lblPover
        self.lblIeq = self.lockpanel.lblIeq
        self.lblIover = self.lockpanel.lblIover

        self.timer = self.lockpanel.timer
        self.sldStepValue = self.lockpanel.sldStepValue
        self.txtStepValue = self.lockpanel.txtStepValue
        self.bSend = self.lockpanel.bSend

        self.chStep = self.lockpanel.chStep
        self.txtStartStep = self.lockpanel.txtStartStep
        self.txtEndStep = self.lockpanel.txtEndStep
        self.txtStepStep = self.lockpanel.txtStepStep

        self.chSweep = self.lockpanel.chSweep
        self.chMonitor = self.lockpanel.chMonitor
        self.txtStartSweep = self.lockpanel.txtStartSweep
        self.txtEndSweep = self.lockpanel.txtEndSweep
        self.txtStepSweep = self.lockpanel.txtStepSweep
        self.txtNsweeps = self.lockpanel.txtNsweeps
        self.txtNavg = self.lockpanel.txtNavg
        self.txtNsettle = self.lockpanel.txtNsettle

        self.txtP = self.lockpanel.txtP
        self.txtPshift = self.lockpanel.txtPshift
        self.txtI = self.lockpanel.txtI
        self.txtIshift = self.lockpanel.txtIshift
        self.chkNegative = self.lockpanel.chkNegative
        self.bSendPID = self.lockpanel.bSendPID
        self.bResetPID = self.lockpanel.bResetPID

        self.bStart = self.lockpanel.bStart
        self.bLock = self.lockpanel.bLock

        self.controls = [self.lblStepValue, self.bLock, self.sbStep,
                         self.sbSweep, self.sbPID, self.lblStep,
                         self.lblStartStep, self.lblEndStep,
                         self.lblStepStep, self.lblSweep, self.lblMonitor,
                         self.lblStartSweep, self.lblEndSweep,
                         self.lblStepSweep, self.lblNsweeps, self.lblNavg,
                         self.lblNsettle, self.lblPeq, self.lblPover,
                         self.lblIeq, self.lblIover, self.sldStepValue,
                         self.txtStepValue, self.bSend, self.chStep,
                         self.txtStartStep, self.txtEndStep, self.txtStepStep,
                         self.chSweep, self.chMonitor, self.txtStartSweep,
                         self.txtEndSweep, self.txtStepSweep, self.txtNsweeps,
                         self.txtNavg, self.txtNsettle, self.txtP,
                         self.txtPshift, self.txtI, self.txtIshift,
                         self.chkNegative, self.bSendPID, self.bResetPID,
                         self.bStart]

        self.rangetxtctrls = [self.txtStartSweep, self.txtEndSweep,
                              self.txtStepSweep, self.txtStartStep,
                              self.txtEndStep, self.txtStepStep]
        self.Ntxtctrls = [self.txtNsweeps, self.txtNavg, self.txtNsettle]
        self.pidtxtctrls = [self.txtP, self.txtPshift, self.txtI,
                            self.txtIshift]
        # State variables
        self.collecting = False
        self.collected = False
        self.pidcurrent = True
        self.bound = False
        self.stepindex = None

        self.hline = None
        self.vline = None
        self.circle = None

        # Controllers
        self.lc = None

    def bind(self):
        self.lockpanel.bindings = self
        # Canvas not created until Bindings.bind()
        self.canvas = self.pyo.canvas

        # Initialize controls
        self.initialize()

        # Bind controls
        for txt in self.rangetxtctrls:
            txt.Bind(wx.EVT_CHAR, self.on_range_text)

        self.txtStepValue.Bind(wx.EVT_CHAR, self.on_txt_step_val)
        self.sldStepValue.Bind(wx.EVT_SCROLL, self.on_slider)

        self.bStart.Bind(wx.EVT_BUTTON, self.on_start)
        self.bSend.Bind(wx.EVT_BUTTON, self.on_send)
        self.chkNegative.Bind(wx.EVT_CHECKBOX, self.on_negative)
        self.bSendPID.Bind(wx.EVT_BUTTON, self.on_send_PID)
        self.bResetPID.Bind(wx.EVT_BUTTON, self.on_reset)
        self.bLock.Bind(wx.EVT_BUTTON, self.on_lock)

        self.pyo.canvas.mpl_connect('button_press_event', self.on_click)

        # Bind timer
        self.lockpanel.Bind(wx.EVT_TIMER, self.on_timer, self.timer)

    def initialize(self):
        for txt in self.rangetxtctrls:
            validator = DACValidator(self.binder.raw)
            txt.SetValidator(validator)

        for txt in [self.txtStartSweep, self.txtStartStep]:
            txt.SetValue('0.0')
        for txt in [self.txtEndSweep, self.txtEndStep]:
            txt.SetValue('1.0')
        for txt in [self.txtStepSweep, self.txtStepStep]:
            validator = txt.GetValidator()
            txt.SetValue(str(validator._validate(0.1)))

        self.txtNsweeps.SetValidator(IntegerValidator(minval=1, maxval=65535))
        self.txtNsweeps.SetValue('1')
        self.txtNavg.SetValidator(IntegerValidator(minval=100, maxval=65535))
        self.txtNavg.SetValue('167')
        self.txtNsettle.SetValidator(IntegerValidator(minval=10,
                                                      maxval=65535))
        self.txtNsettle.SetValue('50')

        self.txtP.SetValidator(IntegerValidator(minval=0, maxval=65535))
        self.txtPshift.SetValidator(IntegerValidator(minval=0, maxval=32))
        self.txtI.SetValidator(IntegerValidator(minval=0, maxval=65535))
        self.txtIshift.SetValidator(IntegerValidator(minval=0, maxval=32))
        self.set_pid_controls(self.binder.piddict)

        self.sldStepValue.SetValue(0)
        validator = DACValidator(self.binder.raw, num=2)
        self.txtStepValue.SetValidator(validator)
        self.txtStepValue.SetValue('0')

    def _plot(self):
        """
        Make a plot of the currently active data.
        """
        if self.collected and self.bound:
            # Clear crosshairs, if active
            self.clear_crosshairs()

            index = self.sldStepValue.GetValue()
            self.f = self.lc.files[index]
            try:
                pyofname = self.pyo.reader.filename
            except AttributeError:
                pyofname = ''
            if pyofname != self.f:
                self.pyo.switch_file(self.f, reader=HexReader)

            xs = ['swept']

            if self.binder.locked:
                ys = ['locked']
            else:
                ys = ['adc']

            self.pyo.plot(xs, ys, legend=True, windowsize=0)

            self.pyo.autoscale(False)

            minxval = self.get_index_from_txt(self.txtStartSweep)
            maxxval = self.get_index_from_txt(self.txtEndSweep)
            validatorstart = self.txtStartSweep.GetValidator()
            validatorend = self.txtEndSweep.GetValidator()
            if not self.binder.raw:
                minxval = validatorstart._value(minxval)
                maxxval = validatorend._value(maxxval)

            if self.binder.locked:
                minyval, maxyval = self.lc.dacminmax
            else:
                minyval, maxyval = self.lc.adcminmax
            if not self.binder.raw:
                minyval = minyval/65535.
                maxyval = maxyval/65535.

            dx = maxxval - minxval
            dy = maxyval - minyval

            self.ax = self.pyo.axes[0, 0]
            self.ax.set_xlim(minxval - 0.1*dx, maxxval + 0.1*dx)
            self.ax.set_ylim(minyval - 0.1*dy, maxyval + 0.1*dy)

            self.canvas.draw()

    def on_slider(self, event):
        # Send new value to text control
        self.update_step_txt()
        self._plot()

    def on_txt_step_val(self, event):
        key = event.GetKeyCode()

        if key == 13:  # User pressed <Enter>
            self.update_step_slider()
            self._plot()
        elif key == 315:  # <Up arrow>
            self.update_step_slider()
            self._plot()
        elif key == 317:  # <Down arrow>
            self.update_step_slider()
            self._plot()
        else:
            event.Skip()

    def on_start(self, event):
        try:
            self.lc.close()
        except AttributeError:
            pass

        # Get parameters for data collection
        self.stepindex = self.chStep.GetSelection()
        dacindexstep = self.picindices[self.stepindex]
        startstep = self.get_index_from_txt(self.txtStartStep)
        endstep = self.get_index_from_txt(self.txtEndStep)
        deltastep = self.get_index_from_txt(self.txtStepStep)
        settle = 0.01
        stepparams = {'dacindex': dacindexstep,
                      'start': startstep,
                      'end': endstep,
                      'delta': deltastep,
                      'settle': settle}

        self.binder.lockindex = self.chSweep.GetSelection()
        dacindexsweep = self.picindices[self.binder.lockindex]
        monindexsweep = self.picindices[self.chMonitor.GetSelection()]
        startsweep = self.get_index_from_txt(self.txtStartSweep)
        endsweep = self.get_index_from_txt(self.txtEndSweep)
        deltasweep = self.get_index_from_txt(self.txtStepSweep)
        nsweeps = int(self.txtNsweeps.GetValue())
        navg = int(self.txtNavg.GetValue())
        nsettle = int(self.txtNsettle.GetValue())
        sweepparams = {'dacindex': self.binder.lockindex,
                       'monindex': monindexsweep,
                      'start': startsweep,
                      'end': endsweep,
                      'delta': deltasweep,
                      'nsweeps': nsweeps,
                      'navg': navg,
                      'nsettle': nsettle}

        # Create data collection controller and progress window
        self.lc = LockController(self.psquid, stepparams=stepparams,
                            sweepparams=sweepparams)

        dlg = wx.ProgressDialog("Collecting Data",
                                "Collecting V-Phi data, please wait...",
                                maximum = 100,
                                parent=self.lockpanel,
                                style = wx.PD_CAN_ABORT
                                | wx.PD_APP_MODAL
                                | wx.PD_ELAPSED_TIME
                                | wx.PD_ESTIMATED_TIME
                                # | wx.PD_REMAINING_TIME
                               )

        # Collect data and update progress window
        self.binder.collector = 'glock'
        cont = True
        for completion in self.lc:
            count = int(100*completion)
            print "{0}% completed".format(count)
            (cont, skip) = dlg.Update(count)
            wx.Yield()
            if not cont:  # If user presses 'Cancel', stop collecting data
                break

        dlg.Destroy()

        # Hook up Step Value slider and textctrl to collected data
        self.bound = False
        numcollected = len(self.lc.files)
        num = self.lc.stepparams['num']
        minval = self.lc.stepparams['start']
        delta = self.lc.stepparams['delta']
        rawhigh = minval + (numcollected-1)*delta
        scaledlow = minval/65535.
        scaledhigh = rawhigh/65535.
        self.sldStepValue.SetMin(0)
        self.sldStepValue.SetMax(numcollected - 1)

        rawrange = (minval, rawhigh)
        scaledrange = (scaledlow, scaledhigh)
        validator = DACValidator(raw=self.binder.raw,
                                 rawrange=rawrange,
                                 scaledrange=scaledrange,
                                 num=numcollected,
                                 stepval=1,
                                 sig=6)
        self.txtStepValue.SetValidator(validator)  # Uses validator.Clone()
        validator = self.txtStepValue.GetValidator()  # Must get new instance
        val = self.txtStepValue.GetValue()
        val = validator._validate(val)
        self.txtStepValue.SetValue(str(val))

        names = ['DetB', 'S1B', 'S1FB', 'S2B', 'S2FB', 'S3B', 'S3FB',
                 'Offset', 'Gain']
        newname = names[self.stepindex]
        bsendname = 'Send to {0}'.format(newname)
        self.bSend.SetLabelText(bsendname)
        self.bSend.SendSizeEventToParent()

        self.bound = True

        # Update state
        self.collected = True
        self.bStart.SetLabelText('Collect New Data')
        self.bStart.SendSizeEventToParent()

        # Set up plot
        if not self.binder.raw:
            def callback(s):
                s.data = s.data/65535.
        else:
            def callback(s):
                pass
        self.pyo.set_callback(callback)

        self.pyo.autoscale(False)
        self._plot()

    def on_send(self, event):
        if not self.collected or (self.stepindex is None):
            return

        index = self.sldStepValue.GetValue()
        dacval = self.lc._value(index)

        dacpanelbindings = self.binder.dacpanelbindings
        dacpanelbindings.schedule_dac(self.stepindex, dacval)

        sld = dacpanelbindings.sliders[self.stepindex]
        sld.SetValue(dacval)
        dacpanelbindings.update_dac_txt(self.stepindex)

    def on_negative(self, event):
        neg = self.chkNegative.GetValue()
        pos = (not neg)
        self.psquid.pid_params(sign=pos)

    def on_send_PID(self, event):
        # Read values from controls
        p = int(self.txtP.GetValue())
        pshift = int(self.txtPshift.GetValue())
        i = int(self.txtI.GetValue())
        ishift = int(self.txtIshift.GetValue())

        # Store new values
        piddict = {'p': p, 'pshift': pshift, 'i': i, 'ishift': ishift}
        self.binder.piddict = piddict

        # Send parameters to board
        self.psquid.pid_params(p, pshift, i, ishift)
        self.pidcurrent = True

    def on_reset(self, event):
        self.set_pid_controls(self.binder.piddict)

    def on_lock(self, event):
        if self.binder.locked:
            self.psquid.unlock()
            self.binder.locked = False
            self.binder.lockindex = None
            self.binder.dacpanelbindings.dirty_all()
        else:
            if self.circle is None or self.vline is None or self.hline is None:
                return

            # Get (swept DAC, ADC value) at which to lock
            x = self.circle.get_xdata()[0]
            y = self.circle.get_ydata()[0]

            xvalidator = DACValidator(self.binder.raw)
            scaledrange = (0., 1.) if self.binder.locked else (0., 10.)
            yvalidator = DACValidator(self.binder.raw, scaledrange=scaledrange)
            x = xvalidator._index(xvalidator._validate(x))
            y = yvalidator._index(yvalidator._validate(y))

            # Get to-be-locked DAC and PID parameters
            lockdacindex = self.picindices[self.binder.lockindex]
            p, pshift, i, ishift = self.get_pid_control_values()

            # Set bias and lock
            self.on_send(None)  # Set bias
            while True:  # Wait for bias DAC to be set
                wx.Yield()
                index = self.sldStepValue.GetValue()
                dacval = self.lc._value(index)

                dacpanelbindings = self.binder.dacpanelbindings
                sld = dacpanelbindings.sliders[self.stepindex]
                sldval = sld.GetValue()

                if dacval == sldval:
                    break
                else:
                    time.sleep(.2)
            time.sleep(.2)  # Let bias DAC settle
            self.psquid.lock(ch=lockdacindex,
                             setpoint=y,
                             p=p, p_shift=pshift,
                             i=i, i_shift=ishift,
                             init_dac=x,
                             raw=True)
            self.binder.locked = True
            self.binder.piddict = {'p': p, 'pshift': pshift,
                                   'i': i, 'ishift': ishift}

    def on_range_text(self, event):
        obj = event.GetEventObject()
        key = event.GetKeyCode()

        if key == 315:  # <Up arrow>
            obj.SetSelection(-1, -1)
        elif key == 317:  # <Down arrow>
            obj.SetSelection(-1, -1)
        else:
            event.Skip()

    def on_click(self, event):
        """
        Called when clicking on the plot.
        """
        x, y = event.xdata, event.ydata
        ax = event.inaxes

        # Clear previous crosshairs
        self.clear_crosshairs()

        # Find nearest point to click and make crosshairs at point
        line = ax.lines[0]
        xdata = line.get_xdata()
        ydata = line.get_ydata()
        nearxindex = self.find_nearest_index(xdata, x)
        nearx = xdata[nearxindex]
        neary = ydata[nearxindex]
        self.vline = ax.axvline(x=nearx, ls='--', c='k')  # Black dashed line
        self.hline = ax.axhline(y=neary, ls='--', c='k')
        self.circle, = ax.plot([nearx], [neary], ls='', c='k', marker='o')

    def on_timer(self, event):
        """
        Use a timer to check the state of the connection and toggle controls.

        Much of this is redundant, but it ensures that the controls are in the
        correct state even if something goes wrong.
        """
        if not self.psquid.connected:
            for ctrl in self.controls:
                ctrl.Disable()
            return
        else:
            for ctrl in self.controls:
                ctrl.Enable()

        if (self.binder.collector not in (None, 'glock')) or \
           (not self.collected or self.collecting):
            self.enable_lock_params(False)
        else:
            self.enable_lock_params(True)

        if self.binder.locked:
            self.pidcurrent = self.check_pid_controls()
            self.bResetPID.Enable(not self.pidcurrent)
            self.bLock.Enable()
            self.enable_pid_params(True)
            self.bLock.SetLabelText('Unlock')
            if self.pidcurrent:
                self.bSendPID.SetLabelText('Parameters are Current')
                self.bSendPID.Disable()
            else:
                self.bSendPID.SetLabelText('Set PID Parameters')
                self.bSendPID.Enable()
        else:
            self.enable_pid_params(False)
            if (self.circle is not None) and (self.vline is not None) and \
               (self.hline is not None):
                self.bLock.SetLabelText('Lock')
            else:
                btext = 'Click on plot to select lock location'
                self.bLock.SetLabelText(btext)
                self.bLock.Disable()
            self.bSendPID.SetLabelText('Not Locked')
            self.bSendPID.Disable()
            self.bResetPID.Disable()
        self.bSendPID.SendSizeEventToParent()
        self.bLock.SendSizeEventToParent()

    def enable_lock_params(self, state=True):
        """Enable or disable the lock params controls"""
        ctrls = [self.lblStepValue,
                 self.sldStepValue,
                 self.txtStepValue,
                 self.bSend]#,
                 # self.sbPID,
                 # self.lblPeq,
                 # self.txtP,
                 # self.lblPover,
                 # self.txtPshift,
                 # self.lblIeq,
                 # self.txtI,
                 # self.lblIover,
                 # self.txtIshift,
                 # self.bSendPID]

        for ctrl in ctrls:
            ctrl.Enable(state)

    def enable_pid_params(self, state=True):
        """Enable or disable the lock params controls"""
        ctrls = [self.sbPID,
                 self.lblPeq,
                 self.txtP,
                 self.lblPover,
                 self.txtPshift,
                 self.lblIeq,
                 self.txtI,
                 self.lblIover,
                 self.txtIshift,
                 self.bSendPID,
                 self.bLock]

        for ctrl in ctrls:
            ctrl.Enable(state)

    def get_index_from_txt(self, txt):
        val = float(txt.GetValue())
        validator = txt.GetValidator()
        scale = int if validator.raw else validator._index
        index = scale(val)
        return index

    def update_step_txt(self):
        if self.bound:
            sld = self.sldStepValue
            txt = self.txtStepValue
            validator = txt.GetValidator()

            val = sld.GetValue()
            val = validator._value(val)
            txt.SetValue(str(val))

    def update_step_slider(self):
        if self.bound:
            sld = self.sldStepValue
            txt = self.txtStepValue
            validator = txt.GetValidator()

            val = float(txt.GetValue())
            val = validator._index(val)

            sld.SetValue(val)

    def set_pid_controls(self, piddict=None):
        """
        Set only the values displayed on the PID controls to the specified
        values. If None, uses the stored previous values. Does NOT send the
        updated values to the PSquid board!
        """
        if piddict is None:
            piddict = self.binder.piddict

        params = (piddict['p'], piddict['pshift'], piddict['i'],
                  piddict['ishift'])
        ctrls = (self.txtP, self.txtPshift, self.txtI, self.txtIshift)

        for i, ctrl in enumerate(ctrls):
            param = params[i]
            ctrl.SetValue(str(param))

    def get_pid_control_values(self):
        """
        Get the values of the PID controls.

        Returns (p, pshift, i, ishift).
        """
        ctrls = (self.txtP, self.txtPshift, self.txtI, self.txtIshift)

        return [int(ctrl.GetValue()) for ctrl in ctrls]

    def check_pid_controls(self, piddict=None):
        """
        Compare the values displayed on the PID controls to the specified
        values. If None, uses the stored previous values.

        Returns True if the controls display the same values as the
        dictionary, otherwise False.
        """
        if piddict is None:
            piddict = self.binder.piddict

        params = (piddict['p'], piddict['pshift'], piddict['i'],
                  piddict['ishift'])
        ctrls = (self.txtP, self.txtPshift, self.txtI, self.txtIshift)

        for i, ctrl in enumerate(ctrls):
            param = params[i]
            ctrlparam = int(ctrl.GetValue())
            if param != ctrlparam:
                return False
        return True

    def clear_crosshairs(self):
        # Clear previous crosshairs
        if self.vline:
            try:
                self.vline.remove()
            except ValueError:
                pass
        if self.hline:
            try:
                self.hline.remove()
            except ValueError:
                pass
        if self.circle:
            try:
                self.circle.remove()
            except ValueError:
                pass

        self.vline = None
        self.hline = None
        self.circle = None


    @staticmethod
    def find_nearest_index(array, value):
        idx = (np.abs(array-value)).argmin()
        return idx

    @staticmethod
    def find_nearest(array, value):
        idx = self.find_nearest_index(array, value)
        return array[idx]


class MLockPanelBindings(object):
    """
    Callback functions for the Manual Lock Panel bindings.

    Note that the order of DACs on the GUI is different from order of DACs in
    the PIC code. The GUI index is used in all instances here, and the
    translation to PIC index is performed by the timer event callback.

    GUI Index  --  GUI DAC  --  PIC Index
    0              Det Bias     3
    1              S1 Bias      0
    2              S1 FB        4
    3              S2 Bias      1
    4              S2 FB        5
    5              S3 Bias      2
    6              S3 FB        6
    7              Offset       7
    8              Gain         8
    """
    def __init__(self, binder, mlockpanel):
        self.binder = binder
        self.psquid = self.binder.psquid
        self.pyo = self.binder.pyo
        self.lockpanel = mlockpanel
        self.picindices = [3, 0, 4, 1, 5, 2, 6, 7, 8]

        # Controls
        self.sbPID = self.lockpanel.sbPID
        self.lblADC = self.lockpanel.lblADC
        self.lblDACs = self.lockpanel.lblDACs
        self.lblInit = self.lockpanel.lblInit
        self.lblPeq = self.lockpanel.lblPeq
        self.lblPover = self.lockpanel.lblPover
        self.lblIeq = self.lockpanel.lblIeq
        self.lblIover = self.lockpanel.lblIover

        self.timer = self.lockpanel.timer

        self.chDACs = self.lockpanel.chDACs
        self.txtADC = self.lockpanel.txtADC
        self.sldADC = self.lockpanel.sldADC
        self.txtInit = self.lockpanel.txtInit
        self.sldInit = self.lockpanel.sldInit
        self.txtP = self.lockpanel.txtP
        self.txtPshift = self.lockpanel.txtPshift
        self.txtI = self.lockpanel.txtI
        self.txtIshift = self.lockpanel.txtIshift
        self.chkNegative = self.lockpanel.chkNegative
        self.bSendPID = self.lockpanel.bSendPID
        self.bResetPID = self.lockpanel.bResetPID

        self.bLock = self.lockpanel.bLock

        self.controls = [self.lblDACs, self.lblADC, self.chDACs, self.lblInit,
                         self.sldInit, self.txtADC, self.sldADC, self.txtInit,
                         self.bLock, self.lblPeq, self.lblPover, self.lblIeq,
                         self.lblIover, self.txtP, self.txtPshift, self.txtI,
                         self.txtIshift, self.chkNegative, self.bSendPID,
                         self.bResetPID]

        self.txtctrls = [self.txtADC, self.txtInit]
        self.sldctrls = [self.sldADC, self.sldInit]

        self.pidtxtctrls = [self.txtP, self.txtPshift, self.txtI,
                            self.txtIshift]
        # State variables
        self.pidcurrent = True
        self.binder.piddict = {'p': 10, 'pshift': 4,
                        'i': 100, 'ishift': 8}

    def bind(self):
        self.lockpanel.bindings = self
        # Canvas not created until Bindings.bind()
        self.canvas = self.pyo.canvas

        # Initialize controls
        self.initialize()

        # Bind controls
        for txt in self.txtctrls:
            txt.Bind(wx.EVT_CHAR, self.on_txt_char)
        for sld in self.sldctrls:
            sld.Bind(wx.EVT_SCROLL, self.on_slider)
        self.chkNegative.Bind(wx.EVT_CHECKBOX, self.on_negative)
        self.bSendPID.Bind(wx.EVT_BUTTON, self.on_send_PID)
        self.bResetPID.Bind(wx.EVT_BUTTON, self.on_reset)
        self.bLock.Bind(wx.EVT_BUTTON, self.on_lock)

        # Bind timer
        self.lockpanel.Bind(wx.EVT_TIMER, self.on_timer, self.timer)

    def initialize(self):
        for txt in self.txtctrls:
            validator = DACValidator(self.binder.raw)
            txt.SetValidator(validator)

        validator = DACValidator(self.binder.raw, scaledrange=(0., 1.))
        self.txtADC.SetValidator(validator)

        self.sldADC.SetValue(0)
        self.update_dac_txt('adc')
        self.sldInit.SetValue(0)
        self.update_dac_txt('init')

        self.txtP.SetValidator(IntegerValidator(minval=0, maxval=65535))
        self.txtPshift.SetValidator(IntegerValidator(minval=0, maxval=32))
        self.txtI.SetValidator(IntegerValidator(minval=0, maxval=65535))
        self.txtIshift.SetValidator(IntegerValidator(minval=0, maxval=32))
        self.set_pid_controls(self.binder.piddict)

    def on_slider(self, event):
        obj = event.GetEventObject()

        # Send new value to text control
        self.update_dac_txt(obj)

    def on_txt_char(self, event):
        key = event.GetKeyCode()
        obj = event.GetEventObject()

        if key == 13:  # User pressed <Enter>
            self.update_dac_slider(obj)
        elif key == 315:  # <Up arrow>
            self.update_dac_slider(obj)
        elif key == 317:  # <Down arrow>
            self.update_dac_slider(obj)
        else:
            event.Skip()

    def on_negative(self, event):
        neg = self.chkNegative.GetValue()
        pos = (not neg)
        self.psquid.pid_params(sign=pos)

    def on_send_PID(self, event):
        # Read values from controls
        p = int(self.txtP.GetValue())
        pshift = int(self.txtPshift.GetValue())
        i = int(self.txtI.GetValue())
        ishift = int(self.txtIshift.GetValue())

        # Store new values
        piddict = {'p': p, 'pshift': pshift, 'i': i, 'ishift': ishift}
        self.binder.piddict = piddict

        # Send parameters to board
        self.psquid.pid_params(p, pshift, i, ishift)
        self.pidcurrent = True

    def on_reset(self, event):
        self.set_pid_controls(self.binder.piddict)

    def on_lock(self, event):
        if self.binder.locked:
            self.psquid.unlock()
            self.binder.locked = False
            self.binder.lockindex = None
        else:
            x = self.sldInit.GetValue()
            y = self.sldADC.GetValue()
            self.binder.lockindex = self.chDACs.GetSelection()
            lockdacindex = self.picindices[self.binder.lockindex]
            p, pshift, i, ishift = self.get_pid_control_values()

            # Lock
            self.psquid.lock(ch=lockdacindex,
                             setpoint=y,
                             p=p, p_shift=pshift,
                             i=i, i_shift=ishift,
                             init_dac=x,
                             raw=True)
            self.binder.locked = True
            self.binder.piddict = {'p': p, 'pshift': pshift,
                                   'i': i, 'ishift': ishift}

    def on_timer(self, event):
        """
        Use a timer to check the state of the connection and toggle controls.

        Much of this is redundant, but it ensures that the controls are in the
        correct state even if something goes wrong.
        """
        if not self.psquid.connected:
            for ctrl in self.controls:
                ctrl.Disable()
            return
        else:
            for ctrl in self.controls:
                ctrl.Enable()

        if self.binder.locked:
            self.enable_lock_params(False)
            self.pidcurrent = self.check_pid_controls()
            self.bResetPID.Enable(not self.pidcurrent)
            self.bLock.SetLabelText('Unlock')
            if self.pidcurrent:
                self.bSendPID.SetLabelText('Parameters are Current')
                self.bSendPID.Disable()
            else:
                self.bSendPID.SetLabelText('Set PID Parameters')
                self.bSendPID.Enable()
        else:
            self.enable_lock_params(True)
            self.bLock.SetLabelText('Lock')
            self.bLock.Enable()
            self.bSendPID.SetLabelText('Not Locked')
            self.bSendPID.Disable()
            self.bResetPID.Disable()
        self.bSendPID.SendSizeEventToParent()
        self.bLock.SendSizeEventToParent()

    def enable_lock_params(self, state=True):
        """Enable or disable the lock params controls"""
        ctrls = [self.lblADC,
                 self.sldADC,
                 self.txtADC,
                 self.lblDACs,
                 self.chDACs,
                 self.lblInit,
                 self.sldInit,
                 self.txtInit]

        for ctrl in ctrls:
            ctrl.Enable(state)

    def set_pid_controls(self, piddict=None):
        """
        Set only the values displayed on the PID controls to the specified
        values. If None, uses the stored previous values. Does NOT send the
        updated values to the PSquid board!
        """
        if piddict is None:
            piddict = self.binder.piddict

        params = (piddict['p'], piddict['pshift'], piddict['i'],
                  piddict['ishift'])
        ctrls = (self.txtP, self.txtPshift, self.txtI, self.txtIshift)

        for i, ctrl in enumerate(ctrls):
            param = params[i]
            ctrl.SetValue(str(param))

    def get_pid_control_values(self):
        """
        Get the values of the PID controls.

        Returns (p, pshift, i, ishift).
        """
        ctrls = (self.txtP, self.txtPshift, self.txtI, self.txtIshift)

        return [int(ctrl.GetValue()) for ctrl in ctrls]

    def check_pid_controls(self, piddict=None):
        """
        Compare the values displayed on the PID controls to the specified
        values. If None, uses the stored previous values.

        Returns True if the controls display the same values as the
        dictionary, otherwise False.
        """
        if piddict is None:
            piddict = self.binder.piddict

        params = (piddict['p'], piddict['pshift'], piddict['i'],
                  piddict['ishift'])
        ctrls = (self.txtP, self.txtPshift, self.txtI, self.txtIshift)

        for i, ctrl in enumerate(ctrls):
            param = params[i]
            ctrlparam = int(ctrl.GetValue())
            if param != ctrlparam:
                return False
        return True

    def update_dac_txt(self, which):
        slddict = {'adc': self.sldADC,
                   'init': self.sldInit,
                   0: self.sldADC,
                   1: self.sldInit,
                   self.sldADC: self.sldADC,
                   self.txtADC: self.sldADC,
                   self.sldInit: self.sldInit,
                   self.txtInit: self.sldInit}

        txtdict = {'adc': self.txtADC,
                   'init': self.txtInit,
                   0: self.txtADC,
                   1: self.txtInit,
                   self.sldADC: self.txtADC,
                   self.txtADC: self.txtADC,
                   self.sldInit: self.txtInit,
                   self.txtInit: self.txtInit}

        try:
            which = which.lower()
        except AttributeError:
            pass
        sld = slddict[which]
        txt = txtdict[which]
        validator = txt.GetValidator()

        val = sld.GetValue()
        val = validator._value(val)
        txt.SetValue(str(val))

    def update_dac_slider(self, which):
        slddict = {'adc': self.sldADC,
                   'init': self.sldInit,
                   0: self.sldADC,
                   1: self.sldInit,
                   self.sldADC: self.sldADC,
                   self.txtADC: self.sldADC,
                   self.sldInit: self.sldInit,
                   self.txtInit: self.sldInit}

        txtdict = {'adc': self.txtADC,
                   'init': self.txtInit,
                   0: self.txtADC,
                   1: self.txtInit,
                   self.sldADC: self.txtADC,
                   self.txtADC: self.txtADC,
                   self.sldInit: self.txtInit,
                   self.txtInit: self.txtInit}

        try:
            which = which.lower()
        except AttributeError:
            pass
        sld = slddict[which]
        txt = txtdict[which]
        validator = txt.GetValidator()

        val = float(txt.GetValue())
        val = validator._index(val)

        sld.SetValue(val)
