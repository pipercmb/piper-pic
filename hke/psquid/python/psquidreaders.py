#!/bin/env python

"""
psquidreaders.py
jlazear
2014-02-25

File readers for psquid data.

Example:

<example code here>
"""
version = 20140225
releasestatus = 'beta'

from types import StringTypes
from tempfile import _TemporaryFileWrapper
import numpy as np
import pandas as pd


class PSquidDataReader(object):
    """
    Reader for binary-encoded data from the psquid data command.

    See pyoscope.readers.ReaderInterface for info on readers.

    Note that this reader is stupidly inefficient...
    """
    def __init__(self, f, header=True, *args, **kwargs):
        # Load file
        if isinstance(f, file) or isinstance(f, _TemporaryFileWrapper):
            mode = f.mode
            if ('r' in mode) or ('+' in mode):
                self.f = f
            else:
                self.f = open(f.name, 'r')
        elif isinstance(f, StringTypes):
            self.f = open(f, 'r')
        else:
            raise TypeError('f must be a file handle or filename.')
        self.f.seek(0)
        self.filename = self.f.name

        # Construct header if wanted
        if header:
            self._construct_header()
        else:
            self.header = {}

        # Seek to end of header
        self._skip_header()


    # @staticmethod
    # def _cfunc(val):
    #     return float(int(val, 16))

    @staticmethod
    def _split_columns(colstr, typecast=str):
        """
        "[rdac, adc]" or "rdac, adc" or "[rdac adc]" or "rdac adc"
        all go to -> ["rdac", "adc"]

        unless typecast is specified, in which case typecast() is called
        on every value in list
        """
        colstr = colstr.strip('[]')
        if ',' in colstr:
            splitchar = ','
        else:
            splitchar = ' '
        collist = colstr.split(splitchar)
        collist = [typecast(col.strip()) for col in collist]
        return collist

    def _read_header(self, conversiondict=None):
        """
        Read the header from a data file.

        The file pointer is reset to the beginning of the file before
        parse_header returns.

        :Arguments:
            conversiondict - (dict) A dictionary containing header key
                names as keys and a function as the value. The value
                corresponding to the header key name will be operated
                on by the specified function. This is used, e.g., to
                convert a header value to a float or int. Header keys that
                are not listed in conversiondict are attempted to be cast to
                floats, but if this fails then they are unmodified, i.e. the
                corresponding values are left as strings.

        :Returns:
            headerdict - (dict) A dictionary of key-value pairs parsed
                from the header.
        """
        # Make sure we read from the beginning of the file
        self.f.seek(0)

        hlen = 0
        nrows = 5
        headerdict = {}

        try:
            for i in range(nrows):
                line = self.f.readline()
                hlen += len(line)
                if ':' in line:
                    slist = line.split(';')[0].split(':', 1)
                    key = slist[0].split('#')[1].strip()
                    value = slist[1].strip()
                    try:
                        headerdict[key] = float(value)
                    except ValueError:
                        headerdict[key] = value
        finally:
            self.f.seek(0)

        headerdict['skiprows'] = nrows
        headerdict['headerlen'] = hlen

        return headerdict

    def _construct_header(self):
        """
        Construct the header dictionary from the header.
        """
        self.header = self._read_header()
        if 'columns' in self.header:
            cols = self.header['columns']
            self.header['columns'] = self._split_columns(cols)
        if 'navg' in self.header:
            if isinstance(self.header['navg'], StringTypes):
                navg = self._split_columns(self.header['navg'],
                                           typecast=float)
                self.header['navg'] = navg

        if 'columns' in self.header:
            self.numcols = len(self.header['columns'])
        else:
            self.numcols = 1

    def _skip_header(self):
        try:
        	self.f.seek(self.header['headerlen'])
        except ValueError:
        	self.f.seek(0)

    def close(self):
        self.f.close()

    def init_data(self, *args, **kwargs):
        self.header = self._read_header()
        self._construct_header()

        if self.f.closed:
            raise ValueError('I/O operation on closed file.')
        self.args = args
        self.kwargs = kwargs
        if 'columns' in self.header:
        	cols = self.header['columns']
        else:
        	cols = ('locked', 'adc')

        names = ('caret', cols[0], 'space', cols[1], 'crnl')
        formats = ('S1', '>u4', 'S1', '>u4', 'S2')
        dtdict = {'names': names, 'formats': formats}
        dt = np.dtype(dtdict)

        self._skip_header()
        data = np.fromfile(self.f, dt)

	    # Get rid of control columns and '*PSQUID: data_off' response row
        data = data[[names[1], names[3]]][:-2]


        if 'navg' in self.header:
            navg = self.header['navg']
            if isinstance(navg, float):
                navg = [navg]*self.numcols
        else:
            navg = [1.]*self.numcols


        for i, n in enumerate(navg):
            col = cols[i]
            data[col] = data[col]/n

        # Convert to floats
        dt2 = np.dtype({'names': cols, 'formats': ('f4', 'f4')})
        data = data.astype(dt2)

        # Convert to pandas dataframe
        data = pd.DataFrame(data)
        return data

    def update_data(self):
        args = self.args
        kwargs = self.kwargs
        return self.init_data(*args, **kwargs)

    def switch_file(self, f, *args, **kwargs):
        self.__init__(f)
        return self.init_data(*args, **kwargs)


class PSquidSweepReader(object):
    """
    Reader for binary-encoded data from the psquid sweep command.

    See pyoscope.readers.ReaderInterface for info on readers.

    Note that this reader is stupidly inefficient...
    """
    def __init__(self, f, header=True, *args, **kwargs):
        # Load file
        if isinstance(f, file) or isinstance(f, _TemporaryFileWrapper):
            mode = f.mode
            if ('r' in mode) or ('+' in mode):
                self.f = f
            else:
                self.f = open(f.name, 'r')
        elif isinstance(f, StringTypes):
            self.f = open(f, 'r')
        else:
            raise TypeError('f must be a file handle or filename.')
        self.f.seek(0)
        self.filename = self.f.name

        # Construct header if wanted
        if header:
            self._construct_header()
        else:
            self.header = {}

        # Seek to end of header
        self._skip_header()


    # @staticmethod
    # def _cfunc(val):
    #     return float(int(val, 16))

    @staticmethod
    def _split_columns(colstr, typecast=str):
        """
        "[rdac, adc]" or "rdac, adc" or "[rdac adc]" or "rdac adc"
        all go to -> ["rdac", "adc"]

        unless typecast is specified, in which case typecast() is called
        on every value in list
        """
        colstr = colstr.strip('[]')
        if ',' in colstr:
            splitchar = ','
        else:
            splitchar = ' '
        collist = colstr.split(splitchar)
        collist = [typecast(col.strip()) for col in collist]
        return collist

    def _read_header(self, conversiondict=None):
        """
        Read the header from a data file.

        The file pointer is reset to the beginning of the file before
        parse_header returns.

        :Arguments:
            conversiondict - (dict) A dictionary containing header key
                names as keys and a function as the value. The value
                corresponding to the header key name will be operated
                on by the specified function. This is used, e.g., to
                convert a header value to a float or int. Header keys that
                are not listed in conversiondict are attempted to be cast to
                floats, but if this fails then they are unmodified, i.e. the
                corresponding values are left as strings.

        :Returns:
            headerdict - (dict) A dictionary of key-value pairs parsed
                from the header.
        """
        # Make sure we read from the beginning of the file
        self.f.seek(0)

        hlen = 0
        nrows = 11
        headerdict = {}

        # Use try/finally block to guarantee that file is left in known
        # state
        try:
            for i in range(nrows):
                line = self.f.readline()
                hlen += len(line)
                if ':' in line:
                    slist = line.split(';')[0].split(':', 1)
                    key = slist[0].split('#')[1].strip()
                    value = slist[1].strip()
                    try:
                        headerdict[key] = float(value)
                    except ValueError:
                        headerdict[key] = value
        finally:
            self.f.seek(0)

        headerdict['skiprows'] = nrows
        headerdict['headerlen'] = hlen

        return headerdict

    def _construct_header(self):
        """
        Construct the header dictionary from the header.
        """
        self.header = self._read_header()
        if 'columns' in self.header:
            cols = self.header['columns']
            self.header['columns'] = self._split_columns(cols)
        if 'navg' in self.header:
            if isinstance(self.header['navg'], StringTypes):
                navg = self._split_columns(self.header['navg'],
                                           typecast=float)
                self.header['navg'] = navg

        if 'columns' in self.header:
            self.numcols = len(self.header['columns'])
        else:
            self.numcols = 1

    def _skip_header(self):
        try:
            self.f.seek(self.header['headerlen'])
        except ValueError:
            self.f.seek(0)

    def close(self):
        self.f.close()

    def init_data(self, *args, **kwargs):
        self.header = self._read_header()
        self._construct_header()

        if self.f.closed:
            raise ValueError('I/O operation on closed file.')
        self.args = args
        self.kwargs = kwargs
        if 'columns' in self.header:
            cols = self.header['columns']
        else:
            cols = ('swept', 'locked', 'adc')

        names = ('caret', cols[0], 'space', cols[1], 'space2',
                 cols[2], 'crnl')
        formats = ('S1', '>u2', 'S1', '>u4', 'S1', '>u4', 'S2')
        dtdict = {'names': names, 'formats': formats}
        dt = np.dtype(dtdict)

        self._skip_header()
        data = np.fromfile(self.f, dt)

        # Get rid of control columns and '*PSQUID: sweep_off' response row
        data = data[list(cols)][:-2]


        if 'navg' in self.header:
            navg = self.header['navg']
            if isinstance(navg, float):
                navg = [navg]*self.numcols
        else:
            navg = [1.]*self.numcols

        for i, n in enumerate(navg):
            col = cols[i]
            data[col] = data[col]/n

        # Convert to floats
        dt2 = np.dtype({'names': cols, 'formats': ('f4', 'f4', 'f4')})
        data = data.astype(dt2)

        # Convert to pandas dataframe
        data = pd.DataFrame(data)
        return data

    def update_data(self):
        args = self.args
        kwargs = self.kwargs
        return self.init_data(*args, **kwargs)

    def switch_file(self, f, *args, **kwargs):
        self.__init__(f)
        return self.init_data(*args, **kwargs)


class PSquidRawDataReader(object):
    """
    Reader for binary-encoded data from the psquid raw_data command.

    See pyoscope.readers.ReaderInterface for info on readers.

    Note that this reader is stupidly inefficient...
    """
    def __init__(self, f, header=True, *args, **kwargs):
        # Load file
        if isinstance(f, file) or isinstance(f, _TemporaryFileWrapper):
            mode = f.mode
            if ('r' in mode) or ('+' in mode):
                self.f = f
            else:
                self.f = open(f.name, 'r')
        elif isinstance(f, StringTypes):
            self.f = open(f, 'r')
        else:
            raise TypeError('f must be a file handle or filename.')
        self.f.seek(0)
        self.filename = self.f.name

        # Construct header if wanted
        if header:
            self._construct_header()
        else:
            self.header = {}

        # Seek to end of header
        self._skip_header()


    # @staticmethod
    # def _cfunc(val):
    #     return float(int(val, 16))

    @staticmethod
    def _split_columns(colstr, typecast=str):
        """
        "[rdac, adc]" or "rdac, adc" or "[rdac adc]" or "rdac adc"
        all go to -> ["rdac", "adc"]

        unless typecast is specified, in which case typecast() is called
        on every value in list
        """
        colstr = colstr.strip('[]')
        if ',' in colstr:
            splitchar = ','
        else:
            splitchar = ' '
        collist = colstr.split(splitchar)
        collist = [typecast(col.strip()) for col in collist]
        return collist

    def _read_header(self, conversiondict=None):
        """
        Read the header from a data file.

        The file pointer is reset to the beginning of the file before
        parse_header returns.

        :Arguments:
            conversiondict - (dict) A dictionary containing header key
                names as keys and a function as the value. The value
                corresponding to the header key name will be operated
                on by the specified function. This is used, e.g., to
                convert a header value to a float or int. Header keys that
                are not listed in conversiondict are attempted to be cast to
                floats, but if this fails then they are unmodified, i.e. the
                corresponding values are left as strings.

        :Returns:
            headerdict - (dict) A dictionary of key-value pairs parsed
                from the header.
        """
        # Make sure we read from the beginning of the file
        self.f.seek(0)

        hlen = 0
        nrows = 4
        headerdict = {}

        # Use try/finally block to guarantee that file is left in known
        # state
        try:
            for i in range(nrows):
                line = self.f.readline()
                hlen += len(line)
                if ':' in line:
                    slist = line.split(';')[0].split(':', 1)
                    key = slist[0].split('#')[1].strip()
                    value = slist[1].strip()
                    try:
                        headerdict[key] = float(value)
                    except ValueError:
                        headerdict[key] = value
        finally:
            self.f.seek(0)

        headerdict['skiprows'] = nrows
        headerdict['headerlen'] = hlen

        return headerdict

    def _construct_header(self):
        """
        Construct the header dictionary from the header.
        """
        self.header = self._read_header()
        if 'columns' in self.header:
            cols = self.header['columns']
            self.header['columns'] = self._split_columns(cols)

        if 'columns' in self.header:
            self.numcols = len(self.header['columns'])
        else:
            self.numcols = 1

    def _skip_header(self):
        try:
        	self.f.seek(self.header['headerlen'])
        except ValueError:
        	self.f.seek(0)

    def close(self):
        self.f.close()

    def init_data(self, *args, **kwargs):
        self.header = self._read_header()
        self._construct_header()

        if self.f.closed:
            raise ValueError('I/O operation on closed file.')
        self.args = args
        self.kwargs = kwargs
        if 'columns' in self.header:
        	cols = self.header['columns']
        else:
        	cols = ('locked',) if eval(self.header['locked']) else ('adc',)

        names = ('caret', cols[0])
        formats = ('S1', '>u2')
        dtdict = {'names': names, 'formats': formats}
        dt = np.dtype(dtdict)

        self._skip_header()
        data = np.fromfile(self.f, dt)

	    # Get rid of control columns and '*PSQUID: data_off' response row
        data = data[cols[0]][:-6]

        # Convert to floats
        dt2 = np.dtype({'names': cols, 'formats': ('f4')})
        data = data.astype(dt2)

        # Convert to pandas dataframe
        data = pd.DataFrame(data)
        return data

    def update_data(self):
        args = self.args
        kwargs = self.kwargs
        return self.init_data(*args, **kwargs)

    def switch_file(self, f, *args, **kwargs):
        self.__init__(f)
        return self.init_data(*args, **kwargs)