"""
Miscellaneous controls for PSquid.

Includes:
    StaticWrapText - class for autowrapping of static labels.
        Useful for making instruction text fields that automatically will
        resize themselves without the ugliness of a text control.

    DACsChoice - a wx.Choice with auto-populated fields corresponding to DACs
"""
import numpy as np
import os
import time
import wx
import random
from readers import HexReader


class StaticWrapText(wx.PyControl):
    def __init__(self, parent, id=wx.ID_ANY, label='', pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.NO_BORDER,
                 validator=wx.DefaultValidator, name='StaticWrapText'):
        wx.PyControl.__init__(self, parent, id, pos, size, style, validator,
                              name)
        self.statictext = wx.StaticText(self, wx.ID_ANY, label, style=style)
        self.wraplabel = label
        #self.wrap()

    def wrap(self):
        self.Freeze()
        self.statictext.SetLabel(self.wraplabel)
        self.statictext.Wrap(self.GetSize().width)
        self.Thaw()

    def DoGetBestSize(self):
        self.wrap()
        #print self.statictext.GetSize()
        self.SetSize(self.statictext.GetSize())
        return self.GetSize()


class DACsChoice(wx.Choice):
    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0, validator=wx.DefaultValidator,
                 name='DACsChoice', auto=False):
        dacs = ['Auto'] if auto else []
        dacs.extend(['Det Bias',
                     'S1 Bias (Addr/RS)',
                     'S1 FB',
                     'S2 Bias',
                     'S2 FB',
                     'S3 (SA) Bias',
                     'S3 (SA) FB',
                     'Offset',
                     'Gain'
                     ])

        wx.Choice.__init__(self, parent, id, pos, size, dacs, style,
                           validator, name)


class IntegerValidator(wx.PyValidator):
    """
    Validator that allows only integers.

    Defaults to allowing only non-negative integers.
    """
    def __init__(self, minval=0, maxval=None):
        wx.PyValidator.__init__(self)

        self.minval = minval
        self.maxval = maxval

        self.Bind(wx.EVT_CHAR, self.on_char)

    def Clone(self):
        return IntegerValidator(self.minval, self.maxval)

    def Validate(self, win):
        # Validate() is never called for some reason, so move all logic to
        # callback function
        pass

    def _validate(self, val, minval=None, maxval=None):
        if minval is None:
            minval = self.minval
        if maxval is None:
            maxval = self.maxval
        val = int(val)

        val = self._coerce(val, minval, maxval)
        return val

    def _coerce(self, val, minval, maxval):
        """
        Coerce a value to satisfy `min` <= `val` <= `max`.
        """
        if minval is not None:
            val = max(val, minval)
        if maxval is not None:
            val = min(val, maxval)
        return val

    def on_char(self, event):
        key = event.GetKeyCode()
        ctrl = event.GetEventObject()
        curval = ctrl.GetValue()

        allowed = '0123456789'

        if key == 13:
            newval = self._validate(curval)
            ctrl.SetValue(str(newval))
            ctrl.SetInsertionPoint(-1)
            event.Skip()
        elif key < wx.WXK_SPACE or key == wx.WXK_DELETE or key > 255:
            event.Skip()
        elif chr(key) in allowed:
            event.Skip()


class DACValidator(wx.PyValidator):
    """
    Validator for DAC inputs.

    This class ended up being poorly designed. Would be better if redesigned
    to always convert to index units and validate only index units, then
    optionally convert back to scaled units if desired.
    """
    def __init__(self, raw=True, rawrange=(0, 65535), scaledrange=(0., 1.),
                 num=65536, stepval=100, sig=2):
        wx.PyValidator.__init__(self)

        self.raw = raw
        self.rawrange = rawrange
        self.scaledrange = scaledrange
        self.num = num
        self.stepval = stepval
        self.sig = sig

        self.Bind(wx.EVT_CHAR, self.on_char)

    def Clone(self):
        return DACValidator(self.raw, self.rawrange, self.scaledrange,
                            self.num, self.stepval, self.sig)

    def Validate(self, win):
        # Validate() is never called for some reason, so move all logic to
        # callback function
        pass

    def _validate(self, val, minmax=None, num=None, sig=None, cb=None,
                  cbfunc=int):
        if minmax is None:
            minmax = self.rawrange if self.raw else self.scaledrange
        if num is None:
            num = self.num
        if sig is None:
            sig = self.sig
        val = float(val)
        minval, maxval = minmax

        val = self._coerce(val, minval, maxval)
        val = self._digitize(val, minval, maxval, num, sig)

        if cb is None:
            cb = self.raw
        if cb:
            val = cbfunc(val)

        return val

    def _coerce(self, val, minval, maxval):
        """
        Coerce a value to satisfy `min` <= `val` <= `max`.
        """
        val = max(val, minval)
        val = min(val, maxval)
        return val

    def _digitize(self, val, minval=None, maxval=None, num=None, sig=None):
        """
        Digitize a number to the nearest subdivision of the range.

        E.g. for `num` = 6 and `minval`, `maxval` = 1, 2, the allowed
        values are (1.0, 1.2, 1.4, 1.6, 1.8, 2.0), so a value of 1.55
        would be digitized to 1.6.

        `sig` is the number of effective sig figs to display, np.where the
        count is based on the `delta` = (`maxval` - `minval`)/`num`.

        E.g. for `minval`, `maxval` = 0, 10, and `num` = 10, then
        `delta` = 1.1111... and so for `sig` = 2, the significant digit place
        is 0.01, and `val` = 2.4 becomes 2.2.

        Similarly,
        >>> self._digitize(2.4, 0, 10, 10, sig=5)
        2.2222
        """
        if minval is None:
            ran = self.rawrange if self.raw else self.scaledrange
            minval = min(ran)
        if maxval is None:
            ran = self.rawrange if self.raw else self.scaledrange
            minval = max(ran)
        if num is None:
            num = self.num
        if sig is None:
            sig = self.sig
        val = float(val)

        minval = float(minval)  # This will force rest to float
        delta = (maxval - minval)/(num - 1)
        newval = round((val - minval)/delta)*delta + minval

        if sig:
            sig = int(sig)
            sigval = sig - int(np.floor(np.log10(delta))) - 1
            newval = round(newval, sigval)

        return newval

    def _index(self, val, minval=None, maxval=None, num=None):
        """
        Return the 0-based index of a digitized number.

        E.g.
        >>> self._index(2.2, 0, 10, 10)
        2
        """
        if minval is None:
            ran = self.rawrange if self.raw else self.scaledrange
            minval = min(ran)
        if maxval is None:
            ran = self.rawrange if self.raw else self.scaledrange
            maxval = max(ran)
        if num is None:
            num = self.num
        val = float(val)

        delta = (maxval - minval)/(num - 1)
        index = int(round((val - minval)/delta))
        return index

    def _value(self, index, minval=None, maxval=None, num=None, sig=None):
        """
        Return the value corresponding to the 0-based digitization index.

        E.g.
        >>> self._value(2, 0, 10, 10)
        2.2
        """
        if minval is None:
            ran = self.rawrange if self.raw else self.scaledrange
            minval = min(ran)
        if maxval is None:
            ran = self.rawrange if self.raw else self.scaledrange
            maxval = max(ran)
        if num is None:
            num = self.num
        if sig is None:
            sig = self.sig
        index = int(index)

        delta = (maxval - minval)/(num - 1)
        value = minval + delta*index
        value = self._digitize(value, minval, maxval, num, sig)
        return value

    def on_char(self, event):
        key = event.GetKeyCode()
        ctrl = event.GetEventObject()
        curval = ctrl.GetValue()
        selected = ctrl.GetSelection()

        allselected = (len(curval) == max(selected) - min(selected))
        nodot = ('.' not in curval) or allselected

        allowed = '0123456789'
        if nodot and (not self.raw):
            allowed = allowed + '.'

        if key == 13:  # <Enter>
            newval = self._validate(curval)
            ctrl.SetValue(str(newval))
            ctrl.SetInsertionPoint(-1)
            event.Skip()
        elif key == 315:  # <Up arrow>
            newval = self._validate(curval)
            index = self._index(newval) + self.stepval
            nextval = self._value(index)
            nextval = str(self._validate(nextval))
            ctrl.SetValue(nextval)
            ctrl.SetInsertionPoint(-1)
            event.Skip()
        elif key == 317:  # <Down arrow>
            newval = self._validate(curval)
            index = self._index(newval) - self.stepval
            nextval = self._value(index)
            nextval = str(self._validate(nextval))
            ctrl.SetValue(nextval)
            ctrl.SetInsertionPoint(-1)
            event.Skip()
        elif key < wx.WXK_SPACE or key == wx.WXK_DELETE or key > 255:
            event.Skip()
        elif chr(key) in allowed:
            event.Skip()


class LockController(object):
    """
    An Iterator object for organizing and controlling the data collection for
    the graphical locking task.

    `psquid` is the PSquid object that is controlling the PSquid board.

    `stepparams` is a dictionary of parameters for stepping through the bias
    voltages. Values are in raw units. Default values are:

        `stepparams` = {'dacindex': 0,  # Index of DAC to be stepped
                        'start': 0,     # Start value
                        'end': 65535,   # End value
                        'delta': 655,   # Step size
                        'settle': .01}  # Wait this many seconds after step

    `sweepparams` is a dictionary of parameters for stepping through the
    feedback voltages. Values are in raw units. Default values are:

        `sweepparams` = {'dacindex': 0, # Index of DAC to be stepped
                         'monindex': 0, # Index of DAC to be monitored
                         'start': 0,    # Start value
                         'end': 65535,  # End value
                         'delta': 655,  # Step size
                         'nsweeps': 1,  # Number of sweeps at each step
                         'navg': 167,   # N average at each sweep step
                         'nsettle': 50} # Settling time in cycles

    `locked` is a boolean indicating whether or not the ADC or monitored DAC
    should be the target for the lock. Currently this is ignored and you may
    not lock from an already locked state.

    An instance of LockController is an iterator. See below for usage. Note
    that the instance method close() should be called before discarding the
    LockController instance, since the temporary files it creates are not
    automatically scheduled for deletion.

    Usage:
        lc = LockController(psquid)
        for completion in lc:
            print completion
        lc.close()
    """
    def __init__(self, psquid, stepparams=None, sweepparams=None,
                 locked=False):
        if stepparams is None:
            stepparams = {'dacindex': 0,
                          'start': 0,
                          'end': 65535,
                          'delta': 655,
                          'settle': .01}
        if sweepparams is None:
            sweepparams = {'dacindex': 0,
                           'monindex': 0,
                           'start': 0,
                           'end': 65535,
                           'delta': 655,
                           'nsweeps': 1,
                           'navg': 167,
                           'nsettle': 50}

        self.psquid = psquid
        self.stepparams = stepparams
        self.sweepparams = sweepparams
        self.locked = locked

        ststart = self.stepparams['start']
        stend = self.stepparams['end']
        stdelta = self.stepparams['delta']
        stnumsteps = int(np.floor((stend - ststart)/stdelta)) + 1
        self.stepparams['num'] = stnumsteps
        self.stepparams['curval'] = ststart
        self.stepparams['curindex'] = 0

        swstart = self.sweepparams['start']
        swend = self.sweepparams['end']
        swdelta = self.sweepparams['delta']
        swnumsteps = int(np.floor((swend - swstart)/swdelta)) + 1
        self.sweepparams['num'] = swnumsteps

        self.files = []
        self.collecting = False
        self.collected = False
        self.completionfactor = 0.

        self.adcminmax = None
        self.dacminmax = None

    def __del__(self):
        self.close()

    def close(self):
        """
        Cleanly clean up after self. Mainly deletes temporary data files.
        """
        for fname in self.files:
            try:
                os.remove(fname)
            except OSError:
                pass

    def __iter__(self):
        return self

    def next(self):
        curindex = self.stepparams['curindex']
        curval = self.stepparams['curval']
        index = self.stepparams['dacindex']
        wait = self.stepparams['settle']
        delta = self.stepparams['delta']
        num = self.stepparams['num']

        # Reset PSquid to known state
        state = self.psquid.connection.state
        if state == 'data':
            self.psquid.data_off()
            time.sleep(0.2)
        elif state == 'sweep':
            self.psquid.sweep_off()
            time.sleep(0.2)

        # Set DAC and wait for it to settle
        self.psquid.dac(index, curval, raw=True)
        time.sleep(wait)

        # Start sweep and wait for it to end
        fname = self._sweep()
        while self.psquid.connection.state == 'sweep':
            time.sleep(0.01)

        r = HexReader(fname)
        data = r.init_data()
        mindac, maxdac, minadc, maxadc = self._get_minmaxes(data)
        self._update_minmax(mindac, maxdac, minadc, maxadc)
        r.close()

        # Update state
        curindex += 1
        curval += delta
        if curindex >= num:
            raise StopIteration  # Stop if completed all sweeps
        self.stepparams['curindex'] = curindex
        self.stepparams['curval'] = curval

        # Return completion factor (1. = 100%)
        self.completionfactor = float(curindex)/num
        return self.completionfactor

    def send(self, index):
        num = self.stepparams['num']
        if (index < 0) or (index > num - 1):
            raise ValueError('Invalid index sent to LockController.')
        self.stepparams['curindex'] = index
        self.stepparams['curval'] = self._value(index)

    def _sweep(self, sweepparams=None, i=None):
        """
        Execute and store the data for a sweep command.

        Data is stored in the `self.files` list.

        If `sweepparams` is None, uses the instance's `sweepparams`.

        If `i` is specified, inserts into the `self.files` list at the `i`th
        index, otherwise appends to the end.
        """
        if sweepparams is None:
            sweepparams = self.sweepparams

        ch = sweepparams['dacindex']
        read_ch = sweepparams['monindex']
        low = sweepparams['start']
        high = sweepparams['end']
        delta = sweepparams['delta']
        nsweeps = sweepparams['nsweeps']
        navg = sweepparams['navg']
        ntimer = sweepparams['nsettle']

        f = self.psquid.sweep(ch=ch,
                              read_ch=read_ch,
                              low=low,
                              high=high,
                              delta=delta,
                              navg=navg,
                              ntimer=ntimer,
                              nsweeps=nsweeps,
                              raw=True,
                              delete=False)
        # toprint = ("{ch} {read_ch} {low} {high} {delta} {navg} {ntimer}"
        #            " {nsweeps}") #DELME
        # print toprint.format(ch=ch,
        #                      read_ch=read_ch,
        #                      low=low,
        #                      high=high,
        #                      delta=delta,
        #                      navg=navg,
        #                      ntimer=ntimer,
        #                      nsweeps=nsweeps,
        #                      raw=True) #DELME
        fname = f.name
        # fname = 'howdy ho!' + str(random.random()) #DELME

        if i is None:
            self.files.append(fname)
        else:
            self.files[i] = fname
        return fname

    def _value(self, index, which='step'):
        """
        Returns the value associated with the specified index.
        """
        if which == 'step':
            params = self.stepparams
        elif which == 'sweep':
            params = self.sweepparams

        minval = params['start']
        delta = params['delta']
        value = minval + delta*index
        return value

    def _index(self, value, which='step'):
        """
        Returns the nearest index associated with the specified value.
        """
        if which == 'step':
            params = self.stepparams
        elif which == 'sweep':
            params = self.sweepparams

        minval = params['start']
        delta = params['delta']
        index = int(round((float(value) - minval)/delta))
        return index

    def _get_minmaxes(self, data):
        """
        Return the [xmin, xmax] for each of the 'adc' and 'locked' data sets
        in `data`.

        Returns the tuple (xmin_locked, xmax_locked, xmin_adc, xmax_adc)
        """
        xminlocked, xminadc = data.min()[['locked', 'adc']]
        xmaxlocked, xmaxadc = data.max()[['locked', 'adc']]
        return xminlocked, xmaxlocked, xminadc, xmaxadc

    def _update_minmax(self, mindac, maxdac, minadc, maxadc):
        """
        Compare the specified values to the stored instance values and keeps
        the more extreme of the pairs.
        """
        if (self.dacminmax is None) or (self.adcminmax is None):
            newmindac, newmaxdac = mindac, maxdac
            newminadc, newmaxadc = minadc, maxadc
        else:
            newmindac = min(mindac, self.dacminmax[0])
            newmaxdac = max(maxdac, self.dacminmax[1])

            newminadc = min(minadc, self.adcminmax[0])
            newmaxadc = max(maxadc, self.adcminmax[1])

        self.dacminmax = newmindac, newmaxdac
        self.adcminmax = newminadc, newmaxadc


def make_gbsizer_from_array(parent, array, styles=None,
                            vertmargin=0, vminsize=10,
                            horizmargin=False, hminsize=10,
                            hgap=3, vgap=3):
    """
    Creates a wx.GridBagSizer with the correct shape and the elements
    populated according to the items in the 2-dimensional array `array`.

    `styles` is the style that should be passed to the GBS.Add() method. If
    None, uses wx.EXPAND. Uses the top-left-most style for controls than span
    more than one cell.

    If `vertmargin` is not False, creates extra empty columns on the left and
    right of the GridBagSizer. If `horizmargin` is True, creates extra empty
    rows on the top and bottom of the GridBagSizer. The spacers are added to
    the specified row (`vertmargin`) or column (`horizmargin`). Make sure
    nothing is obstructing them! The minimum size of these margins is
    specified in pixels by `vminsize` and `hminsize` for vertical and
    horizontal margins, respectively.

    `hgap` and `vgap` are arguments to wx.GridBagSizer.

    Horizontal lines (wx.StaticLine) may additionally be specified with
    'hline'. Note that lines will extend the full distance of the GBS.

    Veritcal lines (wx.StaticLine) may additionally be specified with 'vline'.
    Note that lines will extend the full distance of the GBS.

    Spacers may be specified by tuples, (width, height).
    """
    coffset = 1 if (vertmargin is not False) else 0
    roffset = 1 if (horizmargin is not False) else 0

    if styles is None:
        styles = np.zeros_like(array, dtype='int') + wx.EXPAND

    arr = np.array(array, dtype='object')
    rlen, clen = arr.shape
    gbs = wx.GridBagSizer(hgap, vgap)

    flat = [item for sublist in array for item in sublist]

    # Get only items that are wx items
    toset = []
    for item in flat:
        if isinstance(item, wx.Object):
            toset.append(item)

    ctrlset = list(set(toset))  # Get unique list of these items

    # Add controls to GridBagSizer
    for i, ctrl in enumerate(ctrlset):
        rs, cs = np.where(arr == ctrl)
        rmin, rmax = min(rs), max(rs)
        cmin, cmax = min(cs), max(cs)
        dr = rmax - rmin + 1
        dc = cmax - cmin + 1

        pos = (rmin + roffset, cmin + coffset)
        span = (dr, dc)

        if isinstance(ctrl, wx.StaticLine):
            vert = ctrl.IsVertical()
            if vert:
                pos = (0, cmin + coffset)
                span = (rlen + roffset*2, dc)
            else:
                pos = (rmin + roffset, 0)
                span = (dr, clen + coffset*2)

        style = styles[rmin, cmin]
        if style is None:
            style = wx.EXPAND
        gbs.Add(ctrl, pos, span, flag=style)

    # Add hlines
    rs, cs = np.where(arr == 'hline')
    rset = set(rs)
    for r in rset:
        cset = cs[rs == r]
        cmin, cmax = min(cset), max(cset)

        pos = (r + roffset, 0)
        span = (1, clen + coffset*2)

        style = styles[rmin, cmin]
        if style is None:
            style = wx.EXPAND
        ctrl = wx.StaticLine(parent, size=(-1, 2), style=wx.LI_HORIZONTAL)
        gbs.Add(ctrl, pos, span, flag=style)

    # Add vlines
    rs, cs = np.where(arr == 'vline')
    cset = set(cs)
    for c in cset:
        rset = rs[cs == c]
        rmin, rmax = min(rset), max(rset)

        pos = (0, cmin + coffset)
        span = (rlen + roffset*2, 1)

        style = styles[rmin, cmin]
        if style is None:
            style = wx.EXPAND
        ctrl = wx.StaticLine(parent, size=(2, -1), style=wx.LI_VERTICAL)
        gbs.Add(ctrl, pos, span, flag=style)

    # Find and add spacers (tuples)
    tuples = []
    poss = []
    for i in range(rlen):
        for j in range(clen):
            elem = arr[i, j]
            if isinstance(elem, tuple):
                tuples.append(elem)
                poss.append((i + roffset, j + coffset))

    for i, ctrl in enumerate(tuples):
        pos = poss[i]
        gbs.Add(ctrl, pos, flag=wx.EXPAND)

    # Add margin spacers
    if (vertmargin is not False):
        gbs.Add((vminsize, -1), (vertmargin, 0), flag=0)
        gbs.Add((vminsize, -1), (vertmargin, clen + coffset), flag=0)
    if (horizmargin is not False):
        gbs.Add((-1, hminsize), (0, horizmargin), flag=0)
        gbs.Add((-1, hminsize), (rlen + roffset, horizmargin), flag=0)

    return gbs
















