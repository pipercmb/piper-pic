"""
The DAC control panel for PSquid.
"""
import wx
from bindings import DACValidator


class DACPanel(wx.Panel):
    """
    The DAC control panel for PSquid.
    """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)

        self.fmf = self.GetTopLevelParent()
        self.timer = wx.Timer(self)
        self.timer.Start(50)  # Send timer event every 50 ms

        self.create_sizers()
        self.populate_top()

    def create_sizers(self):
        """
        Create the sizers in the panel.
        """
        self.bsMain = wx.BoxSizer(wx.VERTICAL)

        self.sbTop = wx.StaticBox(self, label='DACs')
        self.sbsTop = wx.StaticBoxSizer(self.sbTop, wx.HORIZONTAL)
        # self.bsTop = wx.BoxSizer(wx.HORIZONTAL)

        self.bsMain.Add(self.sbsTop, 1, wx.EXPAND)

        self.SetSizer(self.bsMain)

    def populate_top(self, sizer=None):
        """
        Populate the top sizer.
        """
        if sizer is None:
            sizer = self.sbsTop

        bs = wx.GridBagSizer(3, 1)
        self.bsDacs = bs
        sizer.Add(bs, 1, wx.EXPAND)

        dacs = ['Det Bias',
                'S1 Bias (RS)',
                'S1 FB',
                'S2 Bias',
                'S2 FB',
                'S3 (SA) Bias',
                'S3 (SA) FB',
                'Offset',
                'Gain'
                ]

        lblfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        txtfont = wx.Font(12, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
        txtsize = wx.Size(70, 17)
        self.lblDacs = []
        self.txtDacs = []
        self.sldDacs = []

        for i, dac in enumerate(dacs):
            lbl = wx.StaticText(self, wx.ID_ANY, label=dac,
                                style=wx.ALIGN_RIGHT)
            txt = wx.TextCtrl(self, wx.ID_ANY, size=txtsize)
            # txt = wx.TextCtrl(self, wx.ID_ANY, size=txtsize, validator=DACValidator())
            sld = wx.Slider(self, wx.ID_ANY, minValue=0, maxValue=65535,
                            size=(-1, 5))
            # sld = wx.Slider(self, wx.ID_ANY, minValue=0, maxValue=65535,
            #                 size=(-1, 5), validator=DACValidator())
            sld.SetPageSize(100)
            sld.SetLineSize(1)

            lbl.SetFont(lblfont)
            txt.SetFont(txtfont)

            self.lblDacs.append(lbl)
            self.txtDacs.append(txt)
            self.sldDacs.append(sld)

            row = i*2
            bs.Add(lbl, (row, 0), flag=wx.EXPAND | wx.BOTTOM | wx.RIGHT)
            bs.Add(sld, (row, 1), (1, 1), flag=wx.EXPAND | wx.TOP)
            bs.Add(txt, (row, 2), flag=wx.EXPAND | wx.BOTTOM | wx.LEFT)
            if i != len(dacs) - 1:
                line = wx.StaticLine(self, size=(-1, 1))
                bs.Add(line, (row+1, 0), (1, 3), flag=wx.EXPAND)

        bs.AddGrowableCol(1, 1)


# class DACPanel(wx.Panel):
#     """
#     The DAC control panel for PSquid.
#     """
#     def __init__(self, parent):
#         wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY)

#         self.fmf = self.GetTopLevelParent()

#         self.create_sizers()
#         self.populate_top()

#     def create_sizers(self):
#         """
#         Create the sizers in the panel.
#         """
#         self.bsMain = wx.BoxSizer(wx.VERTICAL)

#         self.sbTop = wx.StaticBox(self, label='DACs')
#         self.sbsTop = wx.StaticBoxSizer(self.sbTop, wx.VERTICAL)

#         self.bsMain.Add(self.sbsTop, 1, wx.EXPAND)

#         self.SetSizer(self.bsMain)

#     def populate_top(self, sizer=None):
#         """
#         Populate the top sizer.
#         """
#         if sizer is None:
#             sizer = self.sbsTop

#         dacs = ['Det Bias',
#                 'S1 Bias (Add/RS)',
#                 'S1 FB',
#                 'S2 Bias',
#                 'S2 FB',
#                 'S3 (SA) Bias',
#                 'S3 (SA) FB',
#                 'Offset',
#                 'Gain'
#                 ]

#         self.bsDacs = []
#         self.lblDacs = []
#         self.txtDacs = []
#         self.sldDacs = []
#         for dac in dacs:
#             bsDac, lblDac, txtDac, sldDac = self.make_txt_slider(self, dac)
#             sizer.Add(bsDac, 1, wx.EXPAND)

#     def make_labeled_textctrl(self, parent, label, orientation=wx.HORIZONTAL,
#                               validator=None, size=600):
#         """
#         Makes a text control with an adjacent static text label.

#         Orientation sets whether the label should be to the
#         left (wx.HORIZONTAL, default) or above (wx.VERTICAL).
#         """
#         lblLabel = wx.StaticText(parent, wx.ID_ANY, label=label)
#         if validator is None:
#             txtControl = wx.TextCtrl(parent, wx.ID_ANY, size=(size, -1))
#         else:
#             txtControl = wx.TextCtrl(parent, wx.ID_ANY, validator=validator,
#                                      size=(size, -1))

#         bs = wx.BoxSizer(orientation)
#         bs.Add(lblLabel, 0, wx.EXPAND | wx.RIGHT)
#         bs.Add(txtControl, 1, wx.EXPAND | wx.LEFT)

#         return bs, lblLabel, txtControl

#     def make_txt_slider(self, parent, label, validator=None, min=0., max=1.):
#         """
#         Makes a text control with associated slider and label.
#         """
#         lblLabel = wx.StaticText(parent, wx.ID_ANY, label=label)

#         if validator is None:
#             txtControl = wx.TextCtrl(parent, wx.ID_ANY)
#             sldControl = wx.Slider(parent, wx.ID_ANY, minValue=min,
#                                    maxValue=max)
#         else:
#             txtControl = wx.TextCtrl(parent, wx.ID_ANY, validator=validator)
#             sldControl = wx.Slider(parent, wx.ID_ANY, minValue=min,
#                                    maxValue=max, validator=validator)

#         bsLabel = wx.BoxSizer(wx.HORIZONTAL)
#         bsControl = wx.BoxSizer(wx.VERTICAL)

#         bsLabel.Add(lblLabel, 0, wx.EXPAND | wx.RIGHT)
#         bsLabel.Add(txtControl, 0, wx.EXPAND | wx.LEFT)

#         bsControl.Add(bsLabel, 1, wx.EXPAND | wx.BOTTOM)
#         bsControl.Add(sldControl, 1, wx.EXPAND | wx.TOP)

#         return bsControl, lblLabel, txtControl, sldControl
