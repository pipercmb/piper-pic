#!/bin/env python

"""
psquiderrors.py
jlazear
2013-06-24

Base error class for the PSquid project.
"""
version = 20130624
releasestatus = 'beta'


class PSquidError(Exception):
    """
    PSquid base exception class.
    """
    pass
