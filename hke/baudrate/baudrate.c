/*TAKEN FROM PSYNCADC CODE, MAY BE NONSENSICAL ERRORS!*/
#include <p30F5011.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <spi.h>
#include <stdio.h>
#include <ports.h>
#include <libpic30.h>

#include "baudrate.h"
#include "../common/ptty.h"

/* Device configuration register macros for building the hex file */
_FOSC(CSW_FSCM_OFF & EC_PLL16); /* External clock with 16xPLL oscillator, Failsafe clock off */
_FWDT(WDT_OFF); /* Watchdog timer disabled */
_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
_FGS(CODE_PROT_OFF); /* Code protect disabled */

void init_pic()
{
    // Set up values for GPIO pins
    TRISBbits.TRISB5 = 0; // LED_1 (LOCK)
    TRISBbits.TRISB4 = 0; // LED_2 (SWEEP)
    TRISBbits.TRISB3 = 0; // LED_3 (MISC/DATA)

    TRISGbits.TRISG2 = 0; // ZAP

    // Set up the output pins for GPIO
    TRISCbits.TRISC1 = 0; // Front panel LED
    TRISBbits.TRISB8 = 0; // S1 bias DAC /CS
    TRISBbits.TRISB9 = 0; // S2 bias DAC /CS
    TRISBbits.TRISB10 = 0; // S3 bias DAC /CS
    TRISBbits.TRISB11 = 0; // Det bias DAC /CS
    TRISBbits.TRISB12 = 0; // S1 FB DAC /CS
    TRISBbits.TRISB13 = 0; // S2 FB DAC /CS
    TRISBbits.TRISB14 = 0; // S3 FB DAC /CS
    TRISBbits.TRISB15 = 0; // S3 output pre-amp offset DAC /CS
    TRISGbits.TRISG3 = 0; // S3 output adjustable gain DAC /CS
    TRISDbits.TRISD8 = 0; // Address select MUX LSB
    TRISDbits.TRISD9 = 0; // Address select MUX middle bit
    TRISDbits.TRISD10 = 0; // Address select MUX MSB

    // Set all ADC lines as digital IO (Family Ref 11.3)
    ADPCFG = 0xFFFF;

    // Oscope debug pins, set them to outputs
    TRISDbits.TRISD5 = 0;
    TRISDbits.TRISD4 = 0;
    TRISDbits.TRISD3 = 0;
    TRISDbits.TRISD2 = 0;
    PIN_OSCOPE1 = 0;
    PIN_OSCOPE2 = 0;
    PIN_OSCOPE3 = 0;
    PIN_OSCOPE4 = 0;

    // Short delay to wait for power supply voltages to settle
    __delay32((long) (0.01 * CLOCKFREQ));

    // Setup the UART - 115.2 kbs
    unsigned U2MODEvalue = UART_EN & UART_IDLE_CON & UART_EN_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD & UART_NO_PAR_8BIT & UART_1STOPBIT;
    unsigned U2STAvalue = UART_INT_TX & UART_TX_PIN_NORMAL & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR;
    OpenUART2(U2MODEvalue, U2STAvalue, BRG_SETTING);
    ConfigIntUART2(UART_RX_INT_DIS & UART_TX_INT_DIS); // Disable TX and RX interrups
}

int main(void)
{
    int tick = 48;
    PIN_ZAP = 0; // Make sure zap is disabled
    init_pic(); // Setup the PIC internal peripherals

    PIN_OSCOPE3 = 1; //DEBUG
    PIN_OSCOPE3 = 0; //DEBUG

    // Repeatedly send '0123456789' with short wait between '9' and '0'.
    while (1) {
        if (tick >= 58) {
            tick = 48;
            __delay32((long)1000);
            }
        TTYPutcDirect(tick);
        tick++;
    }

}

void OnIdle(void)
{
    // pass
}