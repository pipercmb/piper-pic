//---------> Some useful stuff
#define BAUD_RATE		230400	// Standard baud rate of 115.2 kbs
#define CLOCKFREQ 20000000L		// 20.000 MHz instruction clock (synthesised from 5 MHz crystal, 16x PLL)
#define BRG_SETTING	0 //((int)((CLOCKFREQ/(BAUD_RATE*16.0)) - 1.00 + 0.5 ))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Adds 0.5 to convert from float to integer correctly
#define NUM_DACS    9
#define WBTYPE_UINT16 1
#define WBTYPE_UINT32 2
#define WBTYPE_STOP   -1
#define WBTYPE_NONE 0

//-----> OUTPUT PINS
#define PIN_LED_LOCK				LATBbits.LATB5	 // Front pannel LED
#define PIN_LED_SWEEP				LATBbits.LATB4	 // Front pannel LED
#define PIN_LED_MISC				LATBbits.LATB3	 // Front pannel LED
#define PIN_DET_BIAS                    LATBbits.LATB8   // Detector bias DAC chip select line

#define PIN_S1_BIAS                     LATBbits.LATB9   // Stage 1 SQUID bias DAC active-low chip select line
                                                         // aka S1 bias DAC /CS
                                                         // Note: Stage 1 = closest to detector
#define PIN_S1_FB                       LATBbits.LATB10  // S1 feedback (FB) DAC /CS

#define PIN_S2_BIAS                     LATBbits.LATB11  // S2 bias DAC /CS
#define PIN_S2_FB                       LATBbits.LATB12  // S2 FB DAC /CS
                                                         // Note: Stage 3 = series array
#define PIN_S3_BIAS                     LATBbits.LATB13  // S3 bias DAC /CS
#define PIN_S3_FB                       LATBbits.LATB14  // S3 FB DAC /CS
#define PIN_S3_OFFSET                   LATBbits.LATB15  // S3 output pre-amp offset DAC /CS
#define PIN_S3_GAIN_DAC                 LATGbits.LATG3   // S3 output adjustable gain DAC /CS

#define PIN_MUX_A0                      LATDbits.LATD8   // Address select MUX LSB
#define PIN_MUX_A1                      LATDbits.LATD9  // Address select MUX middle bit
#define PIN_MUX_A2                      LATDbits.LATD10  // Address select MUX MSB

#define PIN_ZAP                         LATGbits.LATG2   // ZAP button

//#define PIN_PIC_READY_BAR               LATBbits.LATB5	// Drive low to re-enable frame count capture circuit
//#define PIN_A0				LATBbits.LATB11	// ADC input mux
//#define PIN_A1				LATBbits.LATB12	// ADC input mux
//#define PIN_A2				LATBbits.LATB13	// ADC input mux
//#define PIN_A3				LATBbits.LATB14	// ADC input mux
//#define PIN_A4				LATBbits.LATB15	// ADC input mux
//#define PIN_FRAME			LATBbits.LATB10 // FRAME test point output
//#define PIN_TESTPOINT1                  LATBbits.LATB1  // test point 1 output
//#define PIN_TESTPOINT2                  LATBbits.LATB0  // test point 2 output
//#define PIN_DAC_SYNC                    LATFbits.LATF1	// DAC Sync BAR pin
//#define PIN_LOAD_DAC                    LATFbits.LATF0	// LDAC BAR pin
//
//// Diagnostic outputs
#define PIN_OSCOPE1			LATDbits.LATD5	// Debug pins for looking at with scope
#define PIN_OSCOPE2			LATDbits.LATD4
#define PIN_OSCOPE3			LATDbits.LATD3
#define PIN_OSCOPE4			LATDbits.LATD2

//
////------> INPUT PINS
//#define PIN_JP1_1			PORTDbits.RD3
//#define PIN_JP1_2			PORTDbits.RD2
//#define PIN_JP1_3			PORTDbits.RD1
//#define PIN_CONVERT			PORTDbits.RD0	// For READING the ADC Convert pin (output is from OC1 hardware)
//#define PIN_GATE			PORTDbits.RD8	// For READING Gate pin (normally triggers INT1)


//---------> Function Prototypes
// psquid.c
void init_pic( void );		// Init the PIC ports and hardware

// process_cmd.c
void process_cmd( char *cmd );

// do_cmd.c
void write_mux( unsigned short ch );

// cmd_gets.c
void cmd_gets( char *str, int n );

// dacs.c
void write_dac(unsigned val, int ch);

// int_handler.c
void set_dac(unsigned val, int ch);
void set_sweep(int ch, unsigned char read_ch, unsigned int start,
               unsigned int end, long delta, unsigned int dwell,
               unsigned int ntimer, unsigned int nsweeps);
unsigned int get_adc_value(void);
void set_pid(int ch, unsigned p, unsigned i, unsigned setpoint,
             unsigned char i_shift, unsigned char p_shift);
void set_pid_setpoint(unsigned setpoint);
void set_pid_ch(int ch);
void set_pid_p(unsigned p);
void set_pid_i(unsigned i);
void set_pid_i_shift(unsigned i_shift);
void set_pid_p_shift(unsigned p_shift);
void set_data(int ch, unsigned int dwell);
int get_pid_ch(void);
unsigned int get_pid_s(void);
unsigned int get_pid_p(void);
unsigned int get_pid_i(void);
unsigned char get_pid_p_shift(void);
unsigned char get_pid_i_shift(void);
unsigned char get_silent_flag(void);