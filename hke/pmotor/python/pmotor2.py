#!/bin/env python

"""
pmotor2.py
jlazear
2020-01-13

One-line description

Long description

Example:

<example code here>
"""
version = 20200113
releasestatus = 'beta'

import serial
import time

from pmotorreaders import PMotorDataReader as PMDR
from cp import Controller, command, argument, reader

@Controller
class PMotor(object):
    ser = None
    cont = False

    @command
    def set_serial_params(self, port='/dev/tty.usbserial-DA00E3US',
                          baudrate=417000):
        self.port = str(port)
        self.baudrate = int(baudrate)

    @command
    def send(self, tosend, eol='\r'):
        tosend = tosend + eol
        print "tosend = ", repr(tosend) #DELME
        if self.ser is None:
            try:
                ser = serial.Serial(port=self.port, baudrate=self.baudrate)
                ser.write(tosend)
            finally:
                try:
                    ser.close()
                except NameError:
                    pass
        else:
            self.ser.write(tosend)

    @command
    def send_amplitude(self, amplitude):
        astr = str(amplitude)[:4]
        astr = 'a {0}'.format(astr)
        self.send(astr)

    def _loop(self):
        while self.cont:
            try:
                self._write_current_data()
            except:
                self.cont = False
                raise

    def _write_current_data(self):
        num = self.ser.inWaiting()
        written = 0
        while num != 0:
            towrite = ser.read(num)
            written += num
            self.f.write(towrite)
            num = ser.inWaiting()
        return written

    @command
    def send_data_on(self, fname='data.txt', mode='ab'):
        self.ser = serial.Serial(port=self.port, baudrate=self.baudrate)
        self.f = open(fname, mode)
        self.send('data on')
        # self._loop()
        # ser.write('data on\r\n')

    @command
    def send_data_off(ser):
        self.send('data off')

    def _write_current_data(self, f):
        num = self.ser.inWaiting()
        written = 0
        while num != 0:
            towrite = self.ser.read(num)
            written += num
            f.write(towrite)
            num = self.ser.inWaiting()
        return written

    @command
    def loop(amplitude, fname='data.txt', init_wait=.1, wait=.01, timeout=5.):
        self.send_data_on()
        self.ser.flushInput()
        self.ser.flushOutput()
        time.sleep(init_wait)
        self.send_amplitude(amplitude)
        t0 = time.time()
        tc = t0 + 0.
        i = 0
        try:
            while ((tc - t0) < timeout) and (i < 1000):
                num = self._write_current_data(f)
                time.sleep(wait)
                tc = time.time()
                i += 1
        except Exception as e:
            raise e
        finally:
            print "i_final = ", i #DELME
            print "Cleaning up..."
            self.send_data_off()
            time.sleep(.1)
            num = self._write_current_data(ser, f)
            print "FINAL", num
            print "Data off..."
            self.ser.close()
            print "ser closed..."
            f.close()
            print "f closed..."
            print "Cleaned up."
        return fname

    @command
    def loop_cmd(cmd, fname='data.txt', init_wait=.1, wait=.01, timeout=5.):
        self.send_data_on()
        self.ser.flushInput()
        self.ser.flushOutput()
        time.sleep(init_wait)
        self.send(cmd)
        t0 = time.time()
        tc = t0 + 0.
        i = 0
        try:
            while ((tc - t0) < timeout) and (i < 1000):
                num = self._write_current_data(f)
                time.sleep(wait)
                tc = time.time()
                i += 1
        except Exception as e:
            raise e
        finally:
            print "i_final = ", i #DELME
            print "Cleaning up..."
            self.send_data_off()
            time.sleep(.1)
            num = self._write_current_data(ser, f)
            print "FINAL", num
            print "Data off..."
            self.ser.close()
            print "ser closed..."
            f.close()
            print "f closed..."
            print "Cleaned up."
        return fname
