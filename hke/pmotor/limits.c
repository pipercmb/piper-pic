#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <libpic30.h>

#include "pmotor.h"

struct limit_parameters {
    int active;     // 0 = not doing trapezoid slew; 1 = doing trapezoid ramp
    int pot_high;   // High value limit of the pot
    int pot_low;    // Low value limit of the angle pot
    int hysteresis;

    // State variables
    int prev_state;  // Previous state of the limit switches
};

static struct limit_parameters p = {
    .active = 0,
    .pot_low = -4000,
    .pot_high = 20000,
    .hysteresis = 100,

    // State variables
    .prev_state = LIMIT_UNDEFINED
};


// Check the angle-pot and the
int check_limits(int pot)
{
    if( p.active == 0 ) {
        return LIMIT_IN_RANGE;
    }

    // Hysteresis
    int high = (p.prev_state == LIMIT_EXCEED_HIGH) ?(p.pot_high - p.hysteresis) :p.pot_high;
    int low = (p.prev_state == LIMIT_EXCEED_LOW) ?(p.pot_low + p.hysteresis) :p.pot_low;

    int state = LIMIT_IN_RANGE;     // default
    if( pot >= high || !PIN_LIMIT_B)
        state = LIMIT_EXCEED_HIGH;
    else if( pot <= low || !PIN_LIMIT_A)
        state = LIMIT_EXCEED_LOW;

    p.prev_state = state;

    return state;
}

void set_limits(int active){
    p.active=active;
}
int get_limits(void)
{
    return p.active;
}

int get_limit_state(void){
    return p.prev_state;
}
