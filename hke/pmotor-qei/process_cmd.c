#include <p30F6015.h>
#include <string.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>

#include "pmotor-qei.h"
#include "version.h"        // Computer-generated version string (includes compile time and user name)


// Local function prototypes
static char do_cmd_led( void );		// turn LED on or off
static char do_cmd_addr( void );	// read hex address
static char do_cmd_echo( void );	// changes the echo mode of the board
static char do_cmd_data( void );	// turn data stream on or off
static char do_cmd_p( void );		// prints dac status
static char do_cmd_version( void );	// prints version of the code

//QuadEnc board commands
//"p" command - prints current position, velocity, direction
//"data" on/off
//"echo" on/off
//"led" on/off
//possible future commands:
//"swap" phase A & B inputs
//change number of encoder pulses per revolution (for different motors/encoders)
//change for x4 to x2 POSCNT mode
//"reset" - reboot board with new parameters

//Diagnostic commands
/*
led on/off/auto
echo on/off
data on/off
addr

"p" command
        pos_counter =
        delta_pos =
        rev_counter =
        delta_rev =
        direction = QEICONbits.UPDN =
        POSCNT =
        MAXCNT =
        QEICON =
        DFLTCON =

"set" POSCNT

"set" MAXCNT

"set" pos_counter
"set" rev_counter

"reset" delta_pos
"reset" delta_rev
*/

//static char do_cmd_version( void );	// prints version of the code

//#define CMP	!strcmp

/*
 * Process a user command contained in the command buffer.
 */
void process_cmd( char *cmd_string )
{
	char *s;
	unsigned char r;
	
    // Parse command string and check address to see if it's for us
    s = bp_parse_cmd_string(cmd_string);
    if( s == NULL )     // Command is not for us
        return;
	
	if( !strcmp( s, "led" ))		r = do_cmd_led();		// set the LED
	else if( !strcmp( s, "addr" ))	r = do_cmd_addr();		// read in hex address
	else if( !strcmp( s, "echo" ))	r = do_cmd_echo();		// Sets echo on or off
    else if( !strcmp( s, "data" ))	r = do_cmd_data();		// turn data stream on or off
	else if( !strcmp( s, "p" ))		r = do_cmd_p();			// prints dac status
    else if( !strcmp( s, "ver"))	r = do_cmd_version();	// prints software version
    
	else 						r = STATUS_FAIL_CMD;	// User typed unknown command

	// Print out the status, usually OK!
	print_status( r );
}

/*-----------------------------------------------------------
 * Standard Commands
 *-------------------------------------------------------------*/
/** LED
 *	Set the green front panel LED light.
 *
 *	Usage:
 *	led on	-> Sets LED on
 *	led off	-> Sets LED off
 *      led auto -> Sets LED auto (1Hz frame)
 */
static char do_cmd_led( void )
{
    char *arg;

    // Check that the next token is a valid number
    if( !tok_available() )
        return STATUS_FAIL_NARGS;
    
    arg = tok_get_str();

    if( !strcmp( arg, "on" ) )
    {
        set_LED(1); // turn LED on
        //PIN_CHOP_CLK_PIC = 1;
        //PIN_LED = 1;
    }
    else if( !strcmp( arg, "off" ) )
    {
        set_LED(0); // turn LED off
        //PIN_CHOP_CLK_PIC = 0;
        //PIN_LED = 0;
    }
    else if( !strcmp( arg, "auto" ) )
    {
        set_LED(2); // set LED
        //PIN_CHOP_CLK_PIC = 0;
        //PIN_LED = 0;
    }
    else
        return STATUS_FAIL_INVALID;

    return STATUS_OK;
}

/** ADDR
 *	Prints hex address of the board.
 *
 *	Usage:	
 *	No arguments
 */
static char do_cmd_addr( void )
{
    extern unsigned char CardAddress;
	char str[16];
	sprintf( str, "Address = %X\r\n", CardAddress );
	TTYPuts( str );
	return STATUS_OK;
}

/** ECHO
 *	Sets echo on or off.
 *
 *	Usage:
 *	echo on		-> sets echo on
 *	echo off	-> sets echo off
 */
static char do_cmd_echo( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( !strcmp( arg, "on" ) )
		cmd_set_echo( 1 );
	else if( !strcmp( arg, "off" ) )
		cmd_set_echo( 0 );
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;

}

/** DATA
 *	Turns on/off data stream.
 *
 *	Usage:
 *	data on		-> Turns data stream on
 *	data off	-> Turns data stream off
 */
static char do_cmd_data( void )
{
	char *arg;
	
	// Check that the next token is available
	if( !tok_available() )
		return STATUS_FAIL_NARGS;
	arg = tok_get_str();
	if( !strcmp( arg, "on" ) )
		set_data_frame(1); // turn data stream on
	else if( !strcmp( arg, "off" ) )
		set_data_frame(0); // turn data stream off
	else
		return STATUS_FAIL_INVALID;
	
	return STATUS_OK;
}

/** P
 *	Prints a table of data for specified channels:
 *	GDAC, ADAC, Demod, Delta, InA, r.
 *
 *	Usage:
 *	p			-> Prints all channels
 *	p 10			-> Prints data for channel 10
 *	p 10 11 12	-> Prints data for channel 10, 11, 12
 *	p cal		-> Prints calibration resistor data
 */
static char do_cmd_p( void )
{
	//char str[100];	// max string len is about 52 chars, so 100 is very safe
/*
	unsigned char mode = get_cur_mode();
        unsigned char demod_mode = get_demod_mode();
	unsigned short ch_mask = 0;
	int nargs = tok_get_num_args() - 1;         // -1 for cmd

	if (nargs == 0)
		ch_mask = 0xFFFF;
	else
		ch_mask = parse_channel_list(nargs);

        if (demod_mode == DEMOD_MODE_SINE)
            TTYPuts("Demod Mode: SINE\r\n");
        else
            TTYPuts("Demod Mode: SQUARE\r\n");

        if (mode == MODE_DIODE)
		TTYPuts("Ch   GDAC      Demod    Delta  Volts\r\n");
	else
		TTYPuts("Ch   Gain    ADAC         Demod         Delta      Current           Res\r\n");
	
	int i = 0;
	for (i = 0; i < 16; i++) 
	{
		if ((ch_mask >> i) & 0x1) 
		{
			if (mode == MODE_DIODE)
				print_diode_line(i,str);
			else
				print_standard_line(i,str);
		}
	}*/
	return STATUS_OK;
}

/** VER
 *	Spits out a version string for the current code.
 *
 *	Usage:
 *	No arguments
 */
static char do_cmd_version( void )
{
 // Print the board name and mode
 TTYPuts("PMOTOR-QEI ");

 // Print the standard version string and config name
 bp_print_version_info(VERSION_STRING);

 return STATUS_OK;
}
