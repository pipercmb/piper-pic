#ifndef __PMOTOR_QEI_INCLUDED
#define __PMOTOR_QEI_INCLUDED

#define BOARD_NAME  "PMOTOR_QEI"

// Include the standard include files for the project
#include "../bp_common/config_name.h"
#include "../bp_common/bp_common.h"
#include "../common/common.h"
#include "../common/frame_buf.h"
#include "../common/ptty.h"

//---------> Some useful stuff
#define CLOCKFREQ 20000000L		// 20.000 MHz instruction clock (synthesised from 5 MHz crystal, 16x PLL)

// #define BRG_SETTING 2       // 416 kbs.  This is the fastest rate we can generate with a 20 MHz FCY that is
                            // compatible with the MAC using our FTDI chip USB to Fiber board.
                            // BRG = FCLK/(BaudRate*16) - 1
// Calculate the actual BAUD rate from the specified BRG setting
// #define BAUD_RATE           (CLOCKFREQ/(BRG_SETTING+1) >> 4)

#define BAUD_RATE	115200
#define BRG_SETTING	((int)((CLOCKFREQ/(BAUD_RATE*16.0)- 1 + 0.5)))	// BRG (baud rate generator) setting
								// BRG = FCLK/(BaudRate*16) - 1
								// Add 0.5 to convert to int from float
#define ENCODER_PULSE_PER_REV       50
#define ENCODER_COUNTS_PER_REV      200 // 50 lines per revolution and we run hardware in 4x mode
#define ENCODER_MAX       (ENCODER_COUNTS_PER_REV - 1)

#define DATA_MODE_OFF     0     // Board will report no data
#define DATA_MODE_ON      1     // Standard hex-encoded backplane-style data packets

// Interval measured in ticks at which we compute velocity by differencing the encoder readings.
// An interval of 100 ticks gives a velocity loop update rate of 40 Hz.
// It gives a velocity precision of 0.008 revs / sec ~ 0.5 RPM
// calculated as (1 count)/(100 ticks)*(4000 ticks/s)/(5000 counts/rev) =
#define VELOCITY_MEASUREMENT_INTERVAL   100

// Multiply this by velocity in hardware units to get velocity in Hz.  NOTE THE FLOAT (slow!!!)
#define VELOCITY_HZ_PER_COUNTS  ((float) TICKS_PER_FRAME / ((float)VELOCITY_MEASUREMENT_INTERVAL * ((float)ENCODER_COUNTS_PER_REV)))

//------> INPUT PINS
#define PIN_ADDR0           PORTEbits.RE1   // Backplane card address HEX switch
#define PIN_ADDR1           PORTEbits.RE2   // Backplane card address HEX switch
#define PIN_ADDR2           PORTEbits.RE3   // Backplane card address HEX switch
#define PIN_ADDR3           PORTEbits.RE0   // Backplane card address HEX switch
#define PIN_ADDR4           PORTEbits.RE4   // Address high bit (jumper next to hex address switch)

#define PIN_SYNC_CLK        PORTDbits.RD3   // From the Backplane 96 pin connector
#define PIN_FRAME_CLK       PORTDbits.RD2   // From the Backplane 96 pin connector

//-----> OUTPUT PINS
#define PIN_485_ENABLE          LATCbits.LATC14	// Enable the RS-485 driver (so we can transmit over UART)

// Diagnostic outputs
#define PIN_OSCOPE0		LATDbits.LATD6  // Diagnostic pin
#define PIN_OSCOPE1		LATDbits.LATD5  // Diagnostic pin
#define PIN_OSCOPE2		LATDbits.LATD4  // Diagnostic pin

#define PIN_LED1		LATBbits.LATB2	// Green LED
#define PIN_LED2		LATBbits.LATB1	// Yellow LED
#define PIN_LED3		LATBbits.LATB0	// Red LED
#define PIN_LED4		LATEbits.LATE7	// Red LED
#define PIN_LED                 PIN_LED1        // Standard backplane boards require a PIN_LED symbol (flashes on sync_to_frame_clock)

//---------> Function Prototypes
// pmotor-qei.c
void init_pic( void );		// Init the PIC ports and hardware

// process_cmd.c
void process_cmd( char *cmd );

// cmd_gets.c
void cmd_gets( char *str, int n );

// int_handler.c
void set_data_frame( unsigned char state );
unsigned char data_frame_on( void );
void set_LED( unsigned char state );

#endif //__PMOTOR_QEI_INCLUDED
