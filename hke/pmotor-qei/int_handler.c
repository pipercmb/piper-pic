#include <p30F6015.h>
#include <stdio.h>

#include "pmotor-qei.h"

// Local functions for the interrupt handler
//static void update_spi_and_sum();
static void update_frame(); // writes a new reading (if we have one) to our data frame

// Local variables for the interrupt handler
static long current = 0;
static long previous = 0;
static long delta = 0;

static long pos_counter = 0;    // Accumulator for storing absolute position counts from POSCNT
static long curr_pos_count = 0;     // Current position count
static long prev_pos_count = 0;     // Previous position count
static long delta_pos_count = 0;    // Difference between current and previous position counts
static long rev_counter = 0;        // Accumulator for counting motor revolutions
static long curr_rev_count = 0;     // Current revolution count
static long prev_rev_count = 0;     // Previous revolution count
static long delta_rev_count = 0;    // Difference between current and previous revolution counts

static unsigned int tick = 0; // Increments on each interrupt, resets after reading each channel (~250)
static unsigned char frame_counter = 0; // Which frame are we on?, a frame holds 16 readings and takes 1 seconds
//static unsigned char count_error = 0;   // from QEICONbits.CNTERR, POSCNT overflow or underflow, i.e. POSCNT > MAXCNT or POSCNT < 0, 0 = no error, 1 = error, default no error
//static unsigned char enc_direction = 0; // from QEICONbits.UPDN, 0 = negative/reverse direction, 1 = positive/forward direction

//static volatile unsigned char demod_counter = 0; // Counts which reading we're doing within the current frame (from 0 to 15)
// In standard operation, this is the same as TMux setting
// tagged 'volatile' because referenced by 'set_tmux_auto' function that is called by process_cmd()

// Variables used to communicate with the outside world
static volatile unsigned char led_on = 2;   // 0 = off, 1 = on, 2 = auto 1Hz (default)
static volatile unsigned char data_on = 1; // default: data stream is on

/*
 * Interrupt handler, called at 4 kHz, on the *falling* edge of the convert pulse
 */
void __attribute__((__interrupt__, __auto_psv__)) _OC2Interrupt(void)
{
    // 20090720 - Int handler measured to take 10 us normally, with some about 25 us
    // with data on, takes 13 us, so not much more. ~10% duty cycle  (interrupt rate is 4 kHZ, or 250 us per interrupt)
    PIN_OSCOPE2 = 1;

    // Check if a new frame has started by reading in PIN_FRAME_CLK
    char isNewFrame = check_frame_clock();

    // If we have just started a new frame, then reset all our state variables (happens once per second)
    // This guarantees that we stay in sync with the rest of the backplane, (even if a cosmic ray corrupted tick)
    if (isNewFrame)
        tick = 0;

    if(led_on == 0)
        PIN_LED1 = 0;
    else if(led_on == 1)
        PIN_LED1 = 1;
    else
        PIN_LED1 = PIN_FRAME_CLK;

//pos_counter += 1;   //debugging
    // Read in the encoder
    curr_pos_count = POSCNT; //snapshot of current encoder position
    delta_pos_count = curr_pos_count - prev_pos_count;
    prev_pos_count = curr_pos_count;
    if (delta_pos_count > ENCODER_COUNTS_PER_REV/2)
        delta_pos_count -= ENCODER_COUNTS_PER_REV;
    else if (delta_pos_count < -ENCODER_COUNTS_PER_REV/2)
        delta_pos_count += ENCODER_COUNTS_PER_REV;
    pos_counter += delta_pos_count;

    if(tick==0)
    {
        current = rev_counter;
        delta = current - previous;
        previous = current;
    }
    
    // Increment/decrement motor revolution counter
    //QEICONbits.INDEX	// Index Pin State Status bit (read only), 1 = index pin high, 0 = index pin low
    if(IFS2bits.QEIIF == 1)             // detect QEI interrupt flag, this is set by the hardware whenever motor crosses the index mark
    {
        if(QEICONbits.UPDN == 1)        // if motor is spinning in positive/forward direction
            rev_counter += 1;           // increment counter
        else if(QEICONbits.UPDN == 0)   // if motor is spinning in negative/reverse direction
            rev_counter -= 1;           // decrement counter
        IFS2bits.QEIIF = 0;             // reset QEI interrupt flag
    }
    
//rev_counter += 1; //debugging
    
    //once a frame, calculate change in revolution counts (delta), aka rev/sec (Hz))
    if(tick==0)
    {
        curr_rev_count = rev_counter;
        delta_rev_count = curr_rev_count - prev_rev_count;
        prev_rev_count = curr_rev_count;
        
    }
    
    update_frame(); // writes a new reading (if we have one) to our data frame
    update_communication(isNewFrame); // Update address counters and transmit buffer in our time slot (does not look at tick)

    // Increment the tick counter
    tick++;

    PIN_OSCOPE2 = 0;
    
    IFS0bits.OC2IF = 0; // Clear Output Compare 2 interrupt flag
}

static void update_frame()
{
    // The start of a new frame
    if (tick == 0) {
        // Write out the frame counter in new frame buffer
        fb_put8((unsigned char) frame_counter); // Save the bottom 8 bits of the frame counter
        // this is just for consistency with the other boards
        
		fb_put4( 0 );		// Reserved for future use
		fb_put4( 0 );		// Save the Status (not defined yet)
fb_putc('-');
        fb_put32(pos_counter);
fb_putc('-');
        //fb_put32(delta_pos_count);
        fb_put32(delta);
fb_putc('-');
        // Revolution Counter
        fb_put32(rev_counter);
fb_putc('-');
        fb_put32(delta_rev_count);
fb_putc('-');
        fb_put16(ENCODER_PULSE_PER_REV);
fb_putc('-');
        fb_put16(POSCNT);
fb_putc('-');
        fb_put16(MAXCNT);
fb_putc('-');
        fb_put16(QEICON);
fb_putc('-');
        fb_put16(DFLTCON);
        
        // Update the internal frame_counter
        frame_counter++;
    }

    // The end of a frame
    else if (tick == TICKS_PER_FRAME - 10) {
        // This buffer is full, so swap to new one and queue up the old one for sending
        swap_active_frame_buf(); // Swap which frame buffer we're writing to
        
        if (data_on)
            enqueue_last_frame_buf();        
    }
}

void set_data_frame(unsigned char state)
{
    data_on = state;
}

unsigned char data_frame_on(void)
{
    return data_on;
}

void set_LED (unsigned char state)
{
    led_on = state;
}
/*
unsigned char read_enc_direction(void)
{
    return QEICONbits.UPDN;
}
unsigned char get_enc_direction(void)
{
    return enc_direction;
}
*/
