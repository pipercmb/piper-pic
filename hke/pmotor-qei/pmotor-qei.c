#include <p30F6015.h>
#include <libpic30.h>
#include <dsp.h>
#include <uart.h>
#include <timer.h>
#include <outcompare.h>
#include <ports.h>
#include <qei.h>
#include <stdio.h>

#include "pmotor-qei.h"
#include "..\common\ptty.h"

// DSPIC30F6015 Configuration Bit Settings
// FOSC
#pragma config FOSFPR = ECIO_PLL16      // Oscillator (ECIO w/PLL 16x)
#pragma config FCKSMEN = CSW_FSCM_OFF   // Clock Switching and Monitor (Sw Disabled, Mon Disabled)
// FWDT
#pragma config WDT = WDT_OFF            // Watchdog Timer (Disabled)
// FBORPOR
#pragma config BOREN = PBOR_OFF         // PBOR Enable (Disabled)
#pragma config MCLRE = MCLR_EN          // Master Clear Enable (Enabled)

/* Device configuration register macros for building the hex file */
//_FOSC(CSW_FSCM_OFF & XT_PLL16); /* XT with 8xPLL oscillator, Failsafe clock off */
//_FWDT(WDT_OFF); /* Watchdog timer disabled */
//_FBORPOR(PBOR_OFF & MCLR_EN); /* Brown-out reset disabled, MCLR reset enabled */
//_FGS(CODE_PROT_OFF); /* Code protect disabled */


/* Global variables */
unsigned char CardAddress;

void init_pic()
{
	// Set all ADC lines as digital IO (Family Ref 11.3)
	ADPCFG = 0xFFFF;

	// Configure GPIO pins
	TRISBbits.TRISB0 = 0;	// LED3
	TRISBbits.TRISB1 = 0;	// LED2
	TRISBbits.TRISB2 = 0;	// LED1
	TRISEbits.TRISE7 = 0;	// LED4
	TRISCbits.TRISC13 = 1;	// Clock Mode
	TRISCbits.TRISC14 = 0;	// RS-485 Enable
	TRISDbits.TRISD2 = 1;	// Frame Clock
	TRISDbits.TRISD3 = 1;	// Sync Clock
	TRISDbits.TRISD4 = 0;	// OSCOPE2
	TRISDbits.TRISD5 = 0;	// OSCOPE1
	TRISDbits.TRISD6 = 0;	// OSCOPE0
	TRISEbits.TRISE0 = 1;	// ADDR3
	TRISEbits.TRISE1 = 1;	// ADDR0
	TRISEbits.TRISE2 = 1;	// ADDR1
	TRISEbits.TRISE3 = 1;	// ADDR2
    
    // Set outputs to a defined state ("OFF"))
    PIN_LED1 = 0;
    PIN_LED2 = 0;
    PIN_LED3 = 0;
    PIN_LED4 = 0;
    PIN_OSCOPE0 = 0;
    PIN_OSCOPE1 = 0;
    PIN_OSCOPE2 = 0;

	// Short delay to wait for power supply voltages to settle
	__delay32((long) (RESET_DELAY_SECONDS * CLOCKFREQ));

    // Read in our card address and store in global variable
	CardAddress = read_address_pins();
    
	// Setup the UART with standard backplane settings
	U2BRG = BRG_SETTING;	// U2BRG = 10; // 115200 baud rate
	U2MODE = 0x8080;	// UART2 Mode Register
	/* U2MODE = 0b0b1000000010000000;
		U2MODEbits.UARTEN = 1;	// bit <15> UART2 enable, U2TX/U2RX pins controlled by UART2
                                // bit <14> Unimplemented read as '0'
		U2MODEbits.USIDL = 0;	// bit <13> Continue operation in Idle mode
                                // bit <12> Unimplemented read as '0'
                                // bit <11-8> Reserved write '0'
		U2MODEbits.WAKE = 1;	// bit <7> Wake-up enabled
		U2MODEbits.LPBACK = 0;	// bit <6> Loopback Mode disabled
		U2MODEbits.ABAUD = 0;	// bit <5> Input to Capture module from ICx pin
                                // bit <4-3> Unimplemented read as '0'
        U2MODEbits.PDSEL1 = 0;  // bit <2> 8-bit data, no parity
        U2MODEbits.PDSEL0 = 0;  // bit <1> 8-bit data, no parity
		U2MODEbits.STSEL = 0;	// bit <0> 1 stop bit
	*/
	U2STA = 0x0510;		// UART2 Status & Control Register
	/* U2STA = 0b0000 0101 0001 0000;
		U2STAbits.UTXISEL = 0;  // bit <15> Interrupt when a character is transferred to Transmit Shift Register
                                // bit <14-12> Unimplemented read as '0'
		U2STAbits.UTXBRK = 0;	// bit <11> U2TX pin operates normally
		U2STAbits.UTXEN = 1;	// bit <10> UART2 Transmitter enabled, U2TX pin controlled by UART2
		U2STAbits.UTXBF = 0;	// bit <9> Transmit Buffer Full Status bit (read only)
		U2STAbits.TRMT = 1;     // bit <8> Transmit Shift Register Empty bit (read only)
		U2STAbits.URXISEL1 = 0; // bit <7> Interrupt flag bit is set when character is received
        U2STAbits.URXISEL0 = 0; // bit <6> Interrupt flag bit is set when character is received
		U2STAbits.ADDEN = 0;	// bit <5> Address detect mode disabled
		U2STAbits.RIDLE = 1;	// bit <4> Receive Idle bit (read only)
		U2STAbits.PERR = 0;     // bit <3> Parity Error Status bit (read only)
		U2STAbits.FERR = 0;     // bit <2> Framing Error Status bit (read only)
		U2STAbits.OERR = 0;     // bit <1> Receive Buffer Overrun Error Status bit (read/clear only)
		U2STAbits.URXDA = 0;	// bit <0> Receive Buffer Data Available bit (read only)
	*/
	IFS1bits.U2RXIF = 0;	// Clear UART2 Receiver Interrupt Flag Status bit
    //IPC6bits.U2RXIP = 7;    // Assign Priority 7 for UART2 Receiver Interrupt
	IEC1bits.U2RXIE = 0;	// Disable UART2 Receiver Interrupt
	IFS1bits.U2TXIF = 0;	// Clear UART2 Transmitter Interrupt Flag Status bit
    //IPC6bits.U2TXIP = 7;    // Assign Priority 7 for UART2 Transmitter Interrupt
	IEC1bits.U2TXIE = 0;	// Disable UART2 Transmitter Interrupt

	// Setup QEI
    //QEICONbits.QEIM = 0;	// Disable QEI Module
	//QEICONbits.CNTERR = 0;	// Clear Count Error Status Flag bit
	QEICON = 0x1E25;	// QEI Control Register
	/* QEICON = 0b0001 1110 0010 0101
		QEICONbits.CNTERR = 0;	// bit <15> Clear Count Error Status Flag bit
                                // bit <14> Unimplemented read as '0'
		QEICONbits.QEISIDL = 0;	// bit <13> Continue operation in Idle mode
		QEICONbits.INDX = 1;	// bit <12> Index Pin State Status bit (read only)
		QEICONbits.UPDN = 1;	// bit <11> Position Counter Direction Status bit (read only)
        //QEICONbits.QEIM = 0b100;// bit <10-8> QEI enabled (x2 mode) w/ Index Pulse reset POSCNT
        //QEICONbits.QEIM = 0b101;// bit <10-8> QEI enabled (x2 mode) w/ MAXCNT reset POSCNT
        QEICONbits.QEIM = 0b110;// bit <10-8> QEI enabled (x4 mode) w/ Index Pulse reset POSCNT
        //QEICONbits.QEIM = 0b111;// bit <10-8> QEI enabled (x4 mode) w/ MAXCNT reset POSCNT
		QEICONbits.SWPAB = 0;	// bit <7> Phase A & Phase B inputs not swapped
		QEICONbits.PCDOUT = 0;	// bit <6> Position Counter Direction State Output disabled (normal I/O pin operation)
		QEICONbits.TQGATE = 1;	// bit <5> Enable Timer Gated Time Accumulation
		QEICONbits.TQCKPS = 0b00;  // bit <4-3> Timer Input Clock Prescale Select bits (16-bit timer moder only)
		QEICONbits.POSRES = 1;	// bit <2> Index Pulse resets Position Counter
		QEICONbits.TQCS = 0;	// bit <1> Timer Clock Source (internal clock Tcy)
		QEICONbits.UDSRC = 1;	// bit <0> Phase B defines POSCNT direction
	*/
    DFLTCON = 0x01F0;	// Digital Filter Control Register
	/* DFLTCON = 0b0000 0001 1111 0000;
                                    // bit <15-11> Unimplemented read as '0'
        DFLTCONbits.IMV = 0b00;     // bit <10-9> Index Match Value
        DFLTCONbits.CEID = 1;       // bit <8> Disable Count Error Interrupts
        DFLTCONbits.QEOUT = 1;      // bit <7> Enable QEA/QEB/INDX Digital Filter Outputs
        DFLTCONbits.QECK = 0b111;	// bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:256)
        //DFLTCONbits.QECK = 0b110;	// bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:128)
        //DFLTCONbits.QECK = 0b101;	// bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:64)
        //DFLTCONbits.QECK = 0b100;	// bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:32)
        //DFLTCONbits.QECK = 0b011;	// bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:16)
        //DFLTCONbits.QECK = 0b010;	// bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:4)
        //DFLTCONbits.QECK = 0b001;	// bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:2)
        //DFLTCONbits.QECK = 0b000; // bit <6-4> QEA/QEB/INDX Digital Filter Clock Divide (1:1)
                                    // bit <3-0> Unimplemented read as '0'
     * QEI Digital Filter
     * Fcyc = 20MHz    instruction cycle frequency
     * Tcyc = 50ns     instruction cycle period
     * Ffil            digital filter 
     * Tfil            digital filter period
     * Divide   Ffil		Tfil	3*Tfil
     * 1:1      20MHz		50ns	150ns
     * 1:2      10MHz		100ns	300ns
     * 1:4      5MHz		200ns	600ns
     * 1:16     1.25MHz		800ns	2.4us
     * 1:32     625kHz		1.6us	4.8us
     * 1:64     312.5kHz	3.2us	9.6us
     * 1:128	156.25kHz	6.4us	19.2us
     * 1:256    78.125kHz	12.8us	38.4us
	*/
    POSCNT = 0;         // Reset position counter
	MAXCNT = ENCODER_MAX;     // Set maximum counter value
    
	IFS2bits.QEIIF = 0;	// Clear QEI Interrupt Flag Status bit
	IEC2bits.QEIIE = 0;	// Disable QEI Interrupt

	// Before we start the interrupt handler, wait for a rising edge on external frame clock
	// This keeps all our boards in synch, and guarantees that all boards' talk slots are lined up
	// Do this just before turning on the timer interrupt.
	sync_to_frame_clock();

    // Setup Timer 2 and set its period to 5000
    TMR2 = 0;
    PR2 = CYCLES_PER_TICK - 1;  // Note, PIC timer counts from 0 to period, inclusive, so need to sub 1 to get desired sampling / tick rate
    T2CON = 0x8000;	// 
    /* T2CON = 0b1000 0000 0000 0000;
        T2CONbits.TON = 1;      // bit <15> Timer On
                                // bit <14> Unimplemented read as '0'
        T2CONbits.TSIDL = 0;	// bit <13> Continue operation in Idle Mode
                                // bit <12-7> Unimplemented read as '0'
        T2CONbits.TGATE = 0;    // bit <6> Timer gated time accumulation disabled
        T2CONbits.TCKPS = 0b00; // bit <5-4> Timer Input Clock Prescale (1:1)
        T2CONbits.T32 = 0;      // bit <3> 32-bit Timer Mode Select bits
                                // bit <2> Unimplemented read as '0'
        T2CONbits.TCS = 0;      // bit <1> Internal clock (Fcyc=Fosc/4)
                                // bit <0> Unimplemented read as '0'	
    */

    // Setup Output Compare 2 module to generate 10 usec pulses, with interrupts on FALLING edge
    OC2CONbits.OCM = 0;		// turn off OC2 before switching to new mode
    OC2RS = CYCLES_PER_ADC_CONVERT_PULSE;	// OC2RS Secondary Register
    OC2R = 0;			//OC2R Main Register
    OC2CON = 0x0005;		//Output Compare 2 Control Register
    /* OC2CON = 0b0000 0000 0000 0101;
                                // bit <15-14> Unimplemented read as '0'
        OC2CONbits.OCSIDL = 0;  // bit <13> Continue operation in Idle Mode
                                // bit <12-5> Unimplemented read as '0'
        OC2CONbits.OCFLT = 0;   // bit <4> No PWM Fault condition has occurred (only used when OCM-2:0> = 0b111)
        OC2CONbits.OCTSEL = 0;  // bit <3> Timer 2 is clock source of Output Compare 2
        OC2CONbits.OCM = 0b101; // bit <2-0> Initialize OC2 pin low, generate continuous output pulses on OC2 pin
    */
    TMR2 = (3*CYCLES_PER_TICK/4);   // Clear the timer.  This value sets the phase of the OC2 interrupts with respect to the frame_clk
                              // By setting to (3/4)*cycles_per_tick we wait 1/4 period before our first OC2 interrupt.
                              // This gives some time before the first interrupt, and extra time to the last tick of the frame
                              // The last tick often has to write a bunch of data (esp. DSPID), so needs some extra time

    // Finally, turn on interrupts!
    IFS0bits.OC2IF = 0;		// Clear Output Compare Channel 2 Interrupt Flag Status bit
    IPC1bits.OC2IP = 2;		// Assign Priority 2 for Output Compare Channel 2 Interrupt
    IEC0bits.OC2IE = 1;		// Enable Output Compare Channel 2 Interrupt
}

int main(void)
{
    init_pic(); // Setup the PIC internal peripherals

    // Setup our data frame buffers
    init_frame_buf_backplane('Q', CardAddress);

    PIN_LED1 = 0;
    //PIN_LED1 = 1;

/* Testing values of PIC SFRs after init_pic() is called
 * just double checking correct initializations for UART2, Timer2, Output Compare 2
 */

//char s[100];
//sprintf(s, "QEICON  = %04X\r\n", QEICON);TTYPuts(s);
//sprintf(s, "DFLTCON  = %04X\r\n", DFLTCON);TTYPuts(s);
//sprintf(s, "POSCNT  = %04X\r\n", POSCNT);TTYPuts(s);
//sprintf(s, "MAXCNT  = %04X\r\n", MAXCNT);TTYPuts(s);
//sprintf(s, "\r\n");TTYPuts(s);
//sprintf(s, "IFS2bits.QEIIF  = %1X\r\n", IFS2bits.QEIIF);TTYPuts(s);
//sprintf(s, "IPC10bits.QEIIP = %1X\r\n", IPC10bits.QEIIP);TTYPuts(s);
//sprintf(s, "IEC2bits.QEIIE  = %1X\r\n", IEC2bits.QEIIE);TTYPuts(s);
/*
sprintf(s, "U2BRG  = %04X\r\n", U2BRG);TTYPuts(s);
sprintf(s, "U2MODE = %04X\r\n", U2MODE);TTYPuts(s);
sprintf(s, "U2STA  = %04X\r\n", U2STA);TTYPuts(s);
sprintf(s, "PR2    = %04X\r\n", PR2);TTYPuts(s);
sprintf(s, "T2CON  = %04X\r\n", T2CON);TTYPuts(s);
sprintf(s, "OC2RS  = %04X\r\n", OC2RS);TTYPuts(s);
sprintf(s, "OC2R   = %04X\r\n", OC2R);TTYPuts(s);
sprintf(s, "OC2CON = %04X\r\n", OC2CON);TTYPuts(s);
sprintf(s, "\r\n");TTYPuts(s);
sprintf(s, "IFS1bits.U2RXIF = %1X\r\n", IFS1bits.U2RXIF);TTYPuts(s);
sprintf(s, "IPC6bits.U2RXIP = %1X\r\n", IPC6bits.U2RXIP);TTYPuts(s);
sprintf(s, "IEC1bits.U2RXIE = %1X\r\n", IEC1bits.U2RXIE);TTYPuts(s);
sprintf(s, "IFS1bits.U2TXIF = %1X\r\n", IFS1bits.U2TXIF);TTYPuts(s);
sprintf(s, "IPC6bits.U2TXIP = %1X\r\n", IPC6bits.U2TXIP);TTYPuts(s);
sprintf(s, "IEC1bits.U2TXIE = %1X\r\n", IEC1bits.U2TXIE);TTYPuts(s);
sprintf(s, "IFS0bits.OC2IF  = %1X\r\n", IFS0bits.OC2IF);TTYPuts(s);
sprintf(s, "IPC1bits.OC2IP  = %1X\r\n", IPC1bits.OC2IP);TTYPuts(s);
sprintf(s, "IEC0bits.OC2IE  = %1X\r\n", IEC0bits.OC2IE);TTYPuts(s);
*/
//PIN_LED1 = 1;
    while (1)
    {
        char s[100]; // Note the CH command can have long argument list (up to 90 characters)

        cmd_gets(s, sizeof (s)); // Get a command from the user

        process_cmd(s); // Deal with the command
    }
}

void OnIdle(void)
{
}


//to read QEI
//data = POSCNT;	//store current position count (unsigned int)
//to set Max Count
//MAXCNT = position;  /* set the Maxium Count */
