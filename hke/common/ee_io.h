#ifndef EE_IO_H
#define	EE_IO_H

#ifdef	__cplusplus
extern "C" {
#endif

#define EE_CHECKSUM_LEN 2

void eeOpenWrite(_prog_addressT ee_addr);
void eeWriteByte( unsigned char b );
void eeWriteWord( unsigned w );
void eeWrite( char* ptr, int n);
void eeWriteChecksum( void );
void eeCloseWrite( void );

void eeOpenRead(_prog_addressT ee_addr);
unsigned char eeReadByte( void );
unsigned eeReadWord( void );
int eeVerifyChecksum( _prog_addressT ee_addr, int datalen );
int eeCheckIsBlank( _prog_addressT ee_addr, int datalen );

#ifdef	__cplusplus
}
#endif

#endif	/* EE_IO_H */

