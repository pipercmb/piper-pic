#ifndef __TTY__
#define __TTY__

// these sizes must be powers of 2 and can only be 256 bytes max
#define SER_RXBUF_SIZE  128  	//Size of Rx buffer's data area

// USART transmit interrupt handler
void DTTYTxIsr( void );
void DTTYRxIsr( void );


// Enable interrupts for USART. Call after TTYInit().
void DTTYEnable( void );

// Reset buffer pointers for interrupt routines.
void DTTYResetBuffers( void );

// init the TTY routines and set the baud rage
void DTTYInit( unsigned long BaudRate, unsigned long ClockRate );

// returns non 0 if a character available, else 0.  Never blocks
unsigned char DTTYPeekC( void );

// get a character from TTY.
// Blocks until a char received if not using interrupts.
char DTTYGetC( void );

// Send a buffer
// SEVERE RESTRICTIONS ON USE.  MUST UNDERSTAND BEFORE CALLING!!!!
void DTTYSendBuffer( volatile char * volatile buf, unsigned char n );

// Send a character
// Only for debugging -- DO NOT CALL THIS FUNCTION UNLESS YOU KNOW WHAT YOU'RE DOING!
void DTTY_Debug_Putc( char c );

#endif	// __TTY__
