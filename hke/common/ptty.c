#include <uart.h>
#include <stdio.h>

#include "common.h"
#include "ptty.h"

/* This set of TTY routines is for any board that reports "data_frames".
   It is used by all the backplane boards, but also by others such as psyncadc.
*/

char *txDataBufPtr = NULL; // Pointer to data packet buffer to transmit

// Transmit queue
#define TX_QUEUE_LEN	512
static char tx_queue[ TX_QUEUE_LEN ];
static volatile int tx_head = 0; // Pos in queue buffer to read from (int_handler does reading)
static int tx_tail = 0; // Insertion point for new characters in queue buffer

/*
 * Returns how many characters are queued up to transmit at the next opportunity.
 */
int txQueueBytes()
{
    _INTERRUPTS_OFF;
    int n = tx_tail - tx_head;
    _INTERRUPTS_ON;

    if (n >= 0)
        return n;
    else
        return n + TX_QUEUE_LEN;
}

// enqueue our new character (12 instruction cycles)
// Use only inside this interrupt handler
// Note that it only evaluates the argument once! (ESSENTIAL!)
#define __txQueuePutc(c) {       \
    tx_queue[ tx_tail ] = (c);   \
    tx_tail++;                   \
    if (tx_tail >= TX_QUEUE_LEN) \
        tx_tail = 0;             \
}

/*
 * Put a new character in the *software* serial port transmit queue.
 * --->Call only from main-line code (*not* from int_handler)
 */
void txQueuePutc(char c)
{
    // If the queue is full, we block, waiting on int_handler to clear it out
    while (txQueueBytes() >= TX_QUEUE_LEN - 1)
        ; // Spin our wheels waiting for int_handler to clear out queue

    // Queue not full, so enqueue our new character
    _INTERRUPTS_OFF;
    __txQueuePutc(c);
    _INTERRUPTS_ON;
}

/*
 * Put a new character in the *software* serial port transmit queue.
 * --->Call only from int-handler (*not* from main code)
 * Call from same interrupt handler that is calling TTYUpdateTX() since this routine does not disable ints.
 * For speed, does not check for frame overflows, will just stomp on the buffer.
 */
void TTYPutcFromInt(char c)
{
    __txQueuePutc(c);
}

/*
 * Add a string in the *software* serial port transmit queue.
 * --->Call only from int-handler (*not* from main code)
 * Call from same interrupt handler that is calling TTYUpdateTX() since this routine does not disable ints.
 * For speed, does not check for frame overflows, will just stomp on the buffer.
 */
void TTYPutsFromInt(char *s)
{
    while (*s) {
        __txQueuePutc(*s++);
    }
}

/**
 * Outputs 32 bits to the serial port (in binary).
 * Call from same interrupt handler that is calling TTYUpdateTX() since this routine does not disable ints.
 */
void TTYPut16FromInt(unsigned short x)
{
    __txQueuePutc((unsigned char)(x >> 8));
    __txQueuePutc((unsigned char)x);
}

/**
 * Outputs 32 bits to the serial port (in binary).
 * Call from same interrupt handler that is calling TTYUpdateTX() since this routine does not disable ints.
 */
void TTYPut32FromInt(unsigned long x)
{
    unsigned char c;

    c = (unsigned char)(x >> 24);
    __txQueuePutc(c);

    c = (unsigned char)(x >> 16);
    __txQueuePutc(c);

    c = (unsigned char)(x >> 8);
    __txQueuePutc(c);

    c = (unsigned char)x;
    __txQueuePutc(c);
}


/*
 * Get a character from the software TX QUEUE.
 * --->Call only from inside the interrupt handler.
 */
static char txQueueGetc()
{
    char c;

    // Check if the queue is empty
    if (tx_head == tx_tail)
        return 0;

    // Get the character and update the head index
    c = tx_queue[ tx_head ];
    tx_head++;
    if (tx_head >= TX_QUEUE_LEN)
        tx_head = 0;
    return c;
}

/*
 * Call this routine from the interrupt handler
 * Checks if we have any characters in our software queues to transmit, and sends them
 */
void TTYUpdateTX()
{
    // think about making this more interrupt safe!!!

    // Check if there is any Data Packet data to send
    if (txDataBufPtr != NULL) {
        // U2STAbits.UTXBF == 1 implies hardware TX buffer is full
        while (!U2STAbits.UTXBF) { // Loop while the 4 byte hardware TX FIFO has space in it
            if (*txDataBufPtr) // If we have another character to transmit
                U2TXREG = *txDataBufPtr++; // Transfer data byte to TX FIFO
            else { // Else, we are at end of string
                txDataBufPtr = NULL; // Set the pointer to NULL
                return;
            }
        }
    }
        // Otherwise, see if there's any Response data (from txQueue)
    else {
        while (!U2STAbits.UTXBF) { // Loop while the 4 byte hardware TX FIFO has space in it
            if (txQueueBytes() > 0) // If we have another character to transmit
                U2TXREG = txQueueGetc(); // Transfer data byte to hardware TX FIFO
            else // No more characters in software TX queue
                return;
        }
    }
}

/*
 * Call this routine from the interrupt handler.
 * Checks if we have any characters in our software queues to transmit, and sends them
 * Intended for boards that do not have a frame, so do not need to keep track of
 * txDataBufPtr.
 * WILL POTENTIALLY BLOCK.  USE WITH CARE.
 */
void TTYUpdateTXNoFrame(int nMax)
{
    int i=0, n;

    n = txQueueBytes();
    if( n > nMax)
        n = nMax;

    for( i=0; i<n; ++i ) {
        while (U2STAbits.UTXBF);        // Wait for there to be some room in 4 byte hardware queue

        U2TXREG = txQueueGetc();        // Write out one char
    }    
}

/*
 * Call this routine from the interrupt handler
 * Checks if we have any characters in our software queues to transmit, and write them to the port.
 * Use with boards that do not have a standard backplane data frame, so do not need to keep track of
 * txDataBufPtr.
 *
 * Note: This function pushes at most 4 character to the serial port, so it
 * must be called sufficiently quickly to avoid the software buffer from
 * overflowing! Intended for use only on boards running with very fast interrupt
 * rates.
 */
void TTYUpdateTXNoFrameFast4(void)
{
    int i=0, n;

    n = txQueueBytes();

    for( i=0; i<n; ++i ) {
        if( U2STAbits.UTXBF )        // If hardware buffer is full, go home.  We do not block
            return;
        U2TXREG = txQueueGetc();     // Write out one char
    }
}


/*
 * Call this routine from the interrupt handler
 * Checks if we have any characters in our software queues to transmit, and sends them
 * Intended for boards that do not have a frame, so do not need to keep track of
 * txDataBufPtr.
 *
 * Note: This function pushes at most 1 character to the serial port, so it
 * must be called sufficiently quickly to avoid the software buffer from
 * overflowing! Intended for use only on boards running with very fast interrupt
 * rates.
 */
void TTYUpdateTXNoFrameFast(void)
{
    if (!U2STAbits.UTXBF && txQueueBytes()) {
            U2TXREG = txQueueGetc();
    }
}

/*
 * Returns true if the TTY routines are currently sending frame buffer data over the port
 */
int TTYIsSendingFrameBuf()
{
    if (txDataBufPtr != NULL)
        return 1;
    return 0;
}

void TTYSendFrameBuf(char *buf)
{
    // Check if we are currently transmitting
    if (txDataBufPtr != NULL)
        return;

    // Save a pointer to the buffer to transmit
    txDataBufPtr = buf;
}

/*
 * Send a string over the UART
 */
void TTYPutsDirect(char *s)
{
    while (*s) {
        while (BusyUART2()); // Wait till not busy
        putcUART2(*s++);
    }
}

/*
 * Send a character over the UART
 */
void TTYPutcDirect(char c)
{
    while (BusyUART2()); // Wait till not busy
    putcUART2(c);
}

/*
 * Queue up a character to be transmitted during our talk slot.
 * Call only from regular code - not from interrupt handler.
 */
void TTYPutc(char c)
{
    txQueuePutc(c);
}

/*
 * Queue up a string to be transmitted during our talk slot.
 * Call only from regular code - not from interrupt handler.
 */
void TTYPuts(char *s)
{
    while (*s)
        txQueuePutc(*s++);
}

