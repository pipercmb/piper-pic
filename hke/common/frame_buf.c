#include "ptty.h"
#include "frame_buf.h"

static volatile char * volatile wptr;	// write pointer

static char frame_buf0[ FRAME_BUF_LEN ];
static char frame_buf1[ FRAME_BUF_LEN ];
static volatile char write_buf_num=0;	// Which frame buf is currently being written to
					// The other one is being transmitted

static unsigned char header_bytes=1;	// How many bytes are in the header portion of buffer

char *hex = "0123456789ABCDEF";	// array to numbers into ascii values in HEX

/*
 * Minimal init of the frame buffers.  Used by PSyncADC and any other board that wants the
 * the frame buffer technology for data packet reporting, but isn't part of the "backplane" system.
 */
void init_frame_buf_min( int len )
{
 int i;

 // Set to write to buffer 0 first
 write_buf_num = 0;

 // First, fill all the buffer with known characters, for debugging
 for( i=0; i<len; ++i ) {
	frame_buf0[i] = '_';
	frame_buf1[i] = '=';
 }

  frame_buf0[0] = '*';		// start character
  frame_buf0[len-3] = '\r';
  frame_buf0[len-2] = '\n';
  frame_buf0[len-1] = '\0';

  frame_buf1[0] = '*';		// start character
  frame_buf1[len-3] = '\r';
  frame_buf1[len-2] = '\n';
  frame_buf1[len-1] = '\0';
  
  // Set the write pointer to  the first data byte location
  header_bytes = 1;  // (only one header byte, the * char)
  wptr = &frame_buf0[header_bytes];
}

/*
 * Fill in all the constant stuff for each frame buffer.
 * Used by all boards in the "backplane" system which have addresses, and boardtypes
 */
void init_frame_buf_backplane( char BoardType, unsigned char cardAddress )
{
 init_frame_buf_min( FRAME_BUF_LEN );		// Do the minimal init of the frame buffer
 
 frame_buf0[1] = hex[ cardAddress >> 4 ];	// card address MS Nibble
 frame_buf0[2] = hex[ cardAddress & 0x0F ];	// card address LS Nibble
 frame_buf0[3] = BoardType;		// card type 

 frame_buf1[1] = hex[ cardAddress >> 4 ];	// card address MS Nibble
 frame_buf1[2] = hex[ cardAddress & 0x0F ];	// card address LS Nibble
 frame_buf1[3] = BoardType;		// card type 
 
 // Set the write pointer to  the first data byte location
 header_bytes = 4;		// 4 header bytes, *, address, address, type
 wptr = &frame_buf0[header_bytes];
}

/*
 * Swap which buffer is being written to with which one is being transmitted
 */
void swap_active_frame_buf( void )
{
 if( write_buf_num == 0 )  {
	write_buf_num = 1;
	wptr = &frame_buf1[ header_bytes ];
 }
 else {
	write_buf_num = 0;
	wptr = &frame_buf0[ header_bytes ];
 }
}

/*
 * Send a frame buffer to the tty routines for transmitting
 */
void enqueue_last_frame_buf( void )
{
 if( write_buf_num == 0 )	// Writing to 0, so transmit 1
 	TTYSendFrameBuf( frame_buf1 );
 else				// Writing to 1, so transmit 0
 	TTYSendFrameBuf( frame_buf0 );
}


void fb_putc( unsigned char c )
{
 *wptr++ = c;
}

void fb_puts(char *s)
{
    while (*s)
        *wptr++ = *s++;
}

void fb_put4( unsigned char c )
{
 *wptr++ = hex[ c & 0xf ];	// LSN
}

void fb_put8( unsigned char c )
{
 *wptr++ = hex[ c >> 4 ];	// MSN
 *wptr++ = hex[ c & 0xf ];  // LSN
}

/*
 * Extract the bytes out of a short or long variable in a very efficient manner.
 * B0 is LSB since C18 is little endian.
 */
#define DWORD_B0(x) (*(((unsigned char *)&x)+0))
#define DWORD_B1(x) (*(((unsigned char *)&x)+1))
#define DWORD_B2(x) (*(((unsigned char *)&x)+2))
#define DWORD_B3(x) (*(((unsigned char *)&x)+3))

void fb_put12( unsigned short d )
{
 // Top nibble of 12 bit quantity
 *wptr++ = hex[ DWORD_B1(d) & 0xf ];

 // Middle nibble
 *wptr++ = hex[ DWORD_B0(d) >> 4 ];

 // Lower nibble
 *wptr++ = hex[ DWORD_B0(d) & 0xf ];
}

void fb_put16( unsigned short d )
{
 // Most significant byte first
 *wptr++ = hex[ DWORD_B1(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B1(d) & 0xf ];		// LSN

 // Least significant byte
 *wptr++ = hex[ DWORD_B0(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B0(d) & 0xf ];		// LSN
}


void fb_put24( unsigned long d )
{
 // Most significant byte first
 *wptr++ = hex[ DWORD_B2(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B2(d) & 0xf ];		// LSN

 // Middle byte
 *wptr++ = hex[ DWORD_B1(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B1(d) & 0xf ];		// LSN

 // Least significant byte
 *wptr++ = hex[ DWORD_B0(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B0(d) & 0xf ];		// LSN
}

/*
 * Put a 32 bit value in the frame buffer.
 * Takes 125 instruction cycles.
 */
void fb_put32( unsigned long d )
{
 // Most significant byte first
 *wptr++ = hex[ DWORD_B3(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B3(d) & 0xf ];		// LSN

 // Middle byte
 *wptr++ = hex[ DWORD_B2(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B2(d) & 0xf ];		// LSN

 // Other Middle byte
 *wptr++ = hex[ DWORD_B1(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B1(d) & 0xf ];		// LSN

 // Least significant byte
 *wptr++ = hex[ DWORD_B0(d) >> 4 ];			// MSN
 *wptr++ = hex[ DWORD_B0(d) & 0xf ];		// LSN
}
