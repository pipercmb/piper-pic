#include <stdio.h>

#include "common.h"
#include "ptty.h"
#include "tty_print.h"

/*
 * This is the START of a generic library for fast HEX printing / formatting for the dsPIC.
 * These routines are much faster than sprintf(str, "%x", number );
 */

// Static array for mapping 4 bit numbers to hex characters, used by other files
static char *hex = "0123456789ABCDEF";

/*
 * Extract the bytes out of a short or long variable in a very efficient manner.
 * B0 is LSB since C18 is little endian.
 */
#define DWORD_B0(x) (*(((unsigned char *)&x)+0))
#define DWORD_B1(x) (*(((unsigned char *)&x)+1))

// Print an 8-bit hex number to specified string
int sprintHex8(char *str, unsigned char d)
{
    char c;

    c = hex[(d>>4) & 0xf];
    str[0] = c;

    c = hex[d & 0xf];                   // LSN
    str[1] = c;

    str[2] = '\0';

    return 2;
}

// Print a 16-bit hex number to specified string
int sprintHex16(char *str, unsigned int d)
{
    char c;

    c = hex[(d>>12) & 0xf];
    str[0] = c;

    c = hex[(d>>8) & 0xf];
    str[1] = c;

    c = hex[(d>>4) & 0xf];
    str[2] = c;

    c = hex[d & 0xf];                   // LSN
    str[3] = c;

    str[4] = '\0';

    return 4;
}

// Print a 32-bit hex number to specific string
int sprintHex32(char *str, unsigned long d)
{
    char c;

    c = hex[(d>>28) & 0xf];
    str[0] = c;

    c = hex[(d>>24) & 0xf];
    str[1] = c;

    c = hex[(d>>20) & 0xf];
    str[2] = c;

    c = hex[(d>>16) & 0xf];                   // LSN
    str[3] = c;

    c = hex[(d>>12) & 0xf];
    str[4] = c;

    c = hex[(d>>8) & 0xf];
    str[5] = c;

    c = hex[(d>>4) & 0xf];
    str[6] = c;

    c = hex[d & 0xf];                   // LSN
    str[7] = c;

    str[8] = '\0';

    return 8;
}


// Print a 32 bit hex number
void TTYPrintHex32( unsigned long d )
{
	TTYPrintHex16( (unsigned int)(d >> 16) );
	TTYPrintHex16( (unsigned int)d );
}

// Print a 16 bit hex number
void TTYPrintHex16( unsigned int d )
{
	char c;

	// Most significant byte first
	c = hex[ (d>>12) & 0xf ];                    // MSN
	TTYPutc(c);

	c = hex[ (d>>8) & 0xf ];
	TTYPutc(c);

	// Least significant byte
	c = hex[ (d>>4) & 0xf ]; 
	TTYPutc(c);

	c = hex[ d & 0xf ];                   // LSN
	TTYPutc(c);
}

// Print an 8 bit hex number
void TTYPrintHex8( unsigned char d )
{
	char c;

	c = hex[ (d>>4) & 0xf ]; 
	TTYPutc(c);

	c = hex[ d & 0xf ];                   // LSN
	TTYPutc(c);
}


/*
 * Converts an ascii hex character to its integer equivalant
 * 'E' -> 14
 * a return value of -1 indicates an invalid hex character
 */
int parse_hex_digit( char h )
{
 if( h >= '0' && h <= '9' )
	return h - '0';
 else if( h >= 'a' && h <= 'f' )
	return h - 'a' + 10;
 else if( h >= 'A' && h <= 'F' )
	return h - 'A' + 10;
 else
	return -1;
}
