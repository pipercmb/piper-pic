#include <p30F5011.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>

#include "../common/common.h"
#include "../common/stty.h"
#include "../common/frame_buf.h"


#define STATE_BYTES	32

unsigned char _EEDATA(2) stateDataEE[STATE_BYTES];		// Reserve memory for the state data in EEPROM

#define ROW_WORDS (_EE_ROW/2)
static int rowBuf[ROW_WORDS] __attribute__((aligned(2)));	// Regular RAM to hold one "row" of data while copying to EEPROM
#define HEADER_WORDS  3             // Number of words of "header" info at start of data

// Local functions
static unsigned computeChecksum( char *p, int n );

static int readState( struct psyncAdcState *pState );
static void writeState( struct psyncAdcState *pState );
static void getBoardState( struct psyncAdcState *pState );
static void setBoardState( struct psyncAdcState *pState );
static int compareStates( struct psyncAdcState *p, struct psyncAdcState *q );


// Saves state buffer to the EEPROM memory
void eeSaveState(int version, int* buffer, int len)
{
	// Read in the stored state first, and don't overwrite if it was the same
	if( compareBufferToEE(version, buffer, len) )  {
        TTYPuts("Unchanged, not saving to EEPROM.\r\n");
        return;
	}

	// If we couldn't read old buffer, or the buffer contents have changed, save it
	TTYPuts("Changed, going to actually save.\r\n");

    //-- Write out the first row, which contains our HEADER (version, len, checksum) and some DATA
    // Load up the row buffer with our header info and start of data
    rowBuf[0] = version;
    rowBuf[1] = len;
    rowBuf[2] = 0;          //todo: checksum

    int i;      // Index into the supplied buffer array

    int toload = ROW_WORDS - HEADER_WORDS;  // Init to max words we can fit in first row
    if( toload > len)
        toload = len;
    for( i=0; i<toload; ++i)
        rowBuf[HEADER_WORDS + i] = buffer[i];

    // Erase the first ROW in Data EEPROM
    _erase_eedata(EE_addr, _EE_ROW);
    _wait_eedata();

    // Write a RAM to a row in EEPROM
    _write_eedata_row(EE_addr, (void*)rowBuf);
    _wait_eedata();

    // WRITE the remaining data if needed
    while( i < len) {
        int j;              // Index into row buffer

        int words_left = len - i;
        if( words_left > ROW_WORDS)  {
            for( int j=0; j<ROW_WORDS; ++j) // Copy over a full row's worth
                rowBuf[j] = buffer[i + j];
        } else {
            for( int j=0; j<words_left; ++j)    // Copy all the remaining data words
                rowBuf[j] = buffer[i + j];
            for( ; j<ROW_WORDS; ++j)        // Fill up the rest with a defined character
                rowBuf[j] = 'U';            // set to 'U' for unused (this is not necessary, but helps with debugging)
        }
        // Erase the ROW in Data EEPROM
        _erase_eedata(EE_addr, _EE_ROW);
        _wait_eedata();

        // Write the row to EEPROM
        _write_eedata_row(EE_addr, rowBuf);
        _wait_eedata();
    }
}

/*
 * Compare the supplied state information to that stored in EEPROM.
 * Returns 0 if they do not match.  1 if they do match.
 */
static int compareBufferToEE(int version, unsigned* buffer, int len)
{
    // initialize a variable to represent the Data EEPROM address
    _prog_addressT EE_addr;
    _init_prog_address(EE_addr, stateDataEE);

    unsigned word;

    // Read VERSION from EEPROM
    _memcpy_p2d16(&word, EE_addr, 2);
    if( word != version )
        return 0;           // Wrong version

    EE_addr += 2;           // Advance to the next word

    // Read the length from EEPROM
    _memcpy_p2d16(&word, EE_addr, 2);
    if( word != len )
        return 0;           // Wrong length

    EE_addr += 2;           // Advance to the next word

    int nwords = len >> 1;  // Compute number of words to read (len must be even!)
    for( int i=0; i<nwords; ++i) {
        _memcpy_p2d16(&word, EE_addr, 2);
        if( word != buffer[i] )
            return 0;       // Wrong value
    }

    //TODO: checksum!
    
    return 1;       // All correct!
}

static int readState( struct psyncAdcState *pState )
{
    // initialize a variable to represent the Data EEPROM address
    _prog_addressT EE_addr;
    _init_prog_address(EE_addr, stateDataEE);

    // Copy from EEPROM to RAM
    _memcpy_p2d16(pState, EE_addr, STATE_BYTES);

	// Check the version, if it's wrong, we can't restore anything
	if( pState->version != VERSION )  {
                char s[80];
                sprintf(s,"Wrong EEPROM version (%d)\r\n",pState->version);
		TTYPuts(s);
		return 1;
	}

	// Compute the checksum (note, set checksum to 0 first)
	unsigned ckRead = pState->checksum;	// save a copy of the checksum we read in
	pState->checksum = 0;					// set checksum to 0 (we don't include checksum in the checksum)
	unsigned ckCompute = computeChecksum( (char*)pState, STATE_BYTES );
	if( ckRead != ckCompute )  {
		TTYPuts("Bad checksum\r\n");
		return 2;
	}

	return 0;		// Success
}


// Compute a simple, 16 bit checksum for an array
static unsigned computeChecksum( char *p, int n )
{
	unsigned sum = 0;
	int i;
	for( i=0; i<n; ++i )
		sum += p[i];

	return sum;
}

