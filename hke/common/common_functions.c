/*
 * This file is for functions that are useful across all boards (not just the HKE backplane boards).
 */
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "tty_print.h"


unsigned char execute_command( char *s, struct CommandDefinition cmdDefs[], int num_commands )
{
   	unsigned char r = STATUS_FAIL_CMD;	// default to fail with "unknown command" error message;
    int i;
    char str[32];

    if( !strcmp(s, "help") ) {
        for( i=0; i<num_commands; ++i ) {
            struct CommandDefinition cd = cmdDefs[i];
            sprintf(str, "%12s", cd.name);
            TTYPuts(str);
            TTYPuts(" - ");
            TTYPuts(cd.description);
            TTYPuts("\r\n");
        }
        r = STATUS_OK;
    }

    for( i=0; i<num_commands; ++i ) {
        struct CommandDefinition cd = cmdDefs[i];
        if( !strcmp(s, cd.name)) {
            r = cd.handler();       // Run the command handler
            break;
        }
    }

    return r;
}

/*
 * Print a human readable status message depending on the status code.
 */
void print_status( char status )
{
	switch( status )  {
		case STATUS_OK:
			TTYPuts( "OK" );
			break;
		case STATUS_FAIL_CMD:
			TTYPuts( "FAIL: unknown command" );
			break;
		case STATUS_FAIL_NARGS:
			TTYPuts( "FAIL: incorrect number of arguments" );
			break;
		case STATUS_FAIL_INVALID:
			TTYPuts( "FAIL: invalid argument format" );
			break;
		case STATUS_FAIL_RANGE:
			TTYPuts( "FAIL: argument out of range" );
			break;
		default:
			TTYPuts( "FAIL: unknown error" );
			break;
	}

	// All responses must end with ! to signal parser this is end of response
	TTYPuts( "!\r\n" );
}

