#ifndef __PTTY_INCLUDED
#define __PTTY_INCLUDED

// ptty.c
void TTYUpdateTX();
void TTYUpdateTXNoFrame(int nMax);
void TTYUpdateTXNoFrameFast4(void);
void TTYUpdateTXNoFrameFast(void);
void TTYPuts( char *str );
void TTYPutc( char c );
void TTYPutcFromInt(char c);
void TTYPut16FromInt(unsigned short x);
void TTYPut32FromInt(unsigned long x);
void TTYPutsFromInt(char *s);
void TTYPutsDirect( char *str );
void TTYPutcDirect( char c );
void TTYSendFrameBuf( char *buf );
int TTYIsSendingFrameBuf( void );

#endif